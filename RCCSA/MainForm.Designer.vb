﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Title2 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenFileMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveFileMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExecuteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.AddCustomChartToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChartOptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolBtnOpenXML = New System.Windows.Forms.ToolStripButton()
        Me.ToolBtnSaveXML = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripRun = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.toolbarChartMatModel = New System.Windows.Forms.ToolStripSplitButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.tbChartMain = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.toolbarChartOption = New System.Windows.Forms.ToolStripButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblNLayerReinf = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblReinfHybrid = New System.Windows.Forms.ToolStripStatusLabel()
        Me.GroupBoxCrossSection = New System.Windows.Forms.GroupBox()
        Me.inCrossSect_tf2 = New System.Windows.Forms.TextBox()
        Me.lblCsTf2_unit = New System.Windows.Forms.Label()
        Me.lblCsTf2 = New System.Windows.Forms.Label()
        Me.inCrossSect_tf1 = New System.Windows.Forms.TextBox()
        Me.lblCsTf1_unit = New System.Windows.Forms.Label()
        Me.lblCsTf1 = New System.Windows.Forms.Label()
        Me.inCrossSect_dia = New System.Windows.Forms.TextBox()
        Me.lblCsDia_unit = New System.Windows.Forms.Label()
        Me.lblCsDia = New System.Windows.Forms.Label()
        Me.inCrossSect_bf2 = New System.Windows.Forms.TextBox()
        Me.lblCsBf2_unit = New System.Windows.Forms.Label()
        Me.lblCsBf2 = New System.Windows.Forms.Label()
        Me.inCrossSect_H = New System.Windows.Forms.TextBox()
        Me.lblCsH_unit = New System.Windows.Forms.Label()
        Me.lblCsH = New System.Windows.Forms.Label()
        Me.inCrossSect_bf1 = New System.Windows.Forms.TextBox()
        Me.lblCsBf1_unit = New System.Windows.Forms.Label()
        Me.lblCsBf1 = New System.Windows.Forms.Label()
        Me.inCrossSect_bw = New System.Windows.Forms.TextBox()
        Me.lblCsBw_unit = New System.Windows.Forms.Label()
        Me.lblCsBw = New System.Windows.Forms.Label()
        Me.csPicture = New System.Windows.Forms.PictureBox()
        Me.csOptionIsection = New System.Windows.Forms.RadioButton()
        Me.csOptionTsection = New System.Windows.Forms.RadioButton()
        Me.csOptionCircle = New System.Windows.Forms.RadioButton()
        Me.csOptionRect = New System.Windows.Forms.RadioButton()
        Me.btnExpandCsVar = New System.Windows.Forms.Button()
        Me.btnShrinkCsVar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.inBeamDim_A = New System.Windows.Forms.TextBox()
        Me.lblBeamDimA_unit = New System.Windows.Forms.Label()
        Me.lblBeamDimA = New System.Windows.Forms.Label()
        Me.inBeamDim_L = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.pointLoadOptionDistributed = New System.Windows.Forms.RadioButton()
        Me.pointLoadOptionTwo = New System.Windows.Forms.RadioButton()
        Me.pointLoadOptionOne = New System.Windows.Forms.RadioButton()
        Me.pointLoadPicture = New System.Windows.Forms.PictureBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.uipicPrestressed = New System.Windows.Forms.PictureBox()
        Me.uipicFRP = New System.Windows.Forms.PictureBox()
        Me.uipicSteel = New System.Windows.Forms.PictureBox()
        Me.btnReinfPrestressed = New System.Windows.Forms.Button()
        Me.btnReinfFRP = New System.Windows.Forms.Button()
        Me.btnReinfSteel = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.inCMP_MaxCompStrain = New System.Windows.Forms.TextBox()
        Me.inCMP_TensileStrength = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.inCMP_CompStrength = New System.Windows.Forms.TextBox()
        Me.inCMP_TensionStiffEffect = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.inCMP_ConcreteSSModel = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.uipicStrCfnmt = New System.Windows.Forms.PictureBox()
        Me.uipicStrShear = New System.Windows.Forms.PictureBox()
        Me.uipicStrFlex = New System.Windows.Forms.PictureBox()
        Me.btnStrCnfmt = New System.Windows.Forms.Button()
        Me.btnStrShear = New System.Windows.Forms.Button()
        Me.btnStrFlex = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.uipicSteelConfine = New System.Windows.Forms.PictureBox()
        Me.uipicTransReinf = New System.Windows.Forms.PictureBox()
        Me.btnSteelConfine = New System.Windows.Forms.Button()
        Me.btnTransReinf = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.inDataAnly_N = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.inDataAnly_P = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.inCurv_incr = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.inCurv_max = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.panelInput = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.inAnlyRepeated = New System.Windows.Forms.RadioButton()
        Me.inAnlyNegative = New System.Windows.Forms.RadioButton()
        Me.inAnlyPositive = New System.Windows.Forms.RadioButton()
        Me.gbRepLoadData = New System.Windows.Forms.GroupBox()
        Me.uipicRepLoadData = New System.Windows.Forms.PictureBox()
        Me.btnRepLoadData = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabPage_splash = New System.Windows.Forms.TabPage()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.ApplicationTitle = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.DetailsLayoutPanel = New System.Windows.Forms.TableLayoutPanel()
        Me.Version = New System.Windows.Forms.Label()
        Me.Copyright = New System.Windows.Forms.Label()
        Me.tabPage_input = New System.Windows.Forms.TabPage()
        Me.tabPage_chartMatModel = New System.Windows.Forms.TabPage()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.optChartMatModel = New System.Windows.Forms.ComboBox()
        Me.DgvMatModel = New System.Windows.Forms.DataGridView()
        Me.ClmX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClmY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ChartMatModel = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.ContextMenuChart = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CmChartMenu_ResetChart = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.CmChartMenu_Options = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.cmChartMenu_flipXaxis = New System.Windows.Forms.ToolStripMenuItem()
        Me.cmChartMenu_flipYaxis = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnShowLoP = New System.Windows.Forms.CheckBox()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.btnChartMatModelUpdate = New System.Windows.Forms.Button()
        Me.tabPage_main = New System.Windows.Forms.TabPage()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.optChartMain = New System.Windows.Forms.ComboBox()
        Me.DgvMain = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ChartMain = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.btnChartMainUpdate = New System.Windows.Forms.Button()
        Me.btnShowButtonMain = New System.Windows.Forms.CheckBox()
        Me.btnShowLoPMain = New System.Windows.Forms.CheckBox()
        Me.MainTooltip = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.GroupBoxCrossSection.SuspendLayout()
        CType(Me.csPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.pointLoadPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.uipicPrestressed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uipicFRP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uipicSteel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.uipicStrCfnmt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uipicStrShear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uipicStrFlex, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.uipicSteelConfine, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.uipicTransReinf, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.panelInput.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.gbRepLoadData.SuspendLayout()
        CType(Me.uipicRepLoadData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.tabPage_splash.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DetailsLayoutPanel.SuspendLayout()
        Me.tabPage_input.SuspendLayout()
        Me.tabPage_chartMatModel.SuspendLayout()
        CType(Me.DgvMatModel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChartMatModel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuChart.SuspendLayout()
        Me.tabPage_main.SuspendLayout()
        CType(Me.DgvMain, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChartMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ExecuteToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(804, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewDataToolStripMenuItem, Me.OpenFileMenu, Me.SaveFileMenu, Me.ToolStripMenuItem1, Me.ExitMenu})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'NewDataToolStripMenuItem
        '
        Me.NewDataToolStripMenuItem.Name = "NewDataToolStripMenuItem"
        Me.NewDataToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.NewDataToolStripMenuItem.Text = "New Data"
        '
        'OpenFileMenu
        '
        Me.OpenFileMenu.Name = "OpenFileMenu"
        Me.OpenFileMenu.Size = New System.Drawing.Size(139, 22)
        Me.OpenFileMenu.Text = "Open Data..."
        '
        'SaveFileMenu
        '
        Me.SaveFileMenu.Name = "SaveFileMenu"
        Me.SaveFileMenu.Size = New System.Drawing.Size(139, 22)
        Me.SaveFileMenu.Text = "Save Data"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(136, 6)
        '
        'ExitMenu
        '
        Me.ExitMenu.Name = "ExitMenu"
        Me.ExitMenu.Size = New System.Drawing.Size(139, 22)
        Me.ExitMenu.Text = "Exit"
        '
        'ExecuteToolStripMenuItem
        '
        Me.ExecuteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RunToolStripMenuItem, Me.ToolStripMenuItem2, Me.AddCustomChartToolStripMenuItem, Me.ChartOptionsToolStripMenuItem})
        Me.ExecuteToolStripMenuItem.Name = "ExecuteToolStripMenuItem"
        Me.ExecuteToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.ExecuteToolStripMenuItem.Text = "Execute"
        '
        'RunToolStripMenuItem
        '
        Me.RunToolStripMenuItem.Name = "RunToolStripMenuItem"
        Me.RunToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.RunToolStripMenuItem.Text = "Run"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(170, 6)
        '
        'AddCustomChartToolStripMenuItem
        '
        Me.AddCustomChartToolStripMenuItem.Name = "AddCustomChartToolStripMenuItem"
        Me.AddCustomChartToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.AddCustomChartToolStripMenuItem.Text = "Add Custom Chart"
        '
        'ChartOptionsToolStripMenuItem
        '
        Me.ChartOptionsToolStripMenuItem.Name = "ChartOptionsToolStripMenuItem"
        Me.ChartOptionsToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.ChartOptionsToolStripMenuItem.Text = "Chart Options"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem1, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'HelpToolStripMenuItem1
        '
        Me.HelpToolStripMenuItem1.Name = "HelpToolStripMenuItem1"
        Me.HelpToolStripMenuItem1.Size = New System.Drawing.Size(127, 22)
        Me.HelpToolStripMenuItem1.Text = "View Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolBtnOpenXML, Me.ToolBtnSaveXML, Me.ToolStripSeparator1, Me.ToolStripRun, Me.ToolStripSeparator2, Me.toolbarChartMatModel, Me.ToolStripSeparator3, Me.tbChartMain, Me.ToolStripSeparator4, Me.toolbarChartOption})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.ToolStrip1.Size = New System.Drawing.Size(804, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolBtnOpenXML
        '
        Me.ToolBtnOpenXML.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolBtnOpenXML.Image = CType(resources.GetObject("ToolBtnOpenXML.Image"), System.Drawing.Image)
        Me.ToolBtnOpenXML.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolBtnOpenXML.Name = "ToolBtnOpenXML"
        Me.ToolBtnOpenXML.Size = New System.Drawing.Size(23, 22)
        Me.ToolBtnOpenXML.Text = "Open RCCSA Data"
        '
        'ToolBtnSaveXML
        '
        Me.ToolBtnSaveXML.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolBtnSaveXML.Image = CType(resources.GetObject("ToolBtnSaveXML.Image"), System.Drawing.Image)
        Me.ToolBtnSaveXML.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolBtnSaveXML.Name = "ToolBtnSaveXML"
        Me.ToolBtnSaveXML.Size = New System.Drawing.Size(23, 22)
        Me.ToolBtnSaveXML.Text = "Save RCCSA Data"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripRun
        '
        Me.ToolStripRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripRun.Image = CType(resources.GetObject("ToolStripRun.Image"), System.Drawing.Image)
        Me.ToolStripRun.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripRun.Name = "ToolStripRun"
        Me.ToolStripRun.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripRun.Text = "Run"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'toolbarChartMatModel
        '
        Me.toolbarChartMatModel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolbarChartMatModel.Enabled = False
        Me.toolbarChartMatModel.Image = CType(resources.GetObject("toolbarChartMatModel.Image"), System.Drawing.Image)
        Me.toolbarChartMatModel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolbarChartMatModel.Name = "toolbarChartMatModel"
        Me.toolbarChartMatModel.Size = New System.Drawing.Size(32, 22)
        Me.toolbarChartMatModel.Text = "Chart Material Model"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'tbChartMain
        '
        Me.tbChartMain.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tbChartMain.Image = CType(resources.GetObject("tbChartMain.Image"), System.Drawing.Image)
        Me.tbChartMain.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tbChartMain.Name = "tbChartMain"
        Me.tbChartMain.Size = New System.Drawing.Size(29, 22)
        Me.tbChartMain.Text = "Output Chart"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 25)
        '
        'toolbarChartOption
        '
        Me.toolbarChartOption.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolbarChartOption.Image = CType(resources.GetObject("toolbarChartOption.Image"), System.Drawing.Image)
        Me.toolbarChartOption.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolbarChartOption.Name = "toolbarChartOption"
        Me.toolbarChartOption.Size = New System.Drawing.Size(23, 22)
        Me.toolbarChartOption.Text = "Current Chart Options"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.lblNLayerReinf, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel3, Me.lblReinfHybrid})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 589)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(804, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(130, 17)
        Me.ToolStripStatusLabel1.Text = "Reinforcement Layer = "
        '
        'lblNLayerReinf
        '
        Me.lblNLayerReinf.Name = "lblNLayerReinf"
        Me.lblNLayerReinf.Size = New System.Drawing.Size(19, 17)
        Me.lblNLayerReinf.Text = "  0"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel2.Text = "|"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(138, 17)
        Me.ToolStripStatusLabel3.Text = "Hybrid Reinforcement = "
        '
        'lblReinfHybrid
        '
        Me.lblReinfHybrid.Name = "lblReinfHybrid"
        Me.lblReinfHybrid.Size = New System.Drawing.Size(23, 17)
        Me.lblReinfHybrid.Text = "No"
        '
        'GroupBoxCrossSection
        '
        Me.GroupBoxCrossSection.BackColor = System.Drawing.Color.Transparent
        Me.GroupBoxCrossSection.Controls.Add(Me.inCrossSect_tf2)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsTf2_unit)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsTf2)
        Me.GroupBoxCrossSection.Controls.Add(Me.inCrossSect_tf1)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsTf1_unit)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsTf1)
        Me.GroupBoxCrossSection.Controls.Add(Me.inCrossSect_dia)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsDia_unit)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsDia)
        Me.GroupBoxCrossSection.Controls.Add(Me.inCrossSect_bf2)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsBf2_unit)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsBf2)
        Me.GroupBoxCrossSection.Controls.Add(Me.inCrossSect_H)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsH_unit)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsH)
        Me.GroupBoxCrossSection.Controls.Add(Me.inCrossSect_bf1)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsBf1_unit)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsBf1)
        Me.GroupBoxCrossSection.Controls.Add(Me.inCrossSect_bw)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsBw_unit)
        Me.GroupBoxCrossSection.Controls.Add(Me.lblCsBw)
        Me.GroupBoxCrossSection.Controls.Add(Me.csPicture)
        Me.GroupBoxCrossSection.Controls.Add(Me.csOptionIsection)
        Me.GroupBoxCrossSection.Controls.Add(Me.csOptionTsection)
        Me.GroupBoxCrossSection.Controls.Add(Me.csOptionCircle)
        Me.GroupBoxCrossSection.Controls.Add(Me.csOptionRect)
        Me.GroupBoxCrossSection.Controls.Add(Me.btnExpandCsVar)
        Me.GroupBoxCrossSection.Controls.Add(Me.btnShrinkCsVar)
        Me.GroupBoxCrossSection.Location = New System.Drawing.Point(3, 3)
        Me.GroupBoxCrossSection.Name = "GroupBoxCrossSection"
        Me.GroupBoxCrossSection.Size = New System.Drawing.Size(285, 386)
        Me.GroupBoxCrossSection.TabIndex = 4
        Me.GroupBoxCrossSection.TabStop = False
        Me.GroupBoxCrossSection.Text = "Cross Section Dimensions"
        '
        'inCrossSect_tf2
        '
        Me.inCrossSect_tf2.Location = New System.Drawing.Point(188, 354)
        Me.inCrossSect_tf2.Name = "inCrossSect_tf2"
        Me.inCrossSect_tf2.Size = New System.Drawing.Size(63, 20)
        Me.inCrossSect_tf2.TabIndex = 57
        '
        'lblCsTf2_unit
        '
        Me.lblCsTf2_unit.AutoSize = True
        Me.lblCsTf2_unit.Location = New System.Drawing.Point(253, 357)
        Me.lblCsTf2_unit.Name = "lblCsTf2_unit"
        Me.lblCsTf2_unit.Size = New System.Drawing.Size(29, 13)
        Me.lblCsTf2_unit.TabIndex = 58
        Me.lblCsTf2_unit.Text = "[mm]"
        '
        'lblCsTf2
        '
        Me.lblCsTf2.AutoSize = True
        Me.lblCsTf2.Location = New System.Drawing.Point(151, 357)
        Me.lblCsTf2.Name = "lblCsTf2"
        Me.lblCsTf2.Size = New System.Drawing.Size(28, 13)
        Me.lblCsTf2.TabIndex = 56
        Me.lblCsTf2.Text = "tf2 ="
        '
        'inCrossSect_tf1
        '
        Me.inCrossSect_tf1.Location = New System.Drawing.Point(188, 328)
        Me.inCrossSect_tf1.Name = "inCrossSect_tf1"
        Me.inCrossSect_tf1.Size = New System.Drawing.Size(63, 20)
        Me.inCrossSect_tf1.TabIndex = 51
        '
        'lblCsTf1_unit
        '
        Me.lblCsTf1_unit.AutoSize = True
        Me.lblCsTf1_unit.Location = New System.Drawing.Point(253, 331)
        Me.lblCsTf1_unit.Name = "lblCsTf1_unit"
        Me.lblCsTf1_unit.Size = New System.Drawing.Size(29, 13)
        Me.lblCsTf1_unit.TabIndex = 52
        Me.lblCsTf1_unit.Text = "[mm]"
        '
        'lblCsTf1
        '
        Me.lblCsTf1.AutoSize = True
        Me.lblCsTf1.Location = New System.Drawing.Point(151, 331)
        Me.lblCsTf1.Name = "lblCsTf1"
        Me.lblCsTf1.Size = New System.Drawing.Size(28, 13)
        Me.lblCsTf1.TabIndex = 50
        Me.lblCsTf1.Text = "tf1 ="
        '
        'inCrossSect_dia
        '
        Me.inCrossSect_dia.Location = New System.Drawing.Point(42, 328)
        Me.inCrossSect_dia.Name = "inCrossSect_dia"
        Me.inCrossSect_dia.Size = New System.Drawing.Size(63, 20)
        Me.inCrossSect_dia.TabIndex = 48
        '
        'lblCsDia_unit
        '
        Me.lblCsDia_unit.AutoSize = True
        Me.lblCsDia_unit.Location = New System.Drawing.Point(109, 331)
        Me.lblCsDia_unit.Name = "lblCsDia_unit"
        Me.lblCsDia_unit.Size = New System.Drawing.Size(29, 13)
        Me.lblCsDia_unit.TabIndex = 49
        Me.lblCsDia_unit.Text = "[mm]"
        '
        'lblCsDia
        '
        Me.lblCsDia.AutoSize = True
        Me.lblCsDia.Location = New System.Drawing.Point(6, 331)
        Me.lblCsDia.Name = "lblCsDia"
        Me.lblCsDia.Size = New System.Drawing.Size(35, 13)
        Me.lblCsDia.TabIndex = 47
        Me.lblCsDia.Text = "Dia. ="
        '
        'inCrossSect_bf2
        '
        Me.inCrossSect_bf2.Location = New System.Drawing.Point(188, 302)
        Me.inCrossSect_bf2.Name = "inCrossSect_bf2"
        Me.inCrossSect_bf2.Size = New System.Drawing.Size(63, 20)
        Me.inCrossSect_bf2.TabIndex = 45
        '
        'lblCsBf2_unit
        '
        Me.lblCsBf2_unit.AutoSize = True
        Me.lblCsBf2_unit.Location = New System.Drawing.Point(253, 305)
        Me.lblCsBf2_unit.Name = "lblCsBf2_unit"
        Me.lblCsBf2_unit.Size = New System.Drawing.Size(29, 13)
        Me.lblCsBf2_unit.TabIndex = 46
        Me.lblCsBf2_unit.Text = "[mm]"
        '
        'lblCsBf2
        '
        Me.lblCsBf2.AutoSize = True
        Me.lblCsBf2.Location = New System.Drawing.Point(151, 305)
        Me.lblCsBf2.Name = "lblCsBf2"
        Me.lblCsBf2.Size = New System.Drawing.Size(31, 13)
        Me.lblCsBf2.TabIndex = 44
        Me.lblCsBf2.Text = "bf2 ="
        '
        'inCrossSect_H
        '
        Me.inCrossSect_H.Location = New System.Drawing.Point(42, 302)
        Me.inCrossSect_H.Name = "inCrossSect_H"
        Me.inCrossSect_H.Size = New System.Drawing.Size(63, 20)
        Me.inCrossSect_H.TabIndex = 42
        '
        'lblCsH_unit
        '
        Me.lblCsH_unit.AutoSize = True
        Me.lblCsH_unit.Location = New System.Drawing.Point(109, 305)
        Me.lblCsH_unit.Name = "lblCsH_unit"
        Me.lblCsH_unit.Size = New System.Drawing.Size(29, 13)
        Me.lblCsH_unit.TabIndex = 43
        Me.lblCsH_unit.Text = "[mm]"
        '
        'lblCsH
        '
        Me.lblCsH.AutoSize = True
        Me.lblCsH.Location = New System.Drawing.Point(6, 305)
        Me.lblCsH.Name = "lblCsH"
        Me.lblCsH.Size = New System.Drawing.Size(24, 13)
        Me.lblCsH.TabIndex = 41
        Me.lblCsH.Text = "H ="
        '
        'inCrossSect_bf1
        '
        Me.inCrossSect_bf1.Location = New System.Drawing.Point(188, 276)
        Me.inCrossSect_bf1.Name = "inCrossSect_bf1"
        Me.inCrossSect_bf1.Size = New System.Drawing.Size(63, 20)
        Me.inCrossSect_bf1.TabIndex = 39
        '
        'lblCsBf1_unit
        '
        Me.lblCsBf1_unit.AutoSize = True
        Me.lblCsBf1_unit.Location = New System.Drawing.Point(253, 279)
        Me.lblCsBf1_unit.Name = "lblCsBf1_unit"
        Me.lblCsBf1_unit.Size = New System.Drawing.Size(29, 13)
        Me.lblCsBf1_unit.TabIndex = 40
        Me.lblCsBf1_unit.Text = "[mm]"
        '
        'lblCsBf1
        '
        Me.lblCsBf1.AutoSize = True
        Me.lblCsBf1.Location = New System.Drawing.Point(151, 279)
        Me.lblCsBf1.Name = "lblCsBf1"
        Me.lblCsBf1.Size = New System.Drawing.Size(31, 13)
        Me.lblCsBf1.TabIndex = 38
        Me.lblCsBf1.Text = "bf1 ="
        '
        'inCrossSect_bw
        '
        Me.inCrossSect_bw.Location = New System.Drawing.Point(42, 276)
        Me.inCrossSect_bw.Name = "inCrossSect_bw"
        Me.inCrossSect_bw.Size = New System.Drawing.Size(63, 20)
        Me.inCrossSect_bw.TabIndex = 36
        '
        'lblCsBw_unit
        '
        Me.lblCsBw_unit.AutoSize = True
        Me.lblCsBw_unit.Location = New System.Drawing.Point(109, 279)
        Me.lblCsBw_unit.Name = "lblCsBw_unit"
        Me.lblCsBw_unit.Size = New System.Drawing.Size(29, 13)
        Me.lblCsBw_unit.TabIndex = 37
        Me.lblCsBw_unit.Text = "[mm]"
        '
        'lblCsBw
        '
        Me.lblCsBw.AutoSize = True
        Me.lblCsBw.Location = New System.Drawing.Point(6, 279)
        Me.lblCsBw.Name = "lblCsBw"
        Me.lblCsBw.Size = New System.Drawing.Size(30, 13)
        Me.lblCsBw.TabIndex = 35
        Me.lblCsBw.Text = "bw ="
        '
        'csPicture
        '
        Me.csPicture.Location = New System.Drawing.Point(8, 19)
        Me.csPicture.Name = "csPicture"
        Me.csPicture.Size = New System.Drawing.Size(270, 183)
        Me.csPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.csPicture.TabIndex = 4
        Me.csPicture.TabStop = False
        '
        'csOptionIsection
        '
        Me.csOptionIsection.AutoSize = True
        Me.csOptionIsection.Location = New System.Drawing.Point(103, 232)
        Me.csOptionIsection.Name = "csOptionIsection"
        Me.csOptionIsection.Size = New System.Drawing.Size(67, 17)
        Me.csOptionIsection.TabIndex = 3
        Me.csOptionIsection.Text = "I-Section"
        Me.csOptionIsection.UseVisualStyleBackColor = True
        '
        'csOptionTsection
        '
        Me.csOptionTsection.AutoSize = True
        Me.csOptionTsection.Location = New System.Drawing.Point(6, 232)
        Me.csOptionTsection.Name = "csOptionTsection"
        Me.csOptionTsection.Size = New System.Drawing.Size(71, 17)
        Me.csOptionTsection.TabIndex = 2
        Me.csOptionTsection.Text = "T-Section"
        Me.csOptionTsection.UseVisualStyleBackColor = True
        '
        'csOptionCircle
        '
        Me.csOptionCircle.AutoSize = True
        Me.csOptionCircle.Location = New System.Drawing.Point(103, 209)
        Me.csOptionCircle.Name = "csOptionCircle"
        Me.csOptionCircle.Size = New System.Drawing.Size(51, 17)
        Me.csOptionCircle.TabIndex = 1
        Me.csOptionCircle.Text = "Circle"
        Me.csOptionCircle.UseVisualStyleBackColor = True
        '
        'csOptionRect
        '
        Me.csOptionRect.AutoSize = True
        Me.csOptionRect.Checked = True
        Me.csOptionRect.Location = New System.Drawing.Point(6, 209)
        Me.csOptionRect.Name = "csOptionRect"
        Me.csOptionRect.Size = New System.Drawing.Size(74, 17)
        Me.csOptionRect.TabIndex = 0
        Me.csOptionRect.TabStop = True
        Me.csOptionRect.Text = "Rectangle"
        Me.csOptionRect.UseVisualStyleBackColor = True
        '
        'btnExpandCsVar
        '
        Me.btnExpandCsVar.Location = New System.Drawing.Point(227, 229)
        Me.btnExpandCsVar.Name = "btnExpandCsVar"
        Me.btnExpandCsVar.Size = New System.Drawing.Size(37, 23)
        Me.btnExpandCsVar.TabIndex = 5
        Me.btnExpandCsVar.Tag = ""
        Me.btnExpandCsVar.Text = "▼"
        Me.MainTooltip.SetToolTip(Me.btnExpandCsVar, "Click to show/hide variables")
        Me.btnExpandCsVar.UseVisualStyleBackColor = True
        '
        'btnShrinkCsVar
        '
        Me.btnShrinkCsVar.Location = New System.Drawing.Point(227, 229)
        Me.btnShrinkCsVar.Name = "btnShrinkCsVar"
        Me.btnShrinkCsVar.Size = New System.Drawing.Size(37, 23)
        Me.btnShrinkCsVar.TabIndex = 6
        Me.btnShrinkCsVar.Tag = ""
        Me.btnShrinkCsVar.Text = "▲"
        Me.btnShrinkCsVar.UseVisualStyleBackColor = True
        Me.btnShrinkCsVar.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.inBeamDim_A)
        Me.GroupBox2.Controls.Add(Me.lblBeamDimA_unit)
        Me.GroupBox2.Controls.Add(Me.lblBeamDimA)
        Me.GroupBox2.Controls.Add(Me.inBeamDim_L)
        Me.GroupBox2.Controls.Add(Me.Label36)
        Me.GroupBox2.Controls.Add(Me.Label37)
        Me.GroupBox2.Controls.Add(Me.pointLoadOptionDistributed)
        Me.GroupBox2.Controls.Add(Me.pointLoadOptionTwo)
        Me.GroupBox2.Controls.Add(Me.pointLoadOptionOne)
        Me.GroupBox2.Controls.Add(Me.pointLoadPicture)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 272)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(285, 211)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Data for Beam Analysis"
        '
        'inBeamDim_A
        '
        Me.inBeamDim_A.Location = New System.Drawing.Point(188, 176)
        Me.inBeamDim_A.Name = "inBeamDim_A"
        Me.inBeamDim_A.Size = New System.Drawing.Size(63, 20)
        Me.inBeamDim_A.TabIndex = 33
        '
        'lblBeamDimA_unit
        '
        Me.lblBeamDimA_unit.AutoSize = True
        Me.lblBeamDimA_unit.Location = New System.Drawing.Point(253, 179)
        Me.lblBeamDimA_unit.Name = "lblBeamDimA_unit"
        Me.lblBeamDimA_unit.Size = New System.Drawing.Size(29, 13)
        Me.lblBeamDimA_unit.TabIndex = 34
        Me.lblBeamDimA_unit.Text = "[mm]"
        '
        'lblBeamDimA
        '
        Me.lblBeamDimA.AutoSize = True
        Me.lblBeamDimA.Location = New System.Drawing.Point(151, 179)
        Me.lblBeamDimA.Name = "lblBeamDimA"
        Me.lblBeamDimA.Size = New System.Drawing.Size(22, 13)
        Me.lblBeamDimA.TabIndex = 32
        Me.lblBeamDimA.Text = "a ="
        '
        'inBeamDim_L
        '
        Me.inBeamDim_L.Location = New System.Drawing.Point(42, 175)
        Me.inBeamDim_L.Name = "inBeamDim_L"
        Me.inBeamDim_L.Size = New System.Drawing.Size(63, 20)
        Me.inBeamDim_L.TabIndex = 30
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(109, 178)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(29, 13)
        Me.Label36.TabIndex = 31
        Me.Label36.Text = "[mm]"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(6, 179)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(22, 13)
        Me.Label37.TabIndex = 29
        Me.Label37.Text = "L ="
        '
        'pointLoadOptionDistributed
        '
        Me.pointLoadOptionDistributed.AutoSize = True
        Me.pointLoadOptionDistributed.Location = New System.Drawing.Point(189, 146)
        Me.pointLoadOptionDistributed.Name = "pointLoadOptionDistributed"
        Me.pointLoadOptionDistributed.Size = New System.Drawing.Size(75, 17)
        Me.pointLoadOptionDistributed.TabIndex = 3
        Me.pointLoadOptionDistributed.Text = "Distributed"
        Me.pointLoadOptionDistributed.UseVisualStyleBackColor = True
        '
        'pointLoadOptionTwo
        '
        Me.pointLoadOptionTwo.AutoSize = True
        Me.pointLoadOptionTwo.Location = New System.Drawing.Point(100, 146)
        Me.pointLoadOptionTwo.Name = "pointLoadOptionTwo"
        Me.pointLoadOptionTwo.Size = New System.Drawing.Size(73, 17)
        Me.pointLoadOptionTwo.TabIndex = 2
        Me.pointLoadOptionTwo.Text = "Two Point"
        Me.pointLoadOptionTwo.UseVisualStyleBackColor = True
        '
        'pointLoadOptionOne
        '
        Me.pointLoadOptionOne.AutoSize = True
        Me.pointLoadOptionOne.Checked = True
        Me.pointLoadOptionOne.Location = New System.Drawing.Point(6, 146)
        Me.pointLoadOptionOne.Name = "pointLoadOptionOne"
        Me.pointLoadOptionOne.Size = New System.Drawing.Size(72, 17)
        Me.pointLoadOptionOne.TabIndex = 1
        Me.pointLoadOptionOne.TabStop = True
        Me.pointLoadOptionOne.Text = "One Point"
        Me.pointLoadOptionOne.UseVisualStyleBackColor = True
        '
        'pointLoadPicture
        '
        Me.pointLoadPicture.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pointLoadPicture.InitialImage = Nothing
        Me.pointLoadPicture.Location = New System.Drawing.Point(7, 19)
        Me.pointLoadPicture.Name = "pointLoadPicture"
        Me.pointLoadPicture.Size = New System.Drawing.Size(270, 124)
        Me.pointLoadPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pointLoadPicture.TabIndex = 0
        Me.pointLoadPicture.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.uipicPrestressed)
        Me.GroupBox3.Controls.Add(Me.uipicFRP)
        Me.GroupBox3.Controls.Add(Me.uipicSteel)
        Me.GroupBox3.Controls.Add(Me.btnReinfPrestressed)
        Me.GroupBox3.Controls.Add(Me.btnReinfFRP)
        Me.GroupBox3.Controls.Add(Me.btnReinfSteel)
        Me.GroupBox3.Location = New System.Drawing.Point(294, 169)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(278, 142)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Longitudal Reinforcement Data"
        '
        'uipicPrestressed
        '
        Me.uipicPrestressed.Location = New System.Drawing.Point(216, 101)
        Me.uipicPrestressed.Name = "uipicPrestressed"
        Me.uipicPrestressed.Size = New System.Drawing.Size(34, 32)
        Me.uipicPrestressed.TabIndex = 9
        Me.uipicPrestressed.TabStop = False
        '
        'uipicFRP
        '
        Me.uipicFRP.Location = New System.Drawing.Point(216, 61)
        Me.uipicFRP.Name = "uipicFRP"
        Me.uipicFRP.Size = New System.Drawing.Size(34, 32)
        Me.uipicFRP.TabIndex = 8
        Me.uipicFRP.TabStop = False
        '
        'uipicSteel
        '
        Me.uipicSteel.Location = New System.Drawing.Point(216, 22)
        Me.uipicSteel.Name = "uipicSteel"
        Me.uipicSteel.Size = New System.Drawing.Size(34, 32)
        Me.uipicSteel.TabIndex = 7
        Me.uipicSteel.TabStop = False
        '
        'btnReinfPrestressed
        '
        Me.btnReinfPrestressed.Location = New System.Drawing.Point(9, 100)
        Me.btnReinfPrestressed.Name = "btnReinfPrestressed"
        Me.btnReinfPrestressed.Size = New System.Drawing.Size(198, 33)
        Me.btnReinfPrestressed.TabIndex = 2
        Me.btnReinfPrestressed.Text = "Prestressed Reinforcement"
        Me.btnReinfPrestressed.UseVisualStyleBackColor = True
        '
        'btnReinfFRP
        '
        Me.btnReinfFRP.Location = New System.Drawing.Point(9, 61)
        Me.btnReinfFRP.Name = "btnReinfFRP"
        Me.btnReinfFRP.Size = New System.Drawing.Size(198, 33)
        Me.btnReinfFRP.TabIndex = 1
        Me.btnReinfFRP.Text = "FRP Reinforcement"
        Me.btnReinfFRP.UseVisualStyleBackColor = True
        '
        'btnReinfSteel
        '
        Me.btnReinfSteel.Location = New System.Drawing.Point(9, 22)
        Me.btnReinfSteel.Name = "btnReinfSteel"
        Me.btnReinfSteel.Size = New System.Drawing.Size(198, 33)
        Me.btnReinfSteel.TabIndex = 0
        Me.btnReinfSteel.Text = "Steel Reinforcement"
        Me.btnReinfSteel.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.inCMP_MaxCompStrain)
        Me.GroupBox4.Controls.Add(Me.inCMP_TensileStrength)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.inCMP_CompStrength)
        Me.GroupBox4.Controls.Add(Me.inCMP_TensionStiffEffect)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.inCMP_ConcreteSSModel)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Location = New System.Drawing.Point(293, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(279, 160)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Concrete Material Properties"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(219, 134)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "[mm/mm]"
        '
        'inCMP_MaxCompStrain
        '
        Me.inCMP_MaxCompStrain.Location = New System.Drawing.Point(154, 131)
        Me.inCMP_MaxCompStrain.Name = "inCMP_MaxCompStrain"
        Me.inCMP_MaxCompStrain.Size = New System.Drawing.Size(63, 20)
        Me.inCMP_MaxCompStrain.TabIndex = 9
        Me.inCMP_MaxCompStrain.Text = "0.003"
        '
        'inCMP_TensileStrength
        '
        Me.inCMP_TensileStrength.Location = New System.Drawing.Point(154, 105)
        Me.inCMP_TensileStrength.Name = "inCMP_TensileStrength"
        Me.inCMP_TensileStrength.Size = New System.Drawing.Size(63, 20)
        Me.inCMP_TensileStrength.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(219, 108)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "[MPa]"
        '
        'inCMP_CompStrength
        '
        Me.inCMP_CompStrength.Location = New System.Drawing.Point(154, 79)
        Me.inCMP_CompStrength.Name = "inCMP_CompStrength"
        Me.inCMP_CompStrength.Size = New System.Drawing.Size(63, 20)
        Me.inCMP_CompStrength.TabIndex = 7
        Me.MainTooltip.SetToolTip(Me.inCMP_CompStrength, "Change this and tensile strength will be auto computed by 0.6 * sqrt(fc)")
        '
        'inCMP_TensionStiffEffect
        '
        Me.inCMP_TensionStiffEffect.FormattingEnabled = True
        Me.inCMP_TensionStiffEffect.Items.AddRange(New Object() {"Exponential Model", "Linear Model", "NTS Model"})
        Me.inCMP_TensionStiffEffect.Location = New System.Drawing.Point(154, 51)
        Me.inCMP_TensionStiffEffect.Name = "inCMP_TensionStiffEffect"
        Me.inCMP_TensionStiffEffect.Size = New System.Drawing.Size(119, 21)
        Me.inCMP_TensionStiffEffect.TabIndex = 6
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(219, 82)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 13)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "[MPa]"
        '
        'inCMP_ConcreteSSModel
        '
        Me.inCMP_ConcreteSSModel.FormattingEnabled = True
        Me.inCMP_ConcreteSSModel.Items.AddRange(New Object() {"Mander Model", "Almusallam Model", "Hognestad Model", "Todeschini Model", "Kent&Park Model", "Concrete Model 1", "Concrete Model 2", "Concrete Model 3", "Concrete Model 4", "High Strength Con. 1", "High Strength Con. 2", "Cyclic Con. Model 1", "Cyclic Con. Model 2", "Linear Model"})
        Me.inCMP_ConcreteSSModel.Location = New System.Drawing.Point(154, 25)
        Me.inCMP_ConcreteSSModel.Name = "inCMP_ConcreteSSModel"
        Me.inCMP_ConcreteSSModel.Size = New System.Drawing.Size(119, 21)
        Me.inCMP_ConcreteSSModel.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 134)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(138, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Maximum Comp. Strain      ="
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(7, 108)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(138, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Tensile Strength, ft            ="
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 82)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(139, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Compression Strength, fc'  ="
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(138, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Tension Stress-Strain Curve"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(130, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Comp. Stress-Strain Curve"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.uipicStrCfnmt)
        Me.GroupBox5.Controls.Add(Me.uipicStrShear)
        Me.GroupBox5.Controls.Add(Me.uipicStrFlex)
        Me.GroupBox5.Controls.Add(Me.btnStrCnfmt)
        Me.GroupBox5.Controls.Add(Me.btnStrShear)
        Me.GroupBox5.Controls.Add(Me.btnStrFlex)
        Me.GroupBox5.Location = New System.Drawing.Point(578, 3)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(207, 160)
        Me.GroupBox5.TabIndex = 8
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Type of Strengthening"
        '
        'uipicStrCfnmt
        '
        Me.uipicStrCfnmt.Location = New System.Drawing.Point(167, 110)
        Me.uipicStrCfnmt.Name = "uipicStrCfnmt"
        Me.uipicStrCfnmt.Size = New System.Drawing.Size(34, 32)
        Me.uipicStrCfnmt.TabIndex = 45
        Me.uipicStrCfnmt.TabStop = False
        '
        'uipicStrShear
        '
        Me.uipicStrShear.Location = New System.Drawing.Point(167, 66)
        Me.uipicStrShear.Name = "uipicStrShear"
        Me.uipicStrShear.Size = New System.Drawing.Size(34, 32)
        Me.uipicStrShear.TabIndex = 44
        Me.uipicStrShear.TabStop = False
        '
        'uipicStrFlex
        '
        Me.uipicStrFlex.Location = New System.Drawing.Point(167, 22)
        Me.uipicStrFlex.Name = "uipicStrFlex"
        Me.uipicStrFlex.Size = New System.Drawing.Size(34, 32)
        Me.uipicStrFlex.TabIndex = 43
        Me.uipicStrFlex.TabStop = False
        '
        'btnStrCnfmt
        '
        Me.btnStrCnfmt.Location = New System.Drawing.Point(9, 110)
        Me.btnStrCnfmt.Name = "btnStrCnfmt"
        Me.btnStrCnfmt.Size = New System.Drawing.Size(152, 33)
        Me.btnStrCnfmt.TabIndex = 28
        Me.btnStrCnfmt.Text = "Confinement"
        Me.btnStrCnfmt.UseVisualStyleBackColor = True
        '
        'btnStrShear
        '
        Me.btnStrShear.Location = New System.Drawing.Point(9, 66)
        Me.btnStrShear.Name = "btnStrShear"
        Me.btnStrShear.Size = New System.Drawing.Size(152, 33)
        Me.btnStrShear.TabIndex = 27
        Me.btnStrShear.Text = "Shear"
        Me.btnStrShear.UseVisualStyleBackColor = True
        '
        'btnStrFlex
        '
        Me.btnStrFlex.Location = New System.Drawing.Point(9, 22)
        Me.btnStrFlex.Name = "btnStrFlex"
        Me.btnStrFlex.Size = New System.Drawing.Size(152, 33)
        Me.btnStrFlex.TabIndex = 26
        Me.btnStrFlex.Text = "Flexural"
        Me.btnStrFlex.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.uipicSteelConfine)
        Me.GroupBox6.Controls.Add(Me.uipicTransReinf)
        Me.GroupBox6.Controls.Add(Me.btnSteelConfine)
        Me.GroupBox6.Controls.Add(Me.btnTransReinf)
        Me.GroupBox6.Location = New System.Drawing.Point(294, 317)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(278, 100)
        Me.GroupBox6.TabIndex = 9
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Transverse Reinforcement Data"
        '
        'uipicSteelConfine
        '
        Me.uipicSteelConfine.Location = New System.Drawing.Point(216, 57)
        Me.uipicSteelConfine.Name = "uipicSteelConfine"
        Me.uipicSteelConfine.Size = New System.Drawing.Size(34, 32)
        Me.uipicSteelConfine.TabIndex = 43
        Me.uipicSteelConfine.TabStop = False
        '
        'uipicTransReinf
        '
        Me.uipicTransReinf.Location = New System.Drawing.Point(216, 19)
        Me.uipicTransReinf.Name = "uipicTransReinf"
        Me.uipicTransReinf.Size = New System.Drawing.Size(34, 32)
        Me.uipicTransReinf.TabIndex = 42
        Me.uipicTransReinf.TabStop = False
        '
        'btnSteelConfine
        '
        Me.btnSteelConfine.Location = New System.Drawing.Point(9, 57)
        Me.btnSteelConfine.Name = "btnSteelConfine"
        Me.btnSteelConfine.Size = New System.Drawing.Size(198, 32)
        Me.btnSteelConfine.TabIndex = 41
        Me.btnSteelConfine.Text = "Steel Confinement"
        Me.btnSteelConfine.UseVisualStyleBackColor = True
        '
        'btnTransReinf
        '
        Me.btnTransReinf.Location = New System.Drawing.Point(9, 19)
        Me.btnTransReinf.Name = "btnTransReinf"
        Me.btnTransReinf.Size = New System.Drawing.Size(198, 32)
        Me.btnTransReinf.TabIndex = 40
        Me.btnTransReinf.Text = "Transverse Reinforcement"
        Me.btnTransReinf.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.inDataAnly_N)
        Me.GroupBox7.Controls.Add(Me.Label33)
        Me.GroupBox7.Controls.Add(Me.inDataAnly_P)
        Me.GroupBox7.Controls.Add(Me.Label22)
        Me.GroupBox7.Controls.Add(Me.Label32)
        Me.GroupBox7.Location = New System.Drawing.Point(578, 169)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(207, 74)
        Me.GroupBox7.TabIndex = 10
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Data for Column Analysis"
        '
        'inDataAnly_N
        '
        Me.inDataAnly_N.Location = New System.Drawing.Point(65, 47)
        Me.inDataAnly_N.Name = "inDataAnly_N"
        Me.inDataAnly_N.Size = New System.Drawing.Size(65, 20)
        Me.inDataAnly_N.TabIndex = 38
        Me.MainTooltip.SetToolTip(Me.inDataAnly_N, "Only one of P and Ni can be filled")
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(8, 51)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(26, 13)
        Me.Label33.TabIndex = 37
        Me.Label33.Text = "Ni ="
        Me.MainTooltip.SetToolTip(Me.Label33, "N of Point in Interaction Diagram")
        '
        'inDataAnly_P
        '
        Me.inDataAnly_P.Location = New System.Drawing.Point(65, 22)
        Me.inDataAnly_P.Name = "inDataAnly_P"
        Me.inDataAnly_P.Size = New System.Drawing.Size(65, 20)
        Me.inDataAnly_P.TabIndex = 30
        Me.MainTooltip.SetToolTip(Me.inDataAnly_P, "Only one of P and Ni can be filled")
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(134, 25)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(27, 13)
        Me.Label22.TabIndex = 31
        Me.Label22.Text = "[kN]"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(8, 25)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(26, 13)
        Me.Label32.TabIndex = 29
        Me.Label32.Text = "P  ="
        '
        'inCurv_incr
        '
        Me.inCurv_incr.Location = New System.Drawing.Point(96, 22)
        Me.inCurv_incr.Name = "inCurv_incr"
        Me.inCurv_incr.Size = New System.Drawing.Size(63, 20)
        Me.inCurv_incr.TabIndex = 40
        Me.inCurv_incr.Text = "0.000001"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(162, 25)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(36, 13)
        Me.Label34.TabIndex = 41
        Me.Label34.Text = "[mm⁻¹]"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(6, 25)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(86, 13)
        Me.Label35.TabIndex = 39
        Me.Label35.Text = "Incr. Curvature ="
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.inCurv_max)
        Me.GroupBox8.Controls.Add(Me.Label38)
        Me.GroupBox8.Controls.Add(Me.Label39)
        Me.GroupBox8.Controls.Add(Me.inCurv_incr)
        Me.GroupBox8.Controls.Add(Me.Label34)
        Me.GroupBox8.Controls.Add(Me.Label35)
        Me.GroupBox8.Location = New System.Drawing.Point(578, 249)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(207, 87)
        Me.GroupBox8.TabIndex = 42
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Curvature Parameter"
        '
        'inCurv_max
        '
        Me.inCurv_max.Location = New System.Drawing.Point(96, 52)
        Me.inCurv_max.Name = "inCurv_max"
        Me.inCurv_max.Size = New System.Drawing.Size(63, 20)
        Me.inCurv_max.TabIndex = 43
        Me.inCurv_max.Text = "0.1"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(162, 56)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(36, 13)
        Me.Label38.TabIndex = 44
        Me.Label38.Text = "[mm⁻¹]"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(6, 55)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(88, 13)
        Me.Label39.TabIndex = 42
        Me.Label39.Text = "Max. Curvature ="
        '
        'panelInput
        '
        Me.panelInput.Controls.Add(Me.GroupBox1)
        Me.panelInput.Controls.Add(Me.gbRepLoadData)
        Me.panelInput.Controls.Add(Me.GroupBox8)
        Me.panelInput.Controls.Add(Me.GroupBox7)
        Me.panelInput.Controls.Add(Me.GroupBox6)
        Me.panelInput.Controls.Add(Me.GroupBox5)
        Me.panelInput.Controls.Add(Me.GroupBox4)
        Me.panelInput.Controls.Add(Me.GroupBox3)
        Me.panelInput.Controls.Add(Me.GroupBoxCrossSection)
        Me.panelInput.Controls.Add(Me.GroupBox2)
        Me.panelInput.Location = New System.Drawing.Point(0, 6)
        Me.panelInput.Name = "panelInput"
        Me.panelInput.Size = New System.Drawing.Size(792, 485)
        Me.panelInput.TabIndex = 43
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.inAnlyRepeated)
        Me.GroupBox1.Controls.Add(Me.inAnlyNegative)
        Me.GroupBox1.Controls.Add(Me.inAnlyPositive)
        Me.GroupBox1.Location = New System.Drawing.Point(578, 343)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(206, 139)
        Me.GroupBox1.TabIndex = 44
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Type of Analysis"
        '
        'inAnlyRepeated
        '
        Me.inAnlyRepeated.AutoSize = True
        Me.inAnlyRepeated.Location = New System.Drawing.Point(11, 101)
        Me.inAnlyRepeated.Name = "inAnlyRepeated"
        Me.inAnlyRepeated.Size = New System.Drawing.Size(80, 17)
        Me.inAnlyRepeated.TabIndex = 2
        Me.inAnlyRepeated.TabStop = True
        Me.inAnlyRepeated.Text = "Cyclic Load"
        Me.inAnlyRepeated.UseVisualStyleBackColor = True
        '
        'inAnlyNegative
        '
        Me.inAnlyNegative.AutoSize = True
        Me.inAnlyNegative.Location = New System.Drawing.Point(11, 65)
        Me.inAnlyNegative.Name = "inAnlyNegative"
        Me.inAnlyNegative.Size = New System.Drawing.Size(109, 17)
        Me.inAnlyNegative.TabIndex = 1
        Me.inAnlyNegative.TabStop = True
        Me.inAnlyNegative.Text = "Negative Moment"
        Me.inAnlyNegative.UseVisualStyleBackColor = True
        '
        'inAnlyPositive
        '
        Me.inAnlyPositive.AutoSize = True
        Me.inAnlyPositive.Location = New System.Drawing.Point(11, 29)
        Me.inAnlyPositive.Name = "inAnlyPositive"
        Me.inAnlyPositive.Size = New System.Drawing.Size(103, 17)
        Me.inAnlyPositive.TabIndex = 0
        Me.inAnlyPositive.TabStop = True
        Me.inAnlyPositive.Text = "Positive Moment"
        Me.inAnlyPositive.UseVisualStyleBackColor = True
        '
        'gbRepLoadData
        '
        Me.gbRepLoadData.Controls.Add(Me.uipicRepLoadData)
        Me.gbRepLoadData.Controls.Add(Me.btnRepLoadData)
        Me.gbRepLoadData.Location = New System.Drawing.Point(293, 423)
        Me.gbRepLoadData.Name = "gbRepLoadData"
        Me.gbRepLoadData.Size = New System.Drawing.Size(277, 60)
        Me.gbRepLoadData.TabIndex = 43
        Me.gbRepLoadData.TabStop = False
        Me.gbRepLoadData.Text = "Cyclic Load Analysis"
        '
        'uipicRepLoadData
        '
        Me.uipicRepLoadData.Location = New System.Drawing.Point(217, 19)
        Me.uipicRepLoadData.Name = "uipicRepLoadData"
        Me.uipicRepLoadData.Size = New System.Drawing.Size(34, 32)
        Me.uipicRepLoadData.TabIndex = 9
        Me.uipicRepLoadData.TabStop = False
        '
        'btnRepLoadData
        '
        Me.btnRepLoadData.Location = New System.Drawing.Point(9, 19)
        Me.btnRepLoadData.Name = "btnRepLoadData"
        Me.btnRepLoadData.Size = New System.Drawing.Size(198, 33)
        Me.btnRepLoadData.TabIndex = 8
        Me.btnRepLoadData.Text = "Cyclic Load Data"
        Me.btnRepLoadData.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabPage_splash)
        Me.TabControl1.Controls.Add(Me.tabPage_input)
        Me.TabControl1.Controls.Add(Me.tabPage_chartMatModel)
        Me.TabControl1.Controls.Add(Me.tabPage_main)
        Me.TabControl1.ImageList = Me.ImageList1
        Me.TabControl1.Location = New System.Drawing.Point(2, 52)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(800, 534)
        Me.TabControl1.TabIndex = 44
        '
        'tabPage_splash
        '
        Me.tabPage_splash.Controls.Add(Me.Label40)
        Me.tabPage_splash.Controls.Add(Me.ApplicationTitle)
        Me.tabPage_splash.Controls.Add(Me.PictureBox1)
        Me.tabPage_splash.Controls.Add(Me.DetailsLayoutPanel)
        Me.tabPage_splash.Location = New System.Drawing.Point(4, 39)
        Me.tabPage_splash.Name = "tabPage_splash"
        Me.tabPage_splash.Size = New System.Drawing.Size(792, 491)
        Me.tabPage_splash.TabIndex = 2
        Me.tabPage_splash.UseVisualStyleBackColor = True
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(125, 134)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(324, 27)
        Me.Label40.TabIndex = 6
        Me.Label40.Text = "by Rendy Thamrin and  Associates"
        '
        'ApplicationTitle
        '
        Me.ApplicationTitle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ApplicationTitle.BackColor = System.Drawing.Color.Transparent
        Me.ApplicationTitle.Font = New System.Drawing.Font("Trebuchet MS", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ApplicationTitle.Location = New System.Drawing.Point(121, 13)
        Me.ApplicationTitle.Name = "ApplicationTitle"
        Me.ApplicationTitle.Size = New System.Drawing.Size(664, 105)
        Me.ApplicationTitle.TabIndex = 3
        Me.ApplicationTitle.Text = "Reinforced Concrete Cross Section Analysis (RCCSA)"
        Me.ApplicationTitle.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.RCCSA.My.Resources.Resources.Logo_UNAND
        Me.PictureBox1.Location = New System.Drawing.Point(128, 267)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(161, 189)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'DetailsLayoutPanel
        '
        Me.DetailsLayoutPanel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.DetailsLayoutPanel.BackColor = System.Drawing.Color.Transparent
        Me.DetailsLayoutPanel.ColumnCount = 1
        Me.DetailsLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 433.0!))
        Me.DetailsLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 142.0!))
        Me.DetailsLayoutPanel.Controls.Add(Me.Version, 0, 0)
        Me.DetailsLayoutPanel.Controls.Add(Me.Copyright, 0, 1)
        Me.DetailsLayoutPanel.Location = New System.Drawing.Point(295, 320)
        Me.DetailsLayoutPanel.Name = "DetailsLayoutPanel"
        Me.DetailsLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.58824!))
        Me.DetailsLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.41177!))
        Me.DetailsLayoutPanel.Size = New System.Drawing.Size(433, 136)
        Me.DetailsLayoutPanel.TabIndex = 4
        '
        'Version
        '
        Me.Version.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Version.BackColor = System.Drawing.Color.Transparent
        Me.Version.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Version.Location = New System.Drawing.Point(16, 4)
        Me.Version.Name = "Version"
        Me.Version.Size = New System.Drawing.Size(401, 20)
        Me.Version.TabIndex = 1
        Me.Version.Text = "Version {0}.{1:0}"
        '
        'Copyright
        '
        Me.Copyright.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Copyright.BackColor = System.Drawing.Color.Transparent
        Me.Copyright.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Copyright.Location = New System.Drawing.Point(17, 42)
        Me.Copyright.Name = "Copyright"
        Me.Copyright.Size = New System.Drawing.Size(399, 80)
        Me.Copyright.TabIndex = 2
        Me.Copyright.Text = "Copyright © 2013-2014, Civil Engineering Department" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Faculty of Engineering, Anda" & _
    "las University" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Padang, INDONESIA"
        '
        'tabPage_input
        '
        Me.tabPage_input.Controls.Add(Me.panelInput)
        Me.tabPage_input.ImageKey = "pencil-32.png"
        Me.tabPage_input.Location = New System.Drawing.Point(4, 39)
        Me.tabPage_input.Name = "tabPage_input"
        Me.tabPage_input.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPage_input.Size = New System.Drawing.Size(792, 491)
        Me.tabPage_input.TabIndex = 0
        Me.tabPage_input.UseVisualStyleBackColor = True
        '
        'tabPage_chartMatModel
        '
        Me.tabPage_chartMatModel.Controls.Add(Me.Label41)
        Me.tabPage_chartMatModel.Controls.Add(Me.optChartMatModel)
        Me.tabPage_chartMatModel.Controls.Add(Me.DgvMatModel)
        Me.tabPage_chartMatModel.Controls.Add(Me.ChartMatModel)
        Me.tabPage_chartMatModel.Controls.Add(Me.btnShowLoP)
        Me.tabPage_chartMatModel.Controls.Add(Me.btnChartMatModelUpdate)
        Me.tabPage_chartMatModel.ImageKey = "Chart-Bar-Blue.png"
        Me.tabPage_chartMatModel.Location = New System.Drawing.Point(4, 39)
        Me.tabPage_chartMatModel.Name = "tabPage_chartMatModel"
        Me.tabPage_chartMatModel.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPage_chartMatModel.Size = New System.Drawing.Size(792, 491)
        Me.tabPage_chartMatModel.TabIndex = 1
        Me.MainTooltip.SetToolTip(Me.tabPage_chartMatModel, "Chart Material Model")
        Me.tabPage_chartMatModel.UseVisualStyleBackColor = True
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(31, 461)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(100, 16)
        Me.Label41.TabIndex = 13
        Me.Label41.Text = "Material Model:"
        '
        'optChartMatModel
        '
        Me.optChartMatModel.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optChartMatModel.FormattingEnabled = True
        Me.optChartMatModel.ItemHeight = 20
        Me.optChartMatModel.Location = New System.Drawing.Point(150, 455)
        Me.optChartMatModel.Name = "optChartMatModel"
        Me.optChartMatModel.Size = New System.Drawing.Size(390, 28)
        Me.optChartMatModel.TabIndex = 10
        '
        'DgvMatModel
        '
        Me.DgvMatModel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvMatModel.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClmX, Me.ClmY})
        Me.DgvMatModel.Location = New System.Drawing.Point(589, 3)
        Me.DgvMatModel.Name = "DgvMatModel"
        Me.DgvMatModel.Size = New System.Drawing.Size(196, 437)
        Me.DgvMatModel.TabIndex = 7
        Me.DgvMatModel.Visible = False
        '
        'ClmX
        '
        Me.ClmX.HeaderText = "X"
        Me.ClmX.Name = "ClmX"
        Me.ClmX.ReadOnly = True
        Me.ClmX.Width = 65
        '
        'ClmY
        '
        Me.ClmY.HeaderText = "Y"
        Me.ClmY.Name = "ClmY"
        Me.ClmY.ReadOnly = True
        Me.ClmY.Width = 65
        '
        'ChartMatModel
        '
        ChartArea1.AxisX.TitleFont = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea1.AxisY.TitleFont = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea1.Name = "ChartArea1"
        Me.ChartMatModel.ChartAreas.Add(ChartArea1)
        Me.ChartMatModel.ContextMenuStrip = Me.ContextMenuChart
        Legend1.Enabled = False
        Legend1.Name = "Legend1"
        Me.ChartMatModel.Legends.Add(Legend1)
        Me.ChartMatModel.Location = New System.Drawing.Point(0, 0)
        Me.ChartMatModel.Name = "ChartMatModel"
        Me.ChartMatModel.Size = New System.Drawing.Size(786, 440)
        Me.ChartMatModel.TabIndex = 0
        Me.ChartMatModel.Text = "Chart1"
        Title1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Title1.Name = "Title1"
        Me.ChartMatModel.Titles.Add(Title1)
        '
        'ContextMenuChart
        '
        Me.ContextMenuChart.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CmChartMenu_ResetChart, Me.ToolStripMenuItem3, Me.CmChartMenu_Options, Me.ToolStripMenuItem4, Me.cmChartMenu_flipXaxis, Me.cmChartMenu_flipYaxis})
        Me.ContextMenuChart.Name = "ContextMenuChart"
        Me.ContextMenuChart.Size = New System.Drawing.Size(135, 104)
        '
        'CmChartMenu_ResetChart
        '
        Me.CmChartMenu_ResetChart.Name = "CmChartMenu_ResetChart"
        Me.CmChartMenu_ResetChart.Size = New System.Drawing.Size(134, 22)
        Me.CmChartMenu_ResetChart.Text = "Reset Chart"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(131, 6)
        '
        'CmChartMenu_Options
        '
        Me.CmChartMenu_Options.Name = "CmChartMenu_Options"
        Me.CmChartMenu_Options.Size = New System.Drawing.Size(134, 22)
        Me.CmChartMenu_Options.Text = "Options"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(131, 6)
        '
        'cmChartMenu_flipXaxis
        '
        Me.cmChartMenu_flipXaxis.Name = "cmChartMenu_flipXaxis"
        Me.cmChartMenu_flipXaxis.Size = New System.Drawing.Size(134, 22)
        Me.cmChartMenu_flipXaxis.Text = "Flip X-Axis"
        '
        'cmChartMenu_flipYaxis
        '
        Me.cmChartMenu_flipYaxis.Name = "cmChartMenu_flipYaxis"
        Me.cmChartMenu_flipYaxis.Size = New System.Drawing.Size(134, 22)
        Me.cmChartMenu_flipYaxis.Text = "Flip Y-Axis"
        '
        'btnShowLoP
        '
        Me.btnShowLoP.Appearance = System.Windows.Forms.Appearance.Button
        Me.btnShowLoP.AutoSize = True
        Me.btnShowLoP.ImageKey = "Table-32.png"
        Me.btnShowLoP.ImageList = Me.ImageList1
        Me.btnShowLoP.Location = New System.Drawing.Point(747, 450)
        Me.btnShowLoP.Name = "btnShowLoP"
        Me.btnShowLoP.Size = New System.Drawing.Size(38, 38)
        Me.btnShowLoP.TabIndex = 12
        Me.btnShowLoP.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Chart.png")
        Me.ImageList1.Images.SetKeyName(1, "Chart-Bar-UI.png")
        Me.ImageList1.Images.SetKeyName(2, "Chart-Bar-Blue.png")
        Me.ImageList1.Images.SetKeyName(3, "pencil-32.png")
        Me.ImageList1.Images.SetKeyName(4, "Chart-Bar-Table-32.png")
        Me.ImageList1.Images.SetKeyName(5, "Chart-Curve-32.png")
        Me.ImageList1.Images.SetKeyName(6, "Chart-Stock-32.png")
        Me.ImageList1.Images.SetKeyName(7, "Table-32.png")
        Me.ImageList1.Images.SetKeyName(8, "Update-32.png")
        Me.ImageList1.Images.SetKeyName(9, "RUN-32.png")
        Me.ImageList1.Images.SetKeyName(10, "Button.png")
        Me.ImageList1.Images.SetKeyName(11, "Build2-32.png")
        '
        'btnChartMatModelUpdate
        '
        Me.btnChartMatModelUpdate.ImageKey = "Update-32.png"
        Me.btnChartMatModelUpdate.ImageList = Me.ImageList1
        Me.btnChartMatModelUpdate.Location = New System.Drawing.Point(550, 450)
        Me.btnChartMatModelUpdate.Name = "btnChartMatModelUpdate"
        Me.btnChartMatModelUpdate.Size = New System.Drawing.Size(98, 38)
        Me.btnChartMatModelUpdate.TabIndex = 11
        Me.btnChartMatModelUpdate.Text = "  Update"
        Me.btnChartMatModelUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnChartMatModelUpdate.UseVisualStyleBackColor = True
        '
        'tabPage_main
        '
        Me.tabPage_main.Controls.Add(Me.Label45)
        Me.tabPage_main.Controls.Add(Me.optChartMain)
        Me.tabPage_main.Controls.Add(Me.DgvMain)
        Me.tabPage_main.Controls.Add(Me.ChartMain)
        Me.tabPage_main.Controls.Add(Me.btnChartMainUpdate)
        Me.tabPage_main.Controls.Add(Me.btnShowButtonMain)
        Me.tabPage_main.Controls.Add(Me.btnShowLoPMain)
        Me.tabPage_main.ImageKey = "Chart-Stock-32.png"
        Me.tabPage_main.Location = New System.Drawing.Point(4, 39)
        Me.tabPage_main.Name = "tabPage_main"
        Me.tabPage_main.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPage_main.Size = New System.Drawing.Size(792, 491)
        Me.tabPage_main.TabIndex = 6
        Me.tabPage_main.Text = "Chart (Empty)"
        Me.tabPage_main.UseVisualStyleBackColor = True
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(66, 461)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(65, 16)
        Me.Label45.TabIndex = 49
        Me.Label45.Text = "Graphics:"
        '
        'optChartMain
        '
        Me.optChartMain.Font = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optChartMain.FormattingEnabled = True
        Me.optChartMain.ItemHeight = 20
        Me.optChartMain.Location = New System.Drawing.Point(150, 455)
        Me.optChartMain.Name = "optChartMain"
        Me.optChartMain.Size = New System.Drawing.Size(390, 28)
        Me.optChartMain.TabIndex = 47
        '
        'DgvMain
        '
        Me.DgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvMain.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8})
        Me.DgvMain.Location = New System.Drawing.Point(589, 3)
        Me.DgvMain.Name = "DgvMain"
        Me.DgvMain.Size = New System.Drawing.Size(196, 437)
        Me.DgvMain.TabIndex = 44
        Me.DgvMain.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "X"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 65
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Y"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 65
        '
        'ChartMain
        '
        ChartArea2.AxisX.TitleFont = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea2.AxisY.TitleFont = New System.Drawing.Font("Trebuchet MS", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea2.Name = "ChartArea1"
        Me.ChartMain.ChartAreas.Add(ChartArea2)
        Me.ChartMain.ContextMenuStrip = Me.ContextMenuChart
        Legend2.Enabled = False
        Legend2.Name = "Legend1"
        Me.ChartMain.Legends.Add(Legend2)
        Me.ChartMain.Location = New System.Drawing.Point(0, 0)
        Me.ChartMain.Name = "ChartMain"
        Me.ChartMain.Size = New System.Drawing.Size(786, 440)
        Me.ChartMain.TabIndex = 0
        Title2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Title2.Name = "Title1"
        Me.ChartMain.Titles.Add(Title2)
        '
        'btnChartMainUpdate
        '
        Me.btnChartMainUpdate.ImageKey = "Update-32.png"
        Me.btnChartMainUpdate.ImageList = Me.ImageList1
        Me.btnChartMainUpdate.Location = New System.Drawing.Point(550, 450)
        Me.btnChartMainUpdate.Name = "btnChartMainUpdate"
        Me.btnChartMainUpdate.Size = New System.Drawing.Size(98, 38)
        Me.btnChartMainUpdate.TabIndex = 48
        Me.btnChartMainUpdate.Text = "  Update"
        Me.btnChartMainUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnChartMainUpdate.UseVisualStyleBackColor = True
        '
        'btnShowButtonMain
        '
        Me.btnShowButtonMain.Appearance = System.Windows.Forms.Appearance.Button
        Me.btnShowButtonMain.AutoSize = True
        Me.btnShowButtonMain.ImageKey = "Button.png"
        Me.btnShowButtonMain.ImageList = Me.ImageList1
        Me.btnShowButtonMain.Location = New System.Drawing.Point(3, 450)
        Me.btnShowButtonMain.Name = "btnShowButtonMain"
        Me.btnShowButtonMain.Size = New System.Drawing.Size(38, 38)
        Me.btnShowButtonMain.TabIndex = 46
        Me.btnShowButtonMain.UseVisualStyleBackColor = True
        '
        'btnShowLoPMain
        '
        Me.btnShowLoPMain.Appearance = System.Windows.Forms.Appearance.Button
        Me.btnShowLoPMain.AutoSize = True
        Me.btnShowLoPMain.ImageKey = "Table-32.png"
        Me.btnShowLoPMain.ImageList = Me.ImageList1
        Me.btnShowLoPMain.Location = New System.Drawing.Point(747, 450)
        Me.btnShowLoPMain.Name = "btnShowLoPMain"
        Me.btnShowLoPMain.Size = New System.Drawing.Size(38, 38)
        Me.btnShowLoPMain.TabIndex = 45
        Me.btnShowLoPMain.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(804, 611)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "MainForm"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Reinforced Concrete Cross Section Analysis (RCCSA)"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.GroupBoxCrossSection.ResumeLayout(False)
        Me.GroupBoxCrossSection.PerformLayout()
        CType(Me.csPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.pointLoadPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.uipicPrestressed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uipicFRP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uipicSteel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.uipicStrCfnmt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uipicStrShear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uipicStrFlex, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.uipicSteelConfine, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.uipicTransReinf, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.panelInput.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbRepLoadData.ResumeLayout(False)
        CType(Me.uipicRepLoadData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.tabPage_splash.ResumeLayout(False)
        Me.tabPage_splash.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DetailsLayoutPanel.ResumeLayout(False)
        Me.tabPage_input.ResumeLayout(False)
        Me.tabPage_chartMatModel.ResumeLayout(False)
        Me.tabPage_chartMatModel.PerformLayout()
        CType(Me.DgvMatModel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartMatModel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuChart.ResumeLayout(False)
        Me.tabPage_main.ResumeLayout(False)
        Me.tabPage_main.PerformLayout()
        CType(Me.DgvMain, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents GroupBoxCrossSection As System.Windows.Forms.GroupBox
    Friend WithEvents csOptionIsection As System.Windows.Forms.RadioButton
    Friend WithEvents csOptionTsection As System.Windows.Forms.RadioButton
    Friend WithEvents csOptionCircle As System.Windows.Forms.RadioButton
    Friend WithEvents csOptionRect As System.Windows.Forms.RadioButton
    Friend WithEvents csPicture As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents pointLoadOptionDistributed As System.Windows.Forms.RadioButton
    Friend WithEvents pointLoadOptionTwo As System.Windows.Forms.RadioButton
    Friend WithEvents pointLoadOptionOne As System.Windows.Forms.RadioButton
    Friend WithEvents pointLoadPicture As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnReinfPrestressed As System.Windows.Forms.Button
    Friend WithEvents btnReinfFRP As System.Windows.Forms.Button
    Friend WithEvents btnReinfSteel As System.Windows.Forms.Button
    Friend WithEvents uipicSteel As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents inCMP_ConcreteSSModel As System.Windows.Forms.ComboBox
    Friend WithEvents inCMP_TensionStiffEffect As System.Windows.Forms.ComboBox
    Friend WithEvents inCMP_MaxCompStrain As System.Windows.Forms.TextBox
    Friend WithEvents inCMP_TensileStrength As System.Windows.Forms.TextBox
    Friend WithEvents inCMP_CompStrength As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents inDataAnly_N As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents inDataAnly_P As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents inCurv_incr As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents uipicPrestressed As System.Windows.Forms.PictureBox
    Friend WithEvents uipicFRP As System.Windows.Forms.PictureBox
    Friend WithEvents btnExpandCsVar As System.Windows.Forms.Button
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents inBeamDim_A As System.Windows.Forms.TextBox
    Friend WithEvents lblBeamDimA_unit As System.Windows.Forms.Label
    Friend WithEvents lblBeamDimA As System.Windows.Forms.Label
    Friend WithEvents inBeamDim_L As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents btnShrinkCsVar As System.Windows.Forms.Button
    Friend WithEvents lblCsTf1_unit As System.Windows.Forms.Label
    Friend WithEvents lblCsTf1 As System.Windows.Forms.Label
    Friend WithEvents inCrossSect_dia As System.Windows.Forms.TextBox
    Friend WithEvents lblCsDia_unit As System.Windows.Forms.Label
    Friend WithEvents lblCsDia As System.Windows.Forms.Label
    Friend WithEvents inCrossSect_bf2 As System.Windows.Forms.TextBox
    Friend WithEvents lblCsBf2_unit As System.Windows.Forms.Label
    Friend WithEvents lblCsBf2 As System.Windows.Forms.Label
    Friend WithEvents inCrossSect_H As System.Windows.Forms.TextBox
    Friend WithEvents lblCsH_unit As System.Windows.Forms.Label
    Friend WithEvents lblCsH As System.Windows.Forms.Label
    Friend WithEvents inCrossSect_bf1 As System.Windows.Forms.TextBox
    Friend WithEvents lblCsBf1_unit As System.Windows.Forms.Label
    Friend WithEvents lblCsBf1 As System.Windows.Forms.Label
    Friend WithEvents inCrossSect_bw As System.Windows.Forms.TextBox
    Friend WithEvents lblCsBw_unit As System.Windows.Forms.Label
    Friend WithEvents lblCsBw As System.Windows.Forms.Label
    Friend WithEvents inCrossSect_tf2 As System.Windows.Forms.TextBox
    Friend WithEvents lblCsTf2_unit As System.Windows.Forms.Label
    Friend WithEvents lblCsTf2 As System.Windows.Forms.Label
    Friend WithEvents SaveFileMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents inCurv_max As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents panelInput As System.Windows.Forms.Panel
    Friend WithEvents inCrossSect_tf1 As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabPage_input As System.Windows.Forms.TabPage
    Friend WithEvents tabPage_chartMatModel As System.Windows.Forms.TabPage
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ChartMatModel As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents ExecuteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ContextMenuChart As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents CmChartMenu_ResetChart As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DgvMatModel As System.Windows.Forms.DataGridView
    Friend WithEvents ToolBtnSaveXML As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolBtnOpenXML As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents optChartMatModel As System.Windows.Forms.ComboBox
    Friend WithEvents btnChartMatModelUpdate As System.Windows.Forms.Button
    Friend WithEvents tabPage_splash As System.Windows.Forms.TabPage
    Friend WithEvents btnShowLoP As System.Windows.Forms.CheckBox
    Friend WithEvents ApplicationTitle As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents DetailsLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Version As System.Windows.Forms.Label
    Friend WithEvents Copyright As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents ClmX As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClmY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddCustomChartToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MainTooltip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripRun As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents NewDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolbarChartOption As System.Windows.Forms.ToolStripButton
    Friend WithEvents CmChartMenu_Options As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChartOptionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolbarChartMatModel As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents optChartMain As System.Windows.Forms.ComboBox
    Friend WithEvents DgvMain As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ChartMain As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents btnChartMainUpdate As System.Windows.Forms.Button
    Friend WithEvents btnShowButtonMain As System.Windows.Forms.CheckBox
    Friend WithEvents btnShowLoPMain As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tbChartMain As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents tabPage_main As System.Windows.Forms.TabPage
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblNLayerReinf As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblReinfHybrid As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents gbRepLoadData As System.Windows.Forms.GroupBox
    Friend WithEvents uipicRepLoadData As System.Windows.Forms.PictureBox
    Friend WithEvents btnRepLoadData As System.Windows.Forms.Button
    Friend WithEvents cmChartMenu_flipXaxis As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmChartMenu_flipYaxis As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnTransReinf As System.Windows.Forms.Button
    Friend WithEvents btnSteelConfine As System.Windows.Forms.Button
    Friend WithEvents uipicSteelConfine As System.Windows.Forms.PictureBox
    Friend WithEvents uipicTransReinf As System.Windows.Forms.PictureBox
    Friend WithEvents btnStrFlex As System.Windows.Forms.Button
    Friend WithEvents btnStrCnfmt As System.Windows.Forms.Button
    Friend WithEvents btnStrShear As System.Windows.Forms.Button
    Friend WithEvents uipicStrCfnmt As System.Windows.Forms.PictureBox
    Friend WithEvents uipicStrShear As System.Windows.Forms.PictureBox
    Friend WithEvents uipicStrFlex As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents inAnlyRepeated As System.Windows.Forms.RadioButton
    Friend WithEvents inAnlyNegative As System.Windows.Forms.RadioButton
    Friend WithEvents inAnlyPositive As System.Windows.Forms.RadioButton

End Class
