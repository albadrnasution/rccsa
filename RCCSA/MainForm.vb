﻿Imports System.Xml
Imports System.IO
Imports System.Text
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.ComponentModel

Public Class MainForm

    Public all_data As SipilData = New SipilData
    Private buttons_inChartMain As New List(Of Button)
    Private buttonVisible As Boolean = False

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Set jendela ke tengah layar
        Dim mainScreen As Screen = Screen.FromPoint(Me.Location)
        Dim X As Integer = (mainScreen.WorkingArea.Width - Me.Width) / 2 + mainScreen.WorkingArea.Left
        Dim Y As Integer = (mainScreen.WorkingArea.Height - Me.Height) / 2 + mainScreen.WorkingArea.Top
        Me.StartPosition = FormStartPosition.Manual
        Me.Location = New System.Drawing.Point(X, Y)

        'Splash
        Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

        'Set posisi label dan group box ke default
        GroupBoxCrossSection.Height = 263
        lblCsDia.Location = New Point(lblCsDia.Location.X, 279)
        lblCsDia_unit.Location = New Point(lblCsDia_unit.Location.X, 279)
        inCrossSect_dia.Location = New Point(inCrossSect_dia.Location.X, 276)
    End Sub

    '/***************************************************************************************************************/
    '/*****************************************************************  CONTROL GENERATION  (TAB MATMODEL & MAIN)  */
    '/***************************************************************************************************************/

    ''' <summary>
    ''' Memutakhirkan daftar menu dan daftar pilihan pada scrollbox update material model berdasarkan isian 
    ''' material model pada longitudal reinforcement.
    ''' </summary>
    Private Sub updateMaterialModelChartOption()
        Dim array_mm_steel(all_data.steel_layerData.Length - 1) As String
        For index = 0 To all_data.steel_layerData.Length - 1
            Dim steel = all_data.steel_layerData(index)
            array_mm_steel(index) = steel.matModel
        Next

        Dim array_mm_frp(all_data.frp_layerData.Length - 1) As String
        For index = 0 To all_data.frp_layerData.Length - 1
            Dim frp = all_data.frp_layerData(index)
            array_mm_frp(index) = frp.matModel
        Next

        Dim array_mm_pre(all_data.prestressed_layerData.Length - 1) As String
        For index = 0 To all_data.prestressed_layerData.Length - 1
            Dim pre = all_data.prestressed_layerData(index)
            array_mm_pre(index) = pre.matModel
        Next

        'Reset menu toolbar mat model
        toolbarChartMatModel.DropDownItems.Clear()
        toolbarChartMatModel.Enabled = True

        '================== Add  Concrete Material Model into menu
        addMenuMatModel(ChartClc.viscon.code, ChartClc.viscon.title)
        '================== Add Conrete Material Moden into scroll box update
        Dim chartOptions As New BindingList(Of CbOption)()
        chartOptions.Add(New CbOption(ChartClc.viscon.code, ChartClc.viscon.title))

        '================== Add material model from longitudal reinforcement
        '====STEEL
        If (array_mm_steel.Contains("1")) Then
            chartOptions.Add(New CbOption(ChartClc.vistl1.code, ChartClc.vistl1.title))
            addMenuMatModel(ChartClc.vistl1.code, ChartClc.vistl1.title)
        End If
        If (array_mm_steel.Contains("2")) Then
            chartOptions.Add(New CbOption(ChartClc.vistl2.code, ChartClc.vistl2.title))
            addMenuMatModel(ChartClc.vistl2.code, ChartClc.vistl2.title)
        End If
        If (array_mm_steel.Contains("3")) Then
            chartOptions.Add(New CbOption(ChartClc.vistl3.code, ChartClc.vistl3.title))
            addMenuMatModel(ChartClc.vistl3.code, ChartClc.vistl3.title)
        End If
        If (array_mm_steel.Contains("4")) Then
            chartOptions.Add(New CbOption(ChartClc.vistl4.code, ChartClc.vistl4.title))
            addMenuMatModel(ChartClc.vistl4.code, ChartClc.vistl4.title)
        End If
        If (array_mm_steel.Contains("5")) Then
            chartOptions.Add(New CbOption(ChartClc.vistl5.code, ChartClc.vistl5.title))
            addMenuMatModel(ChartClc.vistl5.code, ChartClc.vistl5.title)
        End If
        '===FRP
        If (array_mm_frp.Contains("1")) Then
            chartOptions.Add(New CbOption(ChartClc.vifrp1.code, ChartClc.vifrp1.title))
            addMenuMatModel(ChartClc.vifrp1.code, ChartClc.vifrp1.title)
        End If
        If (array_mm_frp.Contains("2")) Then
            chartOptions.Add(New CbOption(ChartClc.vifrp2.code, ChartClc.vifrp2.title))
            addMenuMatModel(ChartClc.vifrp2.code, ChartClc.vifrp2.title)
        End If
        If (array_mm_frp.Contains("3")) Then
            chartOptions.Add(New CbOption(ChartClc.vifrp3.code, ChartClc.vifrp3.title))
            addMenuMatModel(ChartClc.vifrp3.code, ChartClc.vifrp3.title)
        End If
        If (array_mm_frp.Contains("4")) Then
            chartOptions.Add(New CbOption(ChartClc.vifrp4.code, ChartClc.vifrp4.title))
            addMenuMatModel(ChartClc.vifrp4.code, ChartClc.vifrp4.title)
        End If
        '===PRESTRESSED
        If (array_mm_pre.Contains("1")) Then
            chartOptions.Add(New CbOption(ChartClc.vipre1.code, ChartClc.vipre1.title))
            addMenuMatModel(ChartClc.vipre1.code, ChartClc.vipre1.title)
        End If
        If (array_mm_pre.Contains("2")) Then
            chartOptions.Add(New CbOption(ChartClc.vipre2.code, ChartClc.vipre2.title))
            addMenuMatModel(ChartClc.vipre2.code, ChartClc.vipre2.title)
        End If
        If (array_mm_pre.Contains("3")) Then
            chartOptions.Add(New CbOption(ChartClc.vipre3.code, ChartClc.vipre3.title))
            addMenuMatModel(ChartClc.vipre3.code, ChartClc.vipre3.title)
        End If

        Me.optChartMatModel.DataSource = chartOptions
        Me.optChartMatModel.DisplayMember = "display"
        Me.optChartMatModel.ValueMember = "value"
    End Sub

    ''' <summary>
    ''' Menambahkan menu item kepada toolstrip Material Model berdasarkan slug dan text.
    ''' </summary>
    ''' <param name="slug">Bagian nama menu yang menjadi penanda grafik</param>
    ''' <param name="text">Tulisan pada menu</param>
    Private Sub addMenuMatModel(slug As String, text As String)
        Dim toolbarmm = New ToolStripMenuItem()
        toolbarmm.Name = "toolbarmm_" & slug
        toolbarmm.Text = text
        toolbarChartMatModel.DropDownItems.Add(toolbarmm)
        AddHandler toolbarmm.Click, AddressOf ToolbarMatModelClickHandler
    End Sub

    ''' <summary>
    ''' Menambahkan menu item kepada toolstrip Material Model berdasarkan slug dan text.
    ''' </summary>
    ''' <param name="slug">Bagian nama menu yang menjadi penanda grafik</param>
    ''' <param name="text">Tulisan pada menu</param>
    Private Function createButtonChart(slug As String, text As String) As Button
        Dim buttonChart = New Button()
        buttonChart.Name = "buttonChartMain|" & slug
        buttonChart.Text = text
        AddHandler buttonChart.Click, AddressOf ButtonClickHandler
        Return buttonChart
    End Function

    ''' <summary>
    ''' Menambahkan menu item kepada toolstrip Material Model berdasarkan slug dan text.
    ''' </summary>
    ''' <param name="slug">Bagian nama menu yang menjadi penanda grafik</param>
    ''' <param name="text">Tulisan pada menu</param>
    Private Function createToolItem(slug As String, text As String) As ToolStripMenuItem
        Dim toolChart = New ToolStripMenuItem()
        toolChart.Name = "toolbarChartMain|" & slug
        toolChart.Text = text
        AddHandler toolChart.Click, AddressOf ToolbarMainClickHandler
        Return toolChart
    End Function

    '/***************************************************************************************************************/
    '/*****************************************************************  CONTROL GENERATION HANDLERS                */
    '/***************************************************************************************************************/
    ''' <summary>
    ''' Menjalankan fungsi menampilkan grafik ketika salah satu menu item pada toolbar chart material model diklik.
    ''' Grafik yang ditampilkan didasarkan pada nama menu yang diklik.
    ''' </summary>
    ''' <param name="sender">Menu yang diklik</param>
    ''' <param name="e"></param>
    Public Sub ToolbarMatModelClickHandler(ByVal sender As Object, ByVal e As EventArgs)
        'for a condition based on a ToolStripMenuItem that fired it
        Dim name As String = CType(sender, ToolStripMenuItem).Name
        Dim nameSplit As String() = name.Split("_")
        ChartHandler.paintChart(ChartMatModel, DgvMatModel, nameSplit(1))

        TabControl1.SelectedTab = tabPage_chartMatModel
    End Sub

    ''' <summary>
    ''' Menjalankan fungsi menampilkan grafik ketika salah satu menu item pada toolbar chart main diklik.
    ''' Grafik yang ditampilkan didasarkan pada nama menu yang diklik.
    ''' </summary>
    ''' <param name="sender">Menu yang diklik</param>
    ''' <param name="e"></param>
    Public Sub ToolbarMainClickHandler(ByVal sender As Object, ByVal e As EventArgs)
        'for a condition based on a ToolStripMenuItem that fired it
        Dim name As String = CType(sender, ToolStripMenuItem).Name
        Dim nameSplit As String() = name.Split("|")

        ChartHandler.paintChart(ChartMain, DgvMain, nameSplit(1))
        TabControl1.SelectedTab = tabPage_main
    End Sub

    ''' <summary>
    ''' Menjalankan fungsi menampilkan grafik ketika salah satu menu button diklik.
    ''' Grafik yang ditampilkan didasarkan pada nama menu yang diklik.
    ''' </summary>
    ''' <param name="sender">Menu yang diklik</param>
    ''' <param name="e"></param>
    Public Sub ButtonClickHandler(ByVal sender As Object, ByVal e As EventArgs)
        'for a condition based on a ToolStripMenuItem that fired it
        Dim name As String = CType(sender, Button).Name
        Dim nameSplit As String() = name.Split("|")

        ChartHandler.paintChart(ChartMain, DgvMain, nameSplit(1))
        TabControl1.SelectedTab = tabPage_main
    End Sub


    '/**********************************************************************************************************/
    '/****************************************************************************  UI INPUT INTERACTION       */
    '/**********************************************************************************************************/

    Private Sub csOptionRect_CheckedChanged(sender As Object, e As EventArgs) Handles csOptionRect.CheckedChanged
        all_data.crossSectionType = 1
        csPicture.Image = My.Resources.Balok_Persegi
        ' Variabel masking BW dan H
        lblCsBw.Show()
        lblCsBw_unit.Show()
        inCrossSect_bw.Show()
        lblCsH.Show()
        lblCsH_unit.Show()
        inCrossSect_H.Show()
        ' Variabel masking Dia
        lblCsDia.Hide()
        lblCsDia_unit.Hide()
        inCrossSect_dia.Hide()
        'Vairabel masking BF1 BF2
        lblCsBf1.Hide()
        lblCsBf1_unit.Hide()
        inCrossSect_bf1.Hide()
        lblCsBf2.Hide()
        lblCsBf2_unit.Hide()
        inCrossSect_bf2.Hide()
        'Vairabel masking TF1 TF2
        lblCsTf1.Hide()
        lblCsTf1_unit.Hide()
        inCrossSect_tf1.Hide()
        lblCsTf2.Hide()
        lblCsTf2_unit.Hide()
        inCrossSect_tf2.Hide()
        'Set height of groupbox CS
        If (btnShrinkCsVar.Visible) Then
            GroupBoxCrossSection.Height = 340
        End If
    End Sub

    Private Sub csOptionCircle_CheckedChanged(sender As Object, e As EventArgs) Handles csOptionCircle.CheckedChanged
        all_data.crossSectionType = 2
        csPicture.Image = My.Resources.Lingkaran
        ' Variabel masking BW dan H
        lblCsBw.Hide()
        lblCsBw_unit.Hide()
        inCrossSect_bw.Hide()
        lblCsH.Hide()
        lblCsH_unit.Hide()
        inCrossSect_H.Hide()
        ' Variabel masking Dia
        lblCsDia.Show()
        lblCsDia_unit.Show()
        inCrossSect_dia.Show()
        'Vairabel masking BF1 BF2
        lblCsBf1.Hide()
        lblCsBf1_unit.Hide()
        inCrossSect_bf1.Hide()
        lblCsBf2.Hide()
        lblCsBf2_unit.Hide()
        inCrossSect_bf2.Hide()
        'Vairabel masking TF1 TF2
        lblCsTf1.Hide()
        lblCsTf1_unit.Hide()
        inCrossSect_tf1.Hide()
        lblCsTf2.Hide()
        lblCsTf2_unit.Hide()
        inCrossSect_tf2.Hide()
        'Set height of groupbox CS
        If (btnShrinkCsVar.Visible) Then
            GroupBoxCrossSection.Height = 340
        End If
    End Sub

    Private Sub csOptionTbeam_CheckedChanged(sender As Object, e As EventArgs) Handles csOptionTsection.CheckedChanged
        all_data.crossSectionType = 3
        csPicture.Image = My.Resources.Balok_T
        ' Variabel masking BW dan H
        lblCsBw.Show()
        lblCsBw_unit.Show()
        inCrossSect_bw.Show()
        lblCsH.Show()
        lblCsH_unit.Show()
        inCrossSect_H.Show()
        ' Variabel masking Dia
        lblCsDia.Hide()
        lblCsDia_unit.Hide()
        inCrossSect_dia.Hide()
        'Vairabel masking BF1 BF2
        lblCsBf1.Show()
        lblCsBf1_unit.Show()
        inCrossSect_bf1.Show()
        lblCsBf2.Hide()
        lblCsBf2_unit.Hide()
        inCrossSect_bf2.Hide()
        'Vairabel masking TF1 TF2
        lblCsTf1.Location = New Point(lblCsTf1.Location.X, 305)
        lblCsTf1_unit.Location = New Point(lblCsTf1_unit.Location.X, 305)
        inCrossSect_tf1.Location = New Point(inCrossSect_tf1.Location.X, 302)
        lblCsTf1.Show()
        lblCsTf1_unit.Show()
        inCrossSect_tf1.Show()
        lblCsTf2.Hide()
        lblCsTf2_unit.Hide()
        inCrossSect_tf2.Hide()
        'Set height of groupbox CS
        If (btnShrinkCsVar.Visible) Then
            GroupBoxCrossSection.Height = 340
        End If
    End Sub

    Private Sub csOptionIbeam_CheckedChanged(sender As Object, e As EventArgs) Handles csOptionIsection.CheckedChanged
        all_data.crossSectionType = 4
        csPicture.Image = My.Resources.Balok_I
        ' Variabel masking BW dan H
        lblCsBw.Show()
        lblCsBw_unit.Show()
        inCrossSect_bw.Show()
        lblCsH.Show()
        lblCsH_unit.Show()
        inCrossSect_H.Show()
        ' Variabel masking Dia
        lblCsDia.Hide()
        lblCsDia_unit.Hide()
        inCrossSect_dia.Hide()
        'Vairabel masking BF1 BF2
        lblCsBf1.Show()
        lblCsBf1_unit.Show()
        inCrossSect_bf1.Show()
        lblCsBf2.Show()
        lblCsBf2_unit.Show()
        inCrossSect_bf2.Show()
        'Vairabel masking TF1 TF2
        lblCsTf1.Location = New Point(lblCsTf1.Location.X, 331)
        lblCsTf1_unit.Location = New Point(lblCsTf1_unit.Location.X, 331)
        inCrossSect_tf1.Location = New Point(inCrossSect_tf1.Location.X, 328)
        lblCsTf1.Show()
        lblCsTf1_unit.Show()
        inCrossSect_tf1.Show()
        lblCsTf2.Show()
        lblCsTf2_unit.Show()
        inCrossSect_tf2.Show()
        'Set height of groupbox CS
        If (btnShrinkCsVar.Visible) Then
            GroupBoxCrossSection.Height = 386
        End If
    End Sub

    Private Sub btnExpandCsVar_Click(sender As Object, e As EventArgs) Handles btnExpandCsVar.Click
        If (csOptionIsection.Checked) Then
            GroupBoxCrossSection.Height = 386
        Else
            GroupBoxCrossSection.Height = 340
        End If
        btnExpandCsVar.Hide()
        btnShrinkCsVar.Show()
    End Sub

    Private Sub btnShrinkCsVar_Click(sender As Object, e As EventArgs) Handles btnShrinkCsVar.Click
        GroupBoxCrossSection.Height = 263
        btnExpandCsVar.Show()
        btnShrinkCsVar.Hide()
    End Sub

    Private Sub pointLoadOptionOne_CheckedChanged(sender As Object, e As EventArgs) Handles pointLoadOptionOne.CheckedChanged
        all_data.pointLoadType = 1
        pointLoadPicture.Image = My.Resources.Load_One_Point
        lblBeamDimA.Show()
        lblBeamDimA_unit.Show()
        inBeamDim_A.Show()
    End Sub

    Private Sub pointLoadOptionTwo_CheckedChanged(sender As Object, e As EventArgs) Handles pointLoadOptionTwo.CheckedChanged
        all_data.pointLoadType = 2
        pointLoadPicture.Image = My.Resources.Load_Two_Point
        lblBeamDimA.Show()
        lblBeamDimA_unit.Show()
        inBeamDim_A.Show()
    End Sub

    Private Sub pointLoadOptionDistributed_CheckedChanged(sender As Object, e As EventArgs) Handles pointLoadOptionDistributed.CheckedChanged
        all_data.pointLoadType = 3
        pointLoadPicture.Image = My.Resources.Load_Distributed
        lblBeamDimA.Hide()
        lblBeamDimA_unit.Hide()
        inBeamDim_A.Hide()
    End Sub

    Private Sub btnReinfSteel_Click(sender As Object, e As EventArgs) Handles btnReinfSteel.Click
        'Dialog yg mau dibuka adalah dialog Steel Reinforcement
        Dim dlg As New dialogSteelReinf()

        '========================================================== SET DATA KE DIALOG STEEL REINFORCEMENT
        Dim HorDia As String
        If (csOptionCircle.Checked) Then
            'kalau dipilih lingkaran, tampilkan diameter
            HorDia = If(inCrossSect_dia.Text = "", "not specified", inCrossSect_dia.Text & " mm")
            dlg.lblH.Text = "Dia = " & HorDia
        Else
            'kalau selain lingkaran, tampilkan height
            HorDia = If(inCrossSect_H.Text = "", "not specified", inCrossSect_H.Text & " mm")
            dlg.lblH.Text = "H = " & HorDia
        End If
        dlg.steel_layerData = all_data.steel_layerData
        dlg.steel_stressStrainModel = all_data.steel_stressStrainModel

        ' Show the dialog and determine the state of the 
        ' DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then

            '========================================================== AMBIL DATA DARI DIALOG STEEL REINFORCEMENT
            'Ambil pilihan Stress-Strain Model
            all_data.steel_stressStrainModel = dlg.steel_stressStrainModel
            'Ambil layer data dari isian dialog
            all_data.steel_layerData = dlg.steel_layerData
            count_NR_Layer_and_UI()

            If (all_data.steel_layerData.Length > 0) Then
                uipicSteel.Image = My.Resources.Check
            Else
                uipicSteel.Image = Nothing
            End If
        End If
    End Sub

    Private Sub btnReinfFRP_Click(sender As Object, e As EventArgs) Handles btnReinfFRP.Click
        'Dialog yg mau dibuka adalah dialog FRP Reinforcement
        Dim dlg As New DialogFRPReinf()

        '========================================================== SET DATA KE DIALOG FRP REINFORCEMENT
        Dim HorDia As String
        If (csOptionCircle.Checked) Then
            'kalau dipilih lingkaran, tampilkan diameter
            HorDia = If(inCrossSect_dia.Text = "", "not specified", inCrossSect_dia.Text & " mm")
            dlg.lblH.Text = "Dia = " & HorDia
        Else
            'kalau selain lingkaran, tampilkan height
            HorDia = If(inCrossSect_H.Text = "", "not specified", inCrossSect_H.Text & " mm")
            dlg.lblH.Text = "H = " & HorDia
        End If
        dlg.frp_layerData = all_data.frp_layerData
        dlg.frp_stressStrainModel = all_data.frp_stressStrainModel

        ' Show the dialog and determine the state of the 
        ' DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then

            '========================================================== AMBIL DATA DARI DIALOG FRP REINFORCEMENT
            'Ambil pilihan Stress-Strain Model
            all_data.frp_stressStrainModel = dlg.frp_stressStrainModel
            'Ambil layer data dari isian dialog
            all_data.frp_layerData = dlg.frp_layerData
            count_NR_Layer_and_UI()

            If (all_data.frp_layerData.Length > 0) Then
                uipicFRP.Image = My.Resources.Check
            Else
                uipicFRP.Image = Nothing
            End If
        End If
    End Sub

    Private Sub btnReinfPrestressed_Click(sender As Object, e As EventArgs) Handles btnReinfPrestressed.Click
        'Dialog yg mau dibuka adalah dialog Prestressed Reinforcement
        Dim dlg As New DialogPrestressedReinf()

        '========================================================== SET DATA KE DIALOG FRP REINFORCEMENT
        Dim HorDia As String
        If (csOptionCircle.Checked) Then
            'kalau dipilih lingkaran, tampilkan diameter
            HorDia = If(inCrossSect_dia.Text = "", "not specified", inCrossSect_dia.Text & " mm")
            dlg.lblH.Text = "Dia = " & HorDia
        Else
            'kalau selain lingkaran, tampilkan height
            HorDia = If(inCrossSect_H.Text = "", "not specified", inCrossSect_H.Text & " mm")
            dlg.lblH.Text = "H = " & HorDia
        End If
        dlg.prestressed_layerData = all_data.prestressed_layerData
        dlg.prestressed_stressStrainModel = all_data.prestressed_stressStrainModel

        ' Show the dialog and determine the state of the 
        ' DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then

            '========================================================== AMBIL DATA DARI DIALOG PRESTRESS REINFORCEMENT
            'Ambil pilihan Stress-Strain Model
            all_data.prestressed_stressStrainModel = dlg.prestressed_stressStrainModel
            'Ambil layer data dari isian dialog
            all_data.prestressed_layerData = dlg.prestressed_layerData
            count_NR_Layer_and_UI()

            If (all_data.prestressed_layerData.Length > 0) Then
                uipicPrestressed.Image = My.Resources.Check
            Else
                uipicPrestressed.Image = Nothing
            End If
        End If

    End Sub


    Private Sub btnRepLoadData_Click(sender As Object, e As EventArgs) Handles btnRepLoadData.Click
        'Dialog yg mau dibuka adalah dialog Repeated Load Data
        Dim dlg As New DialogRepLoadData()

        'Kirim data dari all_data ke dialog pada saat dialog dibuka
        dlg.repLoadData = all_data.repLoadData

        ' Show the dialog and determine the state of the DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then

            '=================== AMBIL DATA DARI DIALOG REPLOADDATA
            'Ambil rep load data
            all_data.repLoadData = dlg.repLoadData

            If (all_data.repLoadData.Length > 0) Then
                uipicRepLoadData.Image = My.Resources.Check
            Else
                uipicRepLoadData.Image = Nothing
            End If
        End If

    End Sub


    '/**********************************************************************************************************/
    '/****************************************************************************  SUB for SAVE AND OPEN DATA */
    '/**********************************************************************************************************/

    ''' <summary>
    ''' Fungsi untuk menyimpan segala data berkaitan dengan RCSSA ke dalam XML agar kelak dapat dibuka kembali
    ''' </summary>
    Private Sub saveData_intoXML()
        ' ========================================================================== MEMUNCULKAN SAVE DIALOG
        Dim saveFileDialog1 As New SaveFileDialog()
        saveFileDialog1.Filter = "RCSSA DATA (*.xml)|*.xml|All files (*.*)|*.*"
        saveFileDialog1.FilterIndex = 1
        saveFileDialog1.RestoreDirectory = True

        ' setelah pengguna pencet OK
        If saveFileDialog1.ShowDialog() = DialogResult.OK Then
            'ambil nama file 
            Dim filename As String = saveFileDialog1.FileName

            '' ==== Set Window Text
            setWindowText(filename)

            ' ========================================================================== MENYIAPKAN XML UNTUK DITULIS
            ' Create XmlWriterSettings.
            Dim settings As XmlWriterSettings = New XmlWriterSettings()
            settings.Indent = True

            ' Create XmlWriter.
            Using writer As XmlWriter = XmlWriter.Create(filename, settings)
                ' Begin writing.
                writer.WriteStartDocument()
                writer.WriteStartElement("rccsa") ' Root.

                ' XML Bagian CROSS SECTION
                writer.WriteStartElement("cross_section")
                '===tulis tipe: nomor dan deskripsi
                writer.WriteElementString("type", all_data.crossSectionType)
                writer.WriteElementString("description", all_data.cross_section_type_desc_master(all_data.crossSectionType))
                '===tulis variabel
                writer.WriteStartElement("variables")
                If (csOptionRect.Checked) Then
                    writer.WriteElementString("bw", inCrossSect_bw.Text)
                    writer.WriteElementString("h", inCrossSect_H.Text)
                ElseIf (csOptionCircle.Checked) Then
                    writer.WriteElementString("dia", inCrossSect_dia.Text)
                ElseIf (csOptionTsection.Checked) Then
                    writer.WriteElementString("bw", inCrossSect_bw.Text)
                    writer.WriteElementString("h", inCrossSect_H.Text)
                    writer.WriteElementString("bf1", inCrossSect_bf1.Text)
                    writer.WriteElementString("tf1", inCrossSect_tf1.Text)
                ElseIf (csOptionIsection.Checked) Then
                    writer.WriteElementString("bw", inCrossSect_bw.Text)
                    writer.WriteElementString("h", inCrossSect_H.Text)
                    writer.WriteElementString("bf1", inCrossSect_bf1.Text)
                    writer.WriteElementString("bf2", inCrossSect_bf2.Text)
                    writer.WriteElementString("tf1", inCrossSect_tf1.Text)
                    writer.WriteElementString("tf2", inCrossSect_tf2.Text)
                End If
                writer.WriteEndElement()
                '==/akhir dari CROSS SECTION
                writer.WriteEndElement()


                ' XML Bagian BEAM DIMENSION/POINT LOAD
                writer.WriteStartElement("beam_dimension")
                '===tulis tipe: nomor dan deskripsi
                writer.WriteElementString("type", all_data.pointLoadType)
                writer.WriteElementString("description", all_data.point_load_type_desc_master(all_data.pointLoadType))
                '===tulis variabel
                writer.WriteStartElement("variables")
                writer.WriteElementString("l", inBeamDim_L.Text)
                writer.WriteElementString("a", inBeamDim_A.Text)
                writer.WriteEndElement()
                '==/akhir dari BEAM DIMENSION/POINT LOAD
                writer.WriteEndElement()

                ' XML Bagian CONCRETE MATERIAL PROPERTIES
                writer.WriteStartElement("concrete_material_properties")
                '===tulis ssm model: nomor dan deskripsi
                writer.WriteStartElement("strain_stress_model")
                writer.WriteElementString("number", inCMP_ConcreteSSModel.SelectedIndex)
                writer.WriteElementString("description", inCMP_ConcreteSSModel.Text)
                writer.WriteEndElement()
                '===tulis tension stiff efect: nomor dan deskripsi
                writer.WriteStartElement("tension_stiffenning_effect")
                writer.WriteElementString("number", inCMP_TensionStiffEffect.SelectedIndex)
                writer.WriteElementString("description", inCMP_TensionStiffEffect.Text)
                writer.WriteEndElement()
                '===tulis isian lain
                writer.WriteElementString("comp_strength", inCMP_CompStrength.Text)
                writer.WriteElementString("tensile_strength", inCMP_TensileStrength.Text)
                writer.WriteElementString("max_comp_strain", inCMP_MaxCompStrain.Text)
                '==/akhir dari CONCRETE MATERIAL PROPERTIES
                writer.WriteEndElement()

                ' XML BAGIAN LONGITUDAL REINFORCEMENT
                writer.WriteStartElement("longitudal_reinforcement")
                '===tulis seluruh isian steel reinforcement
                writer.WriteStartElement("steel_reinforcement")
                writer.WriteStartElement("layers")
                If (all_data.steel_layerData IsNot Nothing) Then
                    For Each steels In all_data.steel_layerData
                        writer.WriteStartElement("steel")
                        writer.WriteElementString("dia", steels.dia)
                        writer.WriteElementString("n", steels.N)
                        writer.WriteElementString("as", steels.asv)
                        writer.WriteElementString("fy", steels.fy)
                        writer.WriteElementString("es", steels.es)
                        writer.WriteElementString("rup", steels.rup)
                        writer.WriteElementString("di", steels.di)
                        writer.WriteElementString("mat_model", steels.matModel)
                        writer.WriteEndElement()
                    Next
                End If
                writer.WriteEndElement()
                writer.WriteEndElement()
                '===tulis seluruh isian frp reinforcement
                writer.WriteStartElement("frp_reinforcement")
                writer.WriteStartElement("layers")
                If (all_data.frp_layerData IsNot Nothing) Then
                    For Each frp In all_data.frp_layerData
                        writer.WriteStartElement("frp")
                        writer.WriteElementString("dia", frp.dia)
                        writer.WriteElementString("n", frp.N)
                        writer.WriteElementString("af", frp.af)
                        writer.WriteElementString("ffu", frp.ffu)
                        writer.WriteElementString("ef", frp.ef)
                        writer.WriteElementString("rup", frp.rup)
                        writer.WriteElementString("di", frp.di)
                        writer.WriteElementString("mat_model", frp.matModel)
                        writer.WriteEndElement()
                    Next
                End If
                writer.WriteEndElement()
                writer.WriteEndElement()
                '===tulis seluruh isian prestressed reinforcement
                writer.WriteStartElement("prestressed_reinforcement")
                writer.WriteStartElement("layers")
                If (all_data.prestressed_layerData IsNot Nothing) Then
                    For Each prestressed In all_data.prestressed_layerData
                        writer.WriteStartElement("prestressed")
                        writer.WriteElementString("dia", prestressed.dia)
                        writer.WriteElementString("n", prestressed.N)
                        writer.WriteElementString("ap", prestressed.ap)
                        writer.WriteElementString("fyp", prestressed.fyp)
                        writer.WriteElementString("esp", prestressed.esp)
                        writer.WriteElementString("rup", prestressed.rup)
                        writer.WriteElementString("di", prestressed.di)
                        writer.WriteElementString("prestrain", prestressed.prestrain)
                        writer.WriteElementString("mat_model", prestressed.matModel)
                        writer.WriteEndElement()
                    Next
                End If
                writer.WriteEndElement()
                writer.WriteEndElement()
                '==/akhir dari LONGITUDAL REINFORCEMENT
                writer.WriteEndElement()

                ' ANALYSIS TYPE
                writer.WriteElementString("analysis_type", If(inAnlyPositive.Checked, "0",
                                  If(inAnlyNegative.Checked, "1", If(inAnlyRepeated.Checked, "2", "-1"))))

                ' REPEATED LOAD DATA
                writer.WriteStartElement("repeated_load_data")
                '===tulis seluruh isian frp reinforcement
                writer.WriteElementString("count", all_data.repLoadData.Length)
                writer.WriteStartElement("data")
                If (all_data.repLoadData IsNot Nothing) Then
                    For Each rld In all_data.repLoadData
                        writer.WriteStartElement("cyclic")
                        writer.WriteElementString("cyclic_no", rld.no)
                        writer.WriteElementString("num_of_rep", rld.numRepetition)
                        writer.WriteElementString("turning_point_plus", rld.turnPointPlus)
                        writer.WriteElementString("turning_point_min", rld.turnPointMin)
                        writer.WriteEndElement()
                    Next
                End If
                writer.WriteEndElement()
                '/akhir dari repated load data
                writer.WriteEndElement()


                ' XML BAGIAN DATA COLUMN ANALYSIS
                writer.WriteStartElement("data_column_analysis")
                writer.WriteElementString("p", inDataAnly_P.Text)
                writer.WriteElementString("n", inDataAnly_N.Text)
                writer.WriteEndElement()

                ' ===============================XML BAGIAN STRENGTHENING 
                ' STRENGTHENING Flexural
                writer.WriteStartElement("strengthening_flexural")
                writer.WriteElementString("fup", all_data.inStrFlex_fup)
                writer.WriteElementString("ep", all_data.inStrFlex_ep)
                writer.WriteElementString("ult_strain", all_data.inStrFlex_us)
                writer.WriteElementString("lim_strain", all_data.inStrFlex_ls)
                writer.WriteElementString("bp", all_data.inStrFlex_bp)
                writer.WriteElementString("tp", all_data.inStrFlex_tp)
                writer.WriteElementString("di", all_data.inStrFlex_di)
                writer.WriteEndElement()
                ' STRENGTHENING Shear
                writer.WriteStartElement("strengthening_shear")
                writer.WriteElementString("fuf", all_data.inStrShear_fuf)
                writer.WriteElementString("ef", all_data.inStrShear_ef)
                writer.WriteElementString("ult_strain", all_data.inStrShear_us)
                writer.WriteElementString("lim_strain", all_data.inStrShear_ls)
                writer.WriteElementString("tf", all_data.inStrShear_tf)
                writer.WriteElementString("angle", all_data.inStrShear_ang)
                writer.WriteElementString("nf", all_data.inStrShear_nf)
                writer.WriteElementString("discr_1_conti_0", all_data.inStrShear_dorc)
                writer.WriteElementString("bf", all_data.inStrShear_bf)
                writer.WriteElementString("sf", all_data.inStrShear_sf)
                writer.WriteElementString("frp_type", all_data.inStrShear_frpt)
                writer.WriteEndElement()
                ' STRENGTHENING Confinement
                writer.WriteStartElement("strengthening_confine")
                writer.WriteElementString("fuf", all_data.inStrCfnmt_fuf)
                writer.WriteElementString("ef", all_data.inStrCfnmt_ef)
                writer.WriteElementString("ult_strain", all_data.inStrCfnmt_us)
                writer.WriteElementString("lim_strain", all_data.inStrCfnmt_ls)
                writer.WriteElementString("tf", all_data.inStrCfnmt_tf)
                writer.WriteElementString("radius", all_data.inStrCfnmt_rad)
                writer.WriteElementString("nf", all_data.inStrCfnmt_nf)
                writer.WriteElementString("discr_1_conti_0", all_data.inStrCfnmt_dorc)
                writer.WriteElementString("bf", all_data.inStrCfnmt_bf)
                writer.WriteElementString("sf", all_data.inStrCfnmt_sf)
                writer.WriteElementString("frp_type", all_data.inStrCfnmt_frpt)
                writer.WriteEndElement()

                ' XML BAGIAN TRANSVERSE REINFORCEMENT
                writer.WriteStartElement("transverse")
                writer.WriteElementString("fys", all_data.inTransverse_fys)
                writer.WriteElementString("ess", all_data.inTransverse_ess)
                writer.WriteElementString("dia", all_data.inTransverse_dia)
                writer.WriteElementString("spacing", all_data.inTransverse_spacing)
                writer.WriteElementString("type", all_data.inTransverse_type)
                writer.WriteElementString("type_desc", all_data.inTransverse_type)
                writer.WriteEndElement()

                ' XML BAGIAN TRANSVERSE REINFORCEMENT
                writer.WriteStartElement("steel_confinement")
                writer.WriteElementString("wcx", all_data.inSteelConf_wcx)
                writer.WriteElementString("hcy", all_data.inSteelConf_hcy)
                writer.WriteElementString("bc", all_data.inSteelConf_bc)
                writer.WriteElementString("tc", all_data.inSteelConf_tc)
                writer.WriteElementString("slx", all_data.inSteelConf_slx)
                writer.WriteElementString("sly", all_data.inSteelConf_sly)
                writer.WriteElementString("nlx", all_data.inSteelConf_nlx)
                writer.WriteElementString("nly", all_data.inSteelConf_nly)
                writer.WriteEndElement()

                ' XML BAGIAN CURVATURE
                writer.WriteStartElement("curvature")
                writer.WriteElementString("incr", inCurv_incr.Text)
                writer.WriteElementString("max", inCurv_max.Text)
                writer.WriteEndElement()

                ' End document.
                writer.WriteEndElement()
                writer.WriteEndDocument()
            End Using
        End If
    End Sub

    ''' <summary>
    ''' Fungsi untuk membuka segala data berkaitan dengan RCSSA dari XML
    ''' Setiap data dalam xml dimasukkan ke dalam input textbox
    ''' Kemudian data dan UI yang berkaitan dimutakhirkan
    ''' </summary>
    Private Sub openData_fromXML()
        ' ========================================================================== MEMUNCULKAN OPEN DIALOG
        Dim openFileDialog As New OpenFileDialog()
        openFileDialog.Filter = "RCSSA DATA (*.xml)|*.xml|All files (*.*)|*.*"
        openFileDialog.FilterIndex = 1
        openFileDialog.RestoreDirectory = True


        ' setelah pengguna pencet OK
        If openFileDialog.ShowDialog() = DialogResult.OK Then
            'ambil nama file 
            Dim filename As String = openFileDialog.FileName

            'reset sipil data
            all_data = New SipilData

            Try
                'Create the XML Document
                Dim m_xmld As XmlDocument = New XmlDocument()
                'Load the Xml file
                m_xmld.Load(filename)


                '============================================================ Load CROSS SECTION
                Dim cross_section As XmlNode = m_xmld.SelectSingleNode("rccsa/cross_section")
                all_data.crossSectionType = Convert.ToInt32(cross_section.SelectSingleNode("type").InnerText)
                If (all_data.crossSectionType = 1 Or all_data.crossSectionType = 3 Or all_data.crossSectionType = 4) Then
                    'ambil hanya jika tipe crossSection adalah rect, t-beam, atau i-beam
                    inCrossSect_bw.Text = cross_section.SelectSingleNode("variables/bw").InnerText
                    inCrossSect_H.Text = cross_section.SelectSingleNode("variables/h").InnerText
                End If
                If (all_data.crossSectionType = 2) Then
                    'ambil hanya jika tipe crossSection adalah circle
                    inCrossSect_dia.Text = cross_section.SelectSingleNode("variables/dia").InnerText
                End If
                If (all_data.crossSectionType = 3 Or all_data.crossSectionType = 4) Then
                    'ambil hanya jika tipe crossSection adalah t-beam atau i-beam
                    inCrossSect_bf1.Text = cross_section.SelectSingleNode("variables/bf1").InnerText
                    inCrossSect_tf1.Text = cross_section.SelectSingleNode("variables/tf1").InnerText
                End If
                If (all_data.crossSectionType = 4) Then
                    'ambil hanya jika tipe crossSection adalah i-beam
                    inCrossSect_tf2.Text = cross_section.SelectSingleNode("variables/tf2").InnerText
                    inCrossSect_bf2.Text = cross_section.SelectSingleNode("variables/bf2").InnerText
                End If
                '============================================================ Load CROSS SECTION UI
                'Tampilan pilihan radio bar cross section
                If (all_data.crossSectionType = 1) Then
                    csOptionRect.Checked = True
                ElseIf (all_data.crossSectionType = 2) Then
                    csOptionCircle.Checked = True
                ElseIf (all_data.crossSectionType = 3) Then
                    csOptionTsection.Checked = True
                ElseIf (all_data.crossSectionType = 4) Then
                    csOptionIsection.Checked = True
                End If


                '============================================================ Load BEAM DIMENSION
                Dim beam_dimension As XmlNode = m_xmld.SelectSingleNode("rccsa/beam_dimension")
                all_data.pointLoadType = Convert.ToInt32(beam_dimension.SelectSingleNode("type").InnerText)
                inBeamDim_L.Text = beam_dimension.SelectSingleNode("variables/l").InnerText
                If (all_data.pointLoadType = 1 Or all_data.pointLoadType = 2) Then
                    'ambil hanya jika tipe pointLoad adalah one-point atau two-point
                    'distributed point tidak memiliki variabel a
                    inBeamDim_A.Text = beam_dimension.SelectSingleNode("variables/a").InnerText
                End If
                '============================================================ Load BEAM DIMENSION UI
                'Tampilan pilihan radio bar point load
                If (all_data.pointLoadType = 1) Then
                    pointLoadOptionOne.Checked = True
                ElseIf (all_data.pointLoadType = 2) Then
                    pointLoadOptionTwo.Checked = True
                ElseIf (all_data.pointLoadType = 3) Then
                    pointLoadOptionDistributed.Checked = True
                End If


                '============================================================ Load CONCRETE MATERIAL PROPERTIES
                Dim cmp As XmlNode = m_xmld.SelectSingleNode("rccsa/concrete_material_properties")
                inCMP_CompStrength.Text = cmp.SelectSingleNode("comp_strength").InnerText
                inCMP_TensileStrength.Text = cmp.SelectSingleNode("tensile_strength").InnerText
                inCMP_MaxCompStrain.Text = cmp.SelectSingleNode("max_comp_strain").InnerText
                inCMP_ConcreteSSModel.SelectedIndex = Convert.ToInt32(cmp.SelectSingleNode("strain_stress_model/number").InnerText)
                inCMP_TensionStiffEffect.SelectedIndex = Convert.ToInt32(cmp.SelectSingleNode("tension_stiffenning_effect/number").InnerText)


                '============================================================ Load DATA COLUMN ANALYSIS
                Dim column_analysis As XmlNode = m_xmld.SelectSingleNode("rccsa/data_column_analysis")
                inDataAnly_P.Text = column_analysis.SelectSingleNode("p").InnerText
                inDataAnly_N.Text = column_analysis.SelectSingleNode("n").InnerText

                ' Strengthening Flexural
                Dim str_flex As XmlNode = m_xmld.SelectSingleNode("rccsa/strengthening_flexural")
                all_data.inStrFlex_fup = str_flex.SelectSingleNode("fup").InnerText
                all_data.inStrFlex_ep = str_flex.SelectSingleNode("ep").InnerText
                all_data.inStrFlex_us = str_flex.SelectSingleNode("ult_strain").InnerText
                all_data.inStrFlex_ls = str_flex.SelectSingleNode("lim_strain").InnerText
                all_data.inStrFlex_bp = str_flex.SelectSingleNode("bp").InnerText
                all_data.inStrFlex_tp = str_flex.SelectSingleNode("tp").InnerText
                all_data.inStrFlex_di = str_flex.SelectSingleNode("di").InnerText
                'Check List bahwa semua field pada StrFlex terisi
                Dim anyEmptyFlex As Boolean = all_data.inStrFlex_bp = "" Or all_data.inStrFlex_di = "" Or
                    all_data.inStrFlex_ep = "" Or all_data.inStrFlex_fup = "" Or all_data.inStrFlex_ls = "" Or
                    all_data.inStrFlex_tp = "" Or all_data.inStrFlex_us = ""
                'Update UI CheckBox       
                uipicStrFlex.Image = If(anyEmptyFlex, Nothing, My.Resources.Check)

                ' Strengthening Shear
                Dim str_shear As XmlNode = m_xmld.SelectSingleNode("rccsa/strengthening_shear")
                all_data.inStrShear_fuf = str_shear.SelectSingleNode("fuf").InnerText
                all_data.inStrShear_ef = str_shear.SelectSingleNode("ef").InnerText
                all_data.inStrShear_us = str_shear.SelectSingleNode("ult_strain").InnerText
                all_data.inStrShear_ls = str_shear.SelectSingleNode("lim_strain").InnerText
                all_data.inStrShear_tf = str_shear.SelectSingleNode("tf").InnerText
                all_data.inStrShear_ang = str_shear.SelectSingleNode("angle").InnerText
                all_data.inStrShear_nf = str_shear.SelectSingleNode("nf").InnerText
                all_data.inStrShear_dorc = str_shear.SelectSingleNode("discr_1_conti_0").InnerText
                all_data.inStrShear_bf = str_shear.SelectSingleNode("bf").InnerText
                all_data.inStrShear_sf = str_shear.SelectSingleNode("sf").InnerText
                all_data.inStrShear_frpt = str_shear.SelectSingleNode("frp_type").InnerText
                Dim anyEmptyShear As Boolean = all_data.inStrShear_fuf = "" Or all_data.inStrShear_ef = "" Or
                    all_data.inStrShear_us = "" Or all_data.inStrShear_ls = "" Or all_data.inStrShear_tf = "" Or
                    all_data.inStrShear_ang = "" Or all_data.inStrShear_nf = "" Or all_data.inStrShear_dorc = -1 Or
                    all_data.inStrShear_frpt = ""
                If (all_data.inStrShear_dorc = 1) Then
                    anyEmptyShear = anyEmptyShear Or all_data.inStrShear_bf = "" Or all_data.inStrShear_sf = ""
                End If
                'Update UI CheckBox     
                uipicStrShear.Image = If(anyEmptyShear, Nothing, My.Resources.Check)

                ' Strengthening Confinement
                Dim str_cfnmt As XmlNode = m_xmld.SelectSingleNode("rccsa/strengthening_confine")
                all_data.inStrCfnmt_fuf = str_cfnmt.SelectSingleNode("fuf").InnerText
                all_data.inStrCfnmt_ef = str_cfnmt.SelectSingleNode("ef").InnerText
                all_data.inStrCfnmt_us = str_cfnmt.SelectSingleNode("ult_strain").InnerText
                all_data.inStrCfnmt_ls = str_cfnmt.SelectSingleNode("lim_strain").InnerText
                all_data.inStrCfnmt_tf = str_cfnmt.SelectSingleNode("tf").InnerText
                all_data.inStrCfnmt_rad = str_cfnmt.SelectSingleNode("radius").InnerText
                all_data.inStrCfnmt_nf = str_cfnmt.SelectSingleNode("nf").InnerText
                all_data.inStrCfnmt_dorc = str_cfnmt.SelectSingleNode("discr_1_conti_0").InnerText
                all_data.inStrCfnmt_bf = str_cfnmt.SelectSingleNode("bf").InnerText
                all_data.inStrCfnmt_sf = str_cfnmt.SelectSingleNode("sf").InnerText
                all_data.inStrCfnmt_frpt = str_cfnmt.SelectSingleNode("frp_type").InnerText
                'Check List bahwa semua field pada StrConfine terisi
                Dim anyEmptyCfnmt As Boolean = all_data.inStrCfnmt_fuf = "" Or all_data.inStrCfnmt_ef = "" Or
                    all_data.inStrCfnmt_us = "" Or all_data.inStrCfnmt_ls = "" Or all_data.inStrCfnmt_tf = "" Or
                    all_data.inStrCfnmt_rad = "" Or all_data.inStrCfnmt_nf = "" Or all_data.inStrCfnmt_dorc = -1 Or
                    all_data.inStrCfnmt_frpt = ""
                If (all_data.inStrCfnmt_dorc = 1) Then
                    anyEmptyCfnmt = anyEmptyCfnmt Or all_data.inStrCfnmt_bf = "" Or all_data.inStrCfnmt_sf = ""
                End If
                'Update UI CheckBox       
                uipicStrCfnmt.Image = If(anyEmptyCfnmt, Nothing, My.Resources.Check)

                '============================================================ Load TRANSVERSE REINFORCEMENT
                Dim transverse As XmlNode = m_xmld.SelectSingleNode("rccsa/transverse")
                all_data.inTransverse_fys = transverse.SelectSingleNode("fys").InnerText
                all_data.inTransverse_ess = transverse.SelectSingleNode("ess").InnerText
                all_data.inTransverse_dia = transverse.SelectSingleNode("dia").InnerText
                all_data.inTransverse_spacing = transverse.SelectSingleNode("spacing").InnerText
                all_data.inTransverse_type = Convert.ToInt32(transverse.SelectSingleNode("type").InnerText)

                ''if all input is filled, put check mark
                Dim anyEmptyTr As Boolean = all_data.inTransverse_fys = "" Or all_data.inTransverse_ess = "" Or
                    all_data.inTransverse_dia = "" Or all_data.inTransverse_spacing = "" Or all_data.inTransverse_type = -1
                If (Not anyEmptyTr) Then
                    uipicTransReinf.Image = My.Resources.Check
                Else
                    uipicTransReinf.Image = Nothing
                End If

                '============================================================ Load TRANSVERSE REINFORCEMENT
                Dim stConfine As XmlNode = m_xmld.SelectSingleNode("rccsa/steel_confinement")
                all_data.inSteelConf_wcx = stConfine.SelectSingleNode("wcx").InnerText
                all_data.inSteelConf_hcy = stConfine.SelectSingleNode("hcy").InnerText
                all_data.inSteelConf_bc = stConfine.SelectSingleNode("bc").InnerText
                all_data.inSteelConf_tc = stConfine.SelectSingleNode("tc").InnerText
                all_data.inSteelConf_slx = stConfine.SelectSingleNode("slx").InnerText
                all_data.inSteelConf_sly = stConfine.SelectSingleNode("sly").InnerText
                all_data.inSteelConf_nlx = stConfine.SelectSingleNode("nlx").InnerText
                all_data.inSteelConf_nly = stConfine.SelectSingleNode("nly").InnerText

                ''if all input is filled, put check mark
                Dim anyEmptyStc As Boolean = all_data.inSteelConf_wcx = "" Or all_data.inSteelConf_hcy = "" Or
                    all_data.inSteelConf_bc = "" Or all_data.inSteelConf_tc = "" Or all_data.inSteelConf_slx = "" Or
                    all_data.inSteelConf_sly = "" Or all_data.inSteelConf_nlx = "" Or all_data.inSteelConf_nly = ""
                If (Not anyEmptyStc) Then
                    uipicSteelConfine.Image = My.Resources.Check
                Else
                    uipicSteelConfine.Image = Nothing
                End If

                '============================================================ Load CURVATURE
                Dim curvature As XmlNode = m_xmld.SelectSingleNode("rccsa/curvature")
                inCurv_incr.Text = curvature.SelectSingleNode("incr").InnerText
                inCurv_max.Text = curvature.SelectSingleNode("max").InnerText


                '============================================================ Load LONGITUDAL REINFORCEMENT
                '============================STEEL REINFORCEMENT
                'ambil data layer dari xml
                Dim steels As XmlNodeList = m_xmld.SelectNodes("rccsa/longitudal_reinforcement/steel_reinforcement/layers/steel")
                'inisiasi ulang variabel
                ReDim all_data.steel_layerData(steels.Count - 1)
                'ambil setiap layer
                For index As Integer = 0 To steels.Count - 1
                    Dim steel As XmlNode = steels.Item(index)

                    Dim dia As Double = Convert.ToDouble(steel.SelectSingleNode("dia").InnerText)
                    Dim n As Double = Convert.ToDouble(steel.SelectSingleNode("n").InnerText)
                    Dim asv As Double = Convert.ToDouble(steel.SelectSingleNode("as").InnerText)
                    Dim fy As Double = Convert.ToDouble(steel.SelectSingleNode("fy").InnerText)
                    Dim es As Double = Convert.ToDouble(steel.SelectSingleNode("es").InnerText)
                    Dim rup As Double = Convert.ToDouble(steel.SelectSingleNode("rup").InnerText)
                    Dim di As Double = Convert.ToDouble(steel.SelectSingleNode("di").InnerText)
                    Dim matModel As String = steel.SelectSingleNode("mat_model").InnerText

                    all_data.steel_layerData(index) = New LayerDataType_Steel(dia, n, asv, fy, es, rup, di, matModel)
                Next
                '============================STEEL REINFORCEMENT UI
                'Centangan pada longitudinal reinforcement, steel
                If (all_data.steel_layerData.Length > 0) Then
                    uipicSteel.Image = My.Resources.Check
                Else
                    uipicSteel.Image = Nothing
                End If

                '============================FRP REINFORCEMENT
                'ambil data layer dari xml
                Dim frps As XmlNodeList = m_xmld.SelectNodes("rccsa/longitudal_reinforcement/frp_reinforcement/layers/frp")
                'inisiasi ulang variabel
                ReDim all_data.frp_layerData(frps.Count - 1)
                'ambil setiap layer
                For index As Integer = 0 To frps.Count - 1
                    Dim frp As XmlNode = frps.Item(index)

                    Dim dia As Double = Convert.ToDouble(frp.SelectSingleNode("dia").InnerText)
                    Dim n As Double = Convert.ToDouble(frp.SelectSingleNode("n").InnerText)
                    Dim af As Double = Convert.ToDouble(frp.SelectSingleNode("af").InnerText)
                    Dim ffu As Double = Convert.ToDouble(frp.SelectSingleNode("ffu").InnerText)
                    Dim ef As Double = Convert.ToDouble(frp.SelectSingleNode("ef").InnerText)
                    Dim rup As Double = Convert.ToDouble(frp.SelectSingleNode("rup").InnerText)
                    Dim di As Double = Convert.ToDouble(frp.SelectSingleNode("di").InnerText)
                    Dim matModel As String = frp.SelectSingleNode("mat_model").InnerText

                    all_data.frp_layerData(index) = New LayerDataType_FRP(dia, n, af, ffu, ef, rup, di, matModel)
                Next
                '============================FRP REINFORCEMENT UI
                'Centangan pada longitudinal reinforcement, frp
                If (all_data.frp_layerData.Length > 0) Then
                    uipicFRP.Image = My.Resources.Check
                Else
                    uipicFRP.Image = Nothing
                End If

                '============================PRESTRESSED REINFORCEMENT
                'ambil data layer dari xml
                Dim prestresseds As XmlNodeList = m_xmld.SelectNodes("rccsa/longitudal_reinforcement/prestressed_reinforcement/layers/prestressed")
                'inisiasi ulang variabel
                ReDim all_data.prestressed_layerData(prestresseds.Count - 1)
                'ambil setiap layer
                For index As Integer = 0 To prestresseds.Count - 1
                    Dim prestressed As XmlNode = prestresseds.Item(index)

                    Dim dia As Double = Convert.ToDouble(prestressed.SelectSingleNode("dia").InnerText)
                    Dim n As Double = Convert.ToDouble(prestressed.SelectSingleNode("n").InnerText)
                    Dim ap As Double = Convert.ToDouble(prestressed.SelectSingleNode("ap").InnerText)
                    Dim fyp As Double = Convert.ToDouble(prestressed.SelectSingleNode("fyp").InnerText)
                    Dim esp As Double = Convert.ToDouble(prestressed.SelectSingleNode("esp").InnerText)
                    Dim rup As Double = Convert.ToDouble(prestressed.SelectSingleNode("rup").InnerText)
                    Dim di As Double = Convert.ToDouble(prestressed.SelectSingleNode("di").InnerText)
                    Dim prestrain As Double = Convert.ToDouble(prestressed.SelectSingleNode("prestrain").InnerText)
                    Dim matModel As String = prestressed.SelectSingleNode("mat_model").InnerText

                    all_data.prestressed_layerData(index) = New LayerDataType_Prestressed(dia, n, ap, fyp, esp, rup, di, prestrain, matModel)
                Next
                '============================PRESTRESSED REINFORCEMENT UI
                'Centangan pada longitudinal reinforcement, prestressed
                If (all_data.prestressed_layerData.Length > 0) Then
                    uipicPrestressed.Image = My.Resources.Check
                Else
                    uipicPrestressed.Image = Nothing
                End If

                '============================================================ Load REPEATED LOAD
                Dim analysis_type As String = m_xmld.SelectSingleNode("rccsa/analysis_type").InnerText
                If (analysis_type = "0") Then
                    inAnlyPositive.Checked = True
                ElseIf (analysis_type = "1") Then
                    inAnlyNegative.Checked = True
                ElseIf (analysis_type = "2") Then
                    inAnlyRepeated.Checked = True
                End If

                'ambil data layer dari xml
                Dim repLoad As XmlNodeList = m_xmld.SelectNodes("rccsa/repeated_load_data/data/cyclic")
                'inisiasi ulang variabel
                ReDim all_data.repLoadData(repLoad.Count - 1)
                'ambil setiap layer
                For index As Integer = 0 To repLoad.Count - 1
                    'Ambil baris
                    Dim rld As XmlNode = repLoad.Item(index)
                    'Ambil kolom
                    Dim cyclic_no As Integer = Convert.ToDouble(rld.SelectSingleNode("cyclic_no").InnerText)
                    Dim num_of_rep As Integer = Convert.ToDouble(rld.SelectSingleNode("num_of_rep").InnerText)
                    Dim turning_point_plus As Integer = Convert.ToDouble(rld.SelectSingleNode("turning_point_plus").InnerText)
                    Dim turning_point_min As Integer = Convert.ToDouble(rld.SelectSingleNode("turning_point_min").InnerText)
                    'Masukkan ke data
                    all_data.repLoadData(index) = New RepeatedLoadData(cyclic_no, num_of_rep, turning_point_plus, turning_point_min)
                Next
                '============================REP LOAD UI
                'Centangan pada longitudinal reinforcement, prestressed
                If (all_data.repLoadData.Length > 0) Then
                    uipicRepLoadData.Image = My.Resources.Check
                Else
                    uipicRepLoadData.Image = Nothing
                End If

                '============================Menghitung NR
                count_NR_Layer_and_UI()
                '============================Reset
                Me.resetAllChart()

            Catch errorVariable As Exception
                'Error trapping
                Console.Write(errorVariable.ToString())
                MsgBox(errorVariable.ToString())
            Finally

                setWindowText(filename)

            End Try
        End If
    End Sub

    ''' <summary>
    ''' Membangkitkan file data.txt dengan pemformatan (urutan data) yang telah disepakati
    ''' </summary>
    Private Sub saveData_intoDataTxt()
        'Inisiasi pembuat string untuk ditulis ke fiel
        Dim sb As StringBuilder = New StringBuilder()

        'Bangkitkan teks, ambil langsung dari input textbox
        sb.AppendLine("/******************** Cross Section: bw, H, dia, bf1, bf2, tf1, tf2 ********************/")
        Dim CrossSect_bw = If(inCrossSect_bw.Text = "", "0", inCrossSect_bw.Text)
        Dim CrossSect_H = If(inCrossSect_H.Text = "", "0", inCrossSect_H.Text)
        Dim CrossSect_dia = If(inCrossSect_dia.Text = "", "0", inCrossSect_dia.Text)
        Dim CrossSect_bf1 = If(inCrossSect_bf1.Text = "", "0", inCrossSect_bf1.Text)
        Dim CrossSect_bf2 = If(inCrossSect_bf2.Text = "", "0", inCrossSect_bf2.Text)
        Dim CrossSect_tf1 = If(inCrossSect_tf1.Text = "", "0", inCrossSect_tf1.Text)
        Dim CrossSect_tf2 = If(inCrossSect_tf2.Text = "", "0", inCrossSect_tf2.Text)
        If (csOptionRect.Checked) Then
            sb.AppendLine(CrossSect_bw)
            sb.AppendLine(CrossSect_H)
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
        ElseIf (csOptionCircle.Checked) Then
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(CrossSect_dia)
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
        ElseIf (csOptionTsection.Checked) Then
            sb.AppendLine(CrossSect_bw)
            sb.AppendLine(CrossSect_H)
            sb.AppendLine(0)
            sb.AppendLine(CrossSect_bf1)
            sb.AppendLine(0)
            sb.AppendLine(CrossSect_tf1)
            sb.AppendLine(0)
        ElseIf (csOptionIsection.Checked) Then
            sb.AppendLine(CrossSect_bw)
            sb.AppendLine(CrossSect_H)
            sb.AppendLine(0)
            sb.AppendLine(CrossSect_bf1)
            sb.AppendLine(CrossSect_bf2)
            sb.AppendLine(CrossSect_tf1)
            sb.AppendLine(CrossSect_tf2)
        Else
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
            sb.AppendLine(0)
        End If

        sb.AppendLine("/******************** Beam Dimensions: a1, L1, a2, L2, aDistributed, LDistributed ********************/")
        Dim BeamDim_A = If(inBeamDim_A.Text = "", "0", inBeamDim_A.Text)
        Dim BeamDim_L = If(inBeamDim_L.Text = "", "0", inBeamDim_L.Text)
        If (pointLoadOptionOne.Checked) Then
            sb.AppendLine(BeamDim_A)
            sb.AppendLine(BeamDim_L)
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine("0")
        ElseIf (pointLoadOptionTwo.Checked) Then
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine(BeamDim_A)
            sb.AppendLine(BeamDim_L)
            sb.AppendLine("0")
            sb.AppendLine("0")
        ElseIf (pointLoadOptionDistributed.Checked) Then
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine(BeamDim_L)
        Else
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine("0")
            sb.AppendLine("0")
        End If

        sb.AppendLine("/******************** Concrete Material Properties: concrete_stress_strain_model [1=Mander, 2=Almusallam, 3=Hognestad, 4=Todeschini, 5=Kent&Park, 6=ConcreteModel1, 7=ConcreteModel2, 8=ConcreteModel3, 9=ConcreteModel4, 10=High Strength Concrete1, 11=High Strength Concrete2, 12=Cyc.Con.Model1, 13=Cyc.Con.Model2], tension_stif_efx[1=Exponential Model, 2=Linear Model, 3=NTS Model], comp_strength_fc', tensile_str_ft, max_comp_strain ********************/")
        sb.AppendLine(inCMP_ConcreteSSModel.SelectedIndex + 1)
        sb.AppendLine(inCMP_TensionStiffEffect.SelectedIndex + 1)
        sb.AppendLine(If(inCMP_CompStrength.Text = "", "0", inCMP_CompStrength.Text))
        sb.AppendLine(If(inCMP_TensileStrength.Text = "", "0", inCMP_TensileStrength.Text))
        sb.AppendLine(If(inCMP_MaxCompStrain.Text = "", "0", inCMP_MaxCompStrain.Text))

        sb.AppendLine("/******************** LONGITUDAL REINFORCEMENT ********************/")
        sb.AppendLine("/*--- NR total (number of reinforcement layer), is_hybrid[0=no,1=yes] ---*/")
        sb.AppendLine(all_data.nr_total)
        sb.AppendLine(If(all_data.is_hybrid, "1", "0"))
        sb.AppendLine("/*--- steel layers info: n_items  ---*/")
        sb.AppendLine(all_data.steel_layerData.Length)
        sb.AppendLine("/*--- steel layers: dia n as fy es rup di stressStrainModel[0=Empty, 1=Bi-linear,2=Tri-linear,3=Strain Hardening,4=Cylic Steel1,5=Cylic Steel2] ---*/")
        For Each stl In all_data.steel_layerData
            sb.AppendLine(stl.dia & " " & stl.N & " " & stl.asv & " " & stl.fy & " " & stl.es & " " & stl.rup & " " & stl.di & " " & stl.matModel)
        Next

        sb.AppendLine("/*--- frp layers info: n_items  ---*/")
        sb.AppendLine(all_data.frp_layerData.Length)
        sb.AppendLine("/*--- frp layers: dia n af ffu ef rup di stressStrainModel[0=Empty, 1=Linear,2=Hybrid,3=Ductile,4=Cylic FRP]  ---*/")
        For Each frp In all_data.frp_layerData
            sb.AppendLine(frp.dia & " " & frp.N & " " & frp.af & " " & frp.ffu & " " & frp.ef & " " & frp.rup & " " & frp.di & " " & frp.matModel)
        Next

        sb.AppendLine("/*--- prestressed layers info: n_items ---*/")
        sb.AppendLine(all_data.prestressed_layerData.Length)
        sb.AppendLine("/*--- prestressed layers: dia n ap fyp esp rup di prestrain stressStrainModel[0=Empty, 1=Bi-liner,2=Prestressed,3=Cylic Prestressed]  ---*/")
        For Each prs In all_data.prestressed_layerData
            sb.AppendLine(prs.dia & " " & prs.N & " " & prs.ap & " " & prs.fyp & " " & prs.esp & " " & prs.rup & " " & prs.di & " " & prs.prestrain & " " & prs.matModel)
        Next

        sb.AppendLine("/************************************* STRENGTENING  *************************************/")
        sb.AppendLine("/**** Flexural: fup, ep, ult_strain, lim_strain, bp, tp, di   ****************************/")
        sb.AppendLine(If(all_data.inStrFlex_fup = "", "0", all_data.inStrFlex_fup))
        sb.AppendLine(If(all_data.inStrFlex_ep = "", "0", all_data.inStrFlex_ep))
        sb.AppendLine(If(all_data.inStrFlex_us = "", "0", all_data.inStrFlex_us))
        sb.AppendLine(If(all_data.inStrFlex_ls = "", "0", all_data.inStrFlex_ls))
        sb.AppendLine(If(all_data.inStrFlex_bp = "", "0", all_data.inStrFlex_bp))
        sb.AppendLine(If(all_data.inStrFlex_tp = "", "0", all_data.inStrFlex_tp))
        sb.AppendLine(If(all_data.inStrFlex_di = "", "0", all_data.inStrFlex_di))
        sb.AppendLine("/**** Shear: fuf, ef, ult_strain, lim_strain, tf, angle, nf, dc[1:discr;0=cont], bf, sf, frp_type[1:cfrp;2=gfrp;3=afrp]   **/")
        sb.AppendLine(If(all_data.inStrShear_fuf = "", "0", all_data.inStrShear_fuf))
        sb.AppendLine(If(all_data.inStrShear_ef = "", "0", all_data.inStrShear_ef))
        sb.AppendLine(If(all_data.inStrShear_us = "", "0", all_data.inStrShear_us))
        sb.AppendLine(If(all_data.inStrShear_ls = "", "0", all_data.inStrShear_ls))
        sb.AppendLine(If(all_data.inStrShear_tf = "", "0", all_data.inStrShear_tf))
        sb.AppendLine(If(all_data.inStrShear_ang = "", "0", all_data.inStrShear_ang))
        sb.AppendLine(If(all_data.inStrShear_nf = "", "0", all_data.inStrShear_nf))
        sb.AppendLine(all_data.inStrShear_dorc)
        sb.AppendLine(If(all_data.inStrShear_bf = "", "0", all_data.inStrShear_bf))
        sb.AppendLine(If(all_data.inStrShear_sf = "", "0", all_data.inStrShear_sf))
        sb.AppendLine(If(all_data.inStrShear_frpt = "", "0", all_data.inStrShear_frpt))
        sb.AppendLine("/**** Confinement: fuf, ef, ult_strain, lim_strain, tf, radius, nf, dc[1:discr;0=cont], bf, sf, frp_type[1:cfrp;2=gfrp;3=afrp]   **/")
        sb.AppendLine(If(all_data.inStrCfnmt_fuf = "", "0", all_data.inStrCfnmt_fuf))
        sb.AppendLine(If(all_data.inStrCfnmt_ef = "", "0", all_data.inStrCfnmt_ef))
        sb.AppendLine(If(all_data.inStrCfnmt_us = "", "0", all_data.inStrCfnmt_us))
        sb.AppendLine(If(all_data.inStrCfnmt_ls = "", "0", all_data.inStrCfnmt_ls))
        sb.AppendLine(If(all_data.inStrCfnmt_tf = "", "0", all_data.inStrCfnmt_tf))
        sb.AppendLine(If(all_data.inStrCfnmt_rad = "", "0", all_data.inStrCfnmt_rad))
        sb.AppendLine(If(all_data.inStrCfnmt_nf = "", "0", all_data.inStrCfnmt_nf))
        sb.AppendLine(all_data.inStrCfnmt_dorc)
        sb.AppendLine(If(all_data.inStrCfnmt_bf = "", "0", all_data.inStrCfnmt_bf))
        sb.AppendLine(If(all_data.inStrCfnmt_sf = "", "0", all_data.inStrCfnmt_sf))
        sb.AppendLine(If(all_data.inStrCfnmt_frpt = "", "0", all_data.inStrCfnmt_frpt))

        sb.AppendLine("/******************** TRANSVERSE REINFORCEMENT: fys, ess, dia, spacing, type[1=tied, 2=spirally] ********************/")
        sb.AppendLine(If(all_data.inTransverse_fys = "", "0", all_data.inTransverse_fys))
        sb.AppendLine(If(all_data.inTransverse_ess = "", "0", all_data.inTransverse_ess))
        sb.AppendLine(If(all_data.inTransverse_dia = "", "0", all_data.inTransverse_dia))
        sb.AppendLine(If(all_data.inTransverse_spacing = "", "0", all_data.inTransverse_spacing))
        sb.AppendLine(all_data.inTransverse_type + 1)

        sb.AppendLine("/******************** STEEL CONFINEMENT: wcx, hcy, bc, tc, slx, sly, nlx, nly ********************/")
        sb.AppendLine(If(all_data.inSteelConf_wcx = "", "0", all_data.inSteelConf_wcx))
        sb.AppendLine(If(all_data.inSteelConf_hcy = "", "0", all_data.inSteelConf_hcy))
        sb.AppendLine(If(all_data.inSteelConf_bc = "", "0", all_data.inSteelConf_bc))
        sb.AppendLine(If(all_data.inSteelConf_tc = "", "0", all_data.inSteelConf_tc))
        sb.AppendLine(If(all_data.inSteelConf_slx = "", "0", all_data.inSteelConf_slx))
        sb.AppendLine(If(all_data.inSteelConf_sly = "", "0", all_data.inSteelConf_sly))
        sb.AppendLine(If(all_data.inSteelConf_nlx = "", "0", all_data.inSteelConf_nlx))
        sb.AppendLine(If(all_data.inSteelConf_nly = "", "0", all_data.inSteelConf_nly))

        sb.AppendLine("/******************** DATA for COLUMN ANALYSIS: p, number_of_point_in_interaction_diagram ********************/")
        sb.AppendLine(If(inDataAnly_P.Text = "", "0", inDataAnly_P.Text))
        sb.AppendLine(If(inDataAnly_N.Text = "", "0", inDataAnly_N.Text))

        sb.AppendLine("/******************** CURVATURE: incr_curv, max_curv ********************/")
        sb.AppendLine(If(inCurv_incr.Text = "", "0", inCurv_incr.Text))
        sb.AppendLine(If(inCurv_max.Text = "", "0", inCurv_max.Text))

        sb.AppendLine("/********* TYPE OF ANALYSYS (0=positif, 1=negative, 2=repeated) *********/")
        sb.AppendLine(If(inAnlyPositive.Checked, "0",
                          If(inAnlyNegative.Checked, "1",
                             If(inAnlyRepeated.Checked, "2", "-1"))))

        sb.AppendLine("/******************** REPEATED LOAD DATA ********************/")
        sb.AppendLine("/*--- Number of Cyclic ---*/")
        sb.AppendLine(all_data.repLoadData.Length)
        sb.AppendLine("/*--- cyclic_no, number_of_repetition, turning_point(+), turning_point(-) ---*/")
        For Each rld As RepeatedLoadData In all_data.repLoadData
            sb.AppendLine(rld.no & " " & rld.numRepetition & " " & rld.turnPointPlus & " " & rld.turnPointMin)
        Next

        'Tulis teks ke DATA.txt
        Using outfile As StreamWriter = New StreamWriter("DATA.txt")
            outfile.Write(sb.ToString())
            'MsgBox("DATA.txt is saved")
        End Using
    End Sub

    Private Function printStringCheckNull(theString As String) As String
        Return If(theString = "", "0", theString)
    End Function

    ''' <summary>
    ''' Mengembalikan seluruh input pada tab input ke nilai default.
    ''' </summary>
    Private Sub ResetInput()

        ''/** Change tab focus and window title **/
        TabControl1.SelectedTab = tabPage_input
        Me.Text = "RCCSA - <New Input>"

        ''/******************** Cross Section: bw, H, dia, bf1, bf2, tf1, tf2 ********************/
        inCrossSect_bw.Text = ""
        inCrossSect_H.Text = ""
        inCrossSect_dia.Text = ""
        inCrossSect_bf1.Text = ""
        inCrossSect_bf2.Text = ""
        inCrossSect_tf1.Text = ""
        inCrossSect_tf2.Text = ""
        csOptionRect.Checked = False
        csOptionCircle.Checked = False
        csOptionIsection.Checked = False
        csOptionTsection.Checked = False

        ''/******************** Beam Dimensions: a1, L1, a2, L2, aDistributed, LDistributed ********************/
        inBeamDim_A.Text = ""
        inBeamDim_L.Text = ""
        pointLoadOptionOne.Checked = False
        pointLoadOptionTwo.Checked = False
        pointLoadOptionDistributed.Checked = False

        ''/******************** Concrete Material Properties: concrete_stress_strain_model, tension_stif_efx ********************/
        inCMP_ConcreteSSModel.SelectedIndex = -1
        inCMP_TensionStiffEffect.SelectedIndex = -1
        inCMP_CompStrength.Text = ""
        inCMP_TensileStrength.Text = ""
        inCMP_MaxCompStrain.Text = "0.003"

        ''/******************** Logitudal Reinforcement ********************/
        all_data.resetLongitudalReinfData()

        ''/******************** Strengthening, TransFeinf, dan Steel Confinement ********************/
        all_data.resetStrengtheningData()
        all_data.resetTransReinfData()

        ''/******************** DATA for COLUMN ANALYSIS ********************/")
        all_data.resetLoadAnalysis()
        inDataAnly_P.Text = ""
        inDataAnly_N.Text = ""
        inAnlyNegative.Checked = False
        inAnlyPositive.Checked = False
        inAnlyRepeated.Checked = False

        ''/******************** CURVATURE: incr_curv, max_curv ********************/")
        inCurv_incr.Text = "0.000001"
        inCurv_max.Text = "0.1"

        ''/******** USER INTERFACE *******/
        Me.count_NR_Layer_and_UI()
        uipicSteel.Image = Nothing
        uipicFRP.Image = Nothing
        uipicTransReinf.Image = Nothing
        uipicSteelConfine.Image = Nothing
        uipicStrFlex.Image = Nothing
        uipicStrShear.Image = Nothing
        uipicStrCfnmt.Image = Nothing
        uipicPrestressed.Image = Nothing
        uipicRepLoadData.Image = Nothing
    End Sub


    '/**********************************************************************************************************/
    '/****************************************************************************   MISC FUNCTION & SUB       */
    '/**********************************************************************************************************/

    '* Menghitung jumlah NR dan memutakhirkan label jumlah NR dan status hibrid
    Private Sub count_NR_Layer_and_UI()
        all_data.countNR_Layer()
        lblNLayerReinf.Text = all_data.nr_total
        If (all_data.is_hybrid) Then
            lblReinfHybrid.Text = "Yes"
        Else
            lblReinfHybrid.Text = "No"
        End If
    End Sub

    Private Sub ExitMenu_Click(sender As Object, e As EventArgs) Handles ExitMenu.Click
        Close()
    End Sub

    '/**********************************************************************************************************/
    '/****************************************************************************   DEFAULT & AUTO VALUE      */
    '/**********************************************************************************************************/
    ''' <summary>
    ''' Menggisi nilai otomatis kepada tensile strength ketika comp strength diganti nilainya menurut perhitungan 0.3*f'^2
    ''' </summary>
    Private Sub inCMP_CompStrength_TextChanged(sender As Object, e As EventArgs) Handles inCMP_CompStrength.TextChanged
        Try
            'ambil nilai fc', ubah dari string ke double
            Dim fc_accent As Double = Convert.ToDouble(inCMP_CompStrength.Text)
            'isi otomatis nilai ft
            inCMP_TensileStrength.Text = 0.3 * (fc_accent) ^ (1 / 2)
        Catch ex As Exception
            'Error (bisa terjadi saat fc' dikosongkan), abaikan saja
        End Try
    End Sub

    ''' <summary>
    ''' Mengisi nilai otomatis pada input curvature
    ''' </summary>
    Private Sub inCurv_incr_Leave(sender As Object, e As EventArgs) Handles inCurv_incr.Leave
        Try
            'ambil nilai incr, ubah dari string ke double
            Dim incr As Double = Convert.ToDouble(inCurv_incr.Text)

            If (incr <= 0.0000000001 Or incr >= 0.00001) Then
                MsgBox("Incremental value must be in between 0.0000000001 - 0.00001")
                inCurv_incr.Focus()
            End If
        Catch ex As Exception
            'Error (bisa terjadi saat incr dikosongkan), abaikan saja
        End Try
    End Sub

    ''' <summary>
    ''' Mengisi nilai otomatis untuk Strengthening with FRP Plate 'di' ketika
    ''' nilai H atau nilai tp diganti.
    ''' </summary>
    Private Sub inCrossSect_H_TextChanged(sender As Object, e As EventArgs) _
        Handles inCrossSect_H.TextChanged
        If (inCrossSect_H.Text <> "" And all_data.inStrFlex_tp <> "") Then
            Try
                'ambil nilai h dan tp, ubah dari string ke double
                Dim h As Double = Convert.ToDouble(inCrossSect_H.Text)
                Dim tp As Double = Convert.ToDouble(all_data.inStrFlex_tp)
                'isi otomatis nilai di
                Dim di = h + (0.5 * tp)
                all_data.inStrFlex_di = di
            Catch ex As Exception
                'Error (bisa terjadi saat fc' dikosongkan), abaikan saja
            End Try
        End If
    End Sub


    '/**********************************************************************************************************/
    '/**************************************************************************** CONTEXT MENU                */
    '/**********************************************************************************************************/
    ''' <summary>
    ''' Menjalankan fungsi reset ketika context menu reset chart (saat chart di klik kanan).
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub CmChartMenu_ResetChart_Click(sender As Object, e As EventArgs) Handles CmChartMenu_ResetChart.Click
        Dim myItem As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
        Dim cms As ContextMenuStrip = CType(myItem.Owner, ContextMenuStrip)

        If (cms.SourceControl.Name = "ChartMain") Then
            ChartHandler.resetChart(ChartMain, DgvMain)
        ElseIf (cms.SourceControl.Name = "ChartMatModel") Then
            ChartHandler.resetChart(ChartMatModel, DgvMatModel)
        End If
    End Sub

    Private Sub cmChartMenu_flipXaxis_Click(sender As Object, e As EventArgs) Handles cmChartMenu_flipXaxis.Click
        Dim myItem As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
        Dim cms As ContextMenuStrip = CType(myItem.Owner, ContextMenuStrip)

        If (cms.SourceControl.Name = "ChartMain") Then
            ChartMain.ChartAreas(0).AxisX.IsReversed = Not (ChartMain.ChartAreas(0).AxisX.IsReversed)
        ElseIf (cms.SourceControl.Name = "ChartMatModel") Then
            ChartMatModel.ChartAreas(0).AxisX.IsReversed = Not (ChartMatModel.ChartAreas(0).AxisX.IsReversed)
        End If
    End Sub

    Private Sub cmChartMenu_flipYaxis_Click(sender As Object, e As EventArgs) Handles cmChartMenu_flipYaxis.Click
        Dim myItem As ToolStripMenuItem = CType(sender, ToolStripMenuItem)
        Dim cms As ContextMenuStrip = CType(myItem.Owner, ContextMenuStrip)

        If (cms.SourceControl.Name = "ChartMain") Then
            ChartMain.ChartAreas(0).AxisY.IsReversed = Not (ChartMain.ChartAreas(0).AxisY.IsReversed)
        ElseIf (cms.SourceControl.Name = "ChartMatModel") Then
            ChartMatModel.ChartAreas(0).AxisY.IsReversed = Not (ChartMatModel.ChartAreas(0).AxisY.IsReversed)
        End If
    End Sub



    '/**********************************************************************************************************/
    '/**************************************************************************** MENU & TOOLBAR UMUM         */
    '/**********************************************************************************************************/
    Private Sub SaveFileMenu_Click(sender As Object, e As EventArgs) Handles SaveFileMenu.Click
        Me.saveData_intoXML()
    End Sub

    Private Sub OpenFileMenu_Click(sender As Object, e As EventArgs) Handles OpenFileMenu.Click
        Me.openData_fromXML()
        TabControl1.SelectedTab = tabPage_input
    End Sub

    Private Sub ToolBtnSaveXML_Click(sender As Object, e As EventArgs) Handles ToolBtnSaveXML.Click
        Me.saveData_intoXML()
    End Sub

    Private Sub ToolBtnOpenXML_Click(sender As Object, e As EventArgs) Handles ToolBtnOpenXML.Click
        Me.openData_fromXML()
        TabControl1.SelectedTab = tabPage_input
    End Sub

    Private Sub NewDataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NewDataToolStripMenuItem.Click
        Me.ResetInput()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        Dim dlg As New AboutBox1()
        dlg.ShowDialog()
    End Sub


    '/**********************************************************************************************************/
    '/**************************************************************************** SEKUENS PROGRAM KETIKA RUN  */
    '/**********************************************************************************************************/
    ''' <summary>
    ''' Mereset semua chart menjadi kosong
    ''' </summary>
    Private Sub resetAllChart()
        ChartHandler.resetChart(ChartMatModel, DgvMatModel)
        ChartHandler.resetChart(ChartMain, DgvMain)
        'Kembalikan judul tab
        tabPage_main.Text = "Chart (Empty)"
        'Hapus daftar di tombol dan dropdown
        Me.resetMainTabControl()
    End Sub

    ''' <summary>
    ''' Menghapus tombol, menu, dan dropdown di main tab chart
    ''' </summary>
    Private Sub resetMainTabControl()
        '' Kill button and toolbaritem (menu)
        Me.resetMainTabButtonAndMenu()
        '' Kill option in dropdown
        optChartMain.DataSource = New ChartClc().emptyCharts
        optChartMain.Text = ""
    End Sub

    ''' <summary>
    ''' Menghapus tombol dan  menu yang berasosiasi dengan main tab chart
    ''' </summary>
    Private Sub resetMainTabButtonAndMenu()
        '' Kill all previous button
        For Each Button As Button In buttons_inChartMain
            Button.Dispose()
        Next
        buttons_inChartMain.Clear()
        '' Kill all previous toolbarItem
        tbChartMain.DropDownItems.Clear()
    End Sub

    ''' <summary>
    ''' Menjalankan seluruh sekuens sebelum program.exe benar-benar dijalankan. 
    ''' Dan kemudian mengganti tab yang jadi fokus ke pengguna. Serta mendisable yang tidak fokus.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub runProgram_Sequence()
        'reset semua grafik
        resetAllChart()
        ''Uncomment the code below, kalau mau menyimpan txt dulu sebelum menjalankan program
        Me.saveData_intoDataTxt()
        ''Jalankan program.exe
        Me.runProgramExe()

        ''===============CEK SEGALA KONDISI UNTUK MENGGANTI ke TAB CHART
        If (all_data.repLoadData.Length > 0) Then
            'Jika rep load diisi, buka tab chart rep load
            prepareTab("repload")

        Else
            'Jika tidak ada rep load data yg idisi, ada tiga kemungkinan tab chart
            Dim NN, PP As Double
            If ((inDataAnly_N.Text = "" Or (Double.TryParse(inDataAnly_N.Text, NN) And NN = 0)) And
                (inDataAnly_P.Text = "" Or (Double.TryParse(inDataAnly_P.Text, PP) And PP = 0))) Then
                'jika kedua dari N dan P bernilai 0 (atau dikosongkan)

                'cek apakah beam dimension kosong
                Dim DimA, DimL As Double
                If ((inBeamDim_A.Text = "" Or (Double.TryParse(inBeamDim_A.Text, DimA) And DimA = 0)) And
                    (inBeamDim_L.Text = "" Or (Double.TryParse(inBeamDim_L.Text, DimL) And DimL = 0))) Then
                    'jika kedua nilai A dan L pada beam dimension kosong, buka tab section
                    prepareTab("section")

                Else
                    'jika salah ssatu nilai A dan L tidak kosong, buka tab beam
                    prepareTab("beam")
                End If
            ElseIf ((inDataAnly_N.Text <> "" Or (Double.TryParse(inDataAnly_N.Text, NN) And NN <> 0)) And
                (inDataAnly_P.Text = "" Or (Double.TryParse(inDataAnly_P.Text, PP) And PP = 0))) Then
                'jika  P bernilai 0 dan N tidak nol, buka tab column2
                prepareTab("column2")

            ElseIf ((inDataAnly_N.Text = "" Or (Double.TryParse(inDataAnly_N.Text, NN) And NN = 0)) And
                (inDataAnly_P.Text <> "" Or (Double.TryParse(inDataAnly_P.Text, PP) And PP <> 0))) Then
                'jika  N bernilai 0 dan P tidak nol, buka tab column7
                prepareTab("column7")
            End If
        End If

        'Beralih ke tab yang ada chart utamanya
        TabControl1.SelectedTab = tabPage_main
    End Sub

    ''' <summary>
    ''' Melakukan penyesuaian tab main berdasarkan tab yang sedang dibuka sekarang.
    ''' Juga menggenerate tombol dan menu toolbar item dengan tab yg bersesuaian.
    ''' </summary>
    ''' <param name="tab">Nama tab yang akan dibuka.</param>
    Private Sub prepareTab(tab As String)
        Dim chartCollection As ChartClc = New ChartClc()

        Select Case tab
            Case "section"
                tabPage_main.Text = "Section"
                optChartMain.DataSource = chartCollection.sectionCbbOptions
                generateChartMainControl(chartCollection.sectionCharts)
            Case "beam"
                tabPage_main.Text = "Beam"
                optChartMain.DataSource = chartCollection.beamCbbOptions
                generateChartMainControl(chartCollection.beamCharts)
            Case "column2"
                tabPage_main.Text = "Column"
                optChartMain.DataSource = chartCollection.columnCbbOptions2
                generateChartMainControl(chartCollection.columnCharts2)
            Case "column7"
                tabPage_main.Text = "Column"
                optChartMain.DataSource = chartCollection.columnCbbOptions7
                generateChartMainControl(chartCollection.columnCharts7)
            Case "repload"
                tabPage_main.Text = "Cyclic Load"
                optChartMain.DataSource = chartCollection.reploadCbbOptions
                generateChartMainControl(chartCollection.reploadCharts)
            Case Else

        End Select
        Me.optChartMain.DisplayMember = "display"
        Me.optChartMain.ValueMember = "value"
    End Sub

    ''' <summary>
    ''' Menggenerate control dari tab main yang sekarang, sesuai item chart yang ada.
    ''' Control termasuk tombol dan menu toolbaritem.
    ''' </summary>
    ''' <param name="items">Daftar item chart yng didefinisikan pada chartclc</param>
    Private Sub generateChartMainControl(items As BindingList(Of ChartItem))
        '' Kill all previous button and toolbarItem
        Me.resetMainTabButtonAndMenu()

        '' Generate new
        Dim buttonY = 10
        For Each cb As ChartItem In items
            ''Button
            Dim buttonChart = createButtonChart(cb.code, cb.shortTitle)
            buttonChart.Size = New Size(132, 35)
            buttonChart.Location = New Point(0, buttonY)
            buttonY += 40
            tabPage_main.Controls.Add(buttonChart)
            buttonChart.BringToFront()
            buttonChart.Visible = buttonVisible
            buttons_inChartMain.Add(buttonChart)

            ''Toolbaritem
            Dim toolChart = createToolItem(cb.code, cb.shortTitle)
            tbChartMain.DropDownItems.Add(toolChart)
        Next
    End Sub

    '/**********************************************************************************************************/
    '/****************************************************************************   HANDLER EKSEKUSI EXE      */
    '/**********************************************************************************************************/

    ''' <summary>
    ''' Membuat proses program.exe berjalan.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub runProgramExe()
        Dim myProcess As New Process()
        Try
            myProcess.StartInfo.UseShellExecute = False
            ' Starting program.exe
            myProcess.StartInfo.FileName = "program.exe"
            myProcess.StartInfo.CreateNoWindow = False
            myProcess.Start()
            ' This code assumes the process you are starting will terminate itself.  
            ' Given that is is started without a window so you cannot terminate it  
            ' on the desktop, it must terminate itself or you can do it programmatically 
            ' from this application using the Kill method. 

            'Setelah itu, update material, hmm...
            updateMaterialModelChartOption()
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub RunToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RunToolStripMenuItem.Click
        runProgram_Sequence()
    End Sub


    Private Sub ToolStripRun_Click(sender As Object, e As EventArgs) Handles ToolStripRun.Click
        runProgram_Sequence()
    End Sub

    '/**********************************************************************************************************/
    '/****************************************************************************   HELP FILE                 */
    '/**********************************************************************************************************/
    ''' <summary>
    ''' Menampilkan file help dari help.pdf
    ''' </summary>
    Private Sub HelpToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles HelpToolStripMenuItem1.Click
        Dim myProcess = New Process()
        Try
            myProcess.StartInfo.FileName = "help.pdf"
            myProcess.StartInfo.CreateNoWindow = True
            myProcess.Start()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    '/**********************************************************************************************************/
    '/****************************************************************************   DIALOG BOX CHART OPTIONS  */
    '/**********************************************************************************************************/
    ''' <summary>
    ''' Menampilkan dialog box chart option untuk mengubah segala hal (warna, teks) ttg chart yang sedang tampil
    ''' </summary>
    ''' <todo>Penyesuaian dengan tab main generate</todo>
    Private Sub toolbarChartOption_Click(sender As Object, e As EventArgs) Handles toolbarChartOption.Click, ChartOptionsToolStripMenuItem.Click, CmChartMenu_Options.Click
        Dim dlg As New DialogChartOptions()

        If (TabControl1.SelectedTab.Name = "tabPage_main") Then
            Debug.WriteLine(TabControl1.SelectedTab.Name)
            dlg.currentChart = ChartMain
            dlg.Text = "Options: Main Chart"
        ElseIf (TabControl1.SelectedTab.Name = "tabPage_chartMatModel") Then
            dlg.currentChart = ChartMatModel
            dlg.Text = "Options: Chart Material Model"
        Else
            dlg.modeAll = True
            dlg._main = ChartMain
            dlg._matModel = ChartMatModel
            dlg._tab = TabControl1
        End If
        dlg.ShowDialog()
    End Sub


    '/**********************************************************************************************************/
    '/**************************************************************************** DIALOG BOX ADD CUSTOM CHART */
    '/**********************************************************************************************************/

    ''' <summary>
    ''' Membuka dialog box add custom chart ketika menu di klik.
    ''' </summary>
    ''' <todo>penyesuaian dengan chart main generate</todo>
    Private Sub AddCustomChartToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddCustomChartToolStripMenuItem.Click
        'Dialog yg mau dibuka adalah dialog Steel Reinforcement
        Dim dlg As New AddCustomChart()

        ' Show the dialog and determine the state of the 
        ' DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then
            Dim fileName As String = dlg.inFilepath.Text
            Dim seriesName As String = dlg.inSeriesName.Text
            Dim labelXaxis As String = dlg.inLabelX.Text
            Dim labelYaxis As String = dlg.inLabelY.Text
            Dim intoChart As String = dlg.intoChart.SelectedValue

            If (intoChart = "main") Then
                If (dlg.cbAddToCurrentChart.Checked) Then
                    'Tambahkan grafik (mode=add) dan jangan ganti label sumbu X dan Y, serta warna dibuat terserah sistem
                    ChartHandler.addChartSeries(ChartMain, DgvMain, fileName, seriesName, , , , "add")
                Else
                    'Tambahkan grafik sebagai grafik baru, set label sesuai isian pengguna, warna terserah sistem
                    ChartHandler.addChartSeries(ChartMain, DgvMain, fileName, seriesName, labelXaxis, labelYaxis)
                End If
                'Fokus ke tab chart utama
                TabControl1.SelectedTab = tabPage_main

            ElseIf (intoChart = "mat_model") Then
                If (dlg.cbAddToCurrentChart.Checked) Then
                    'Tambahkan grafik (mode=add) dan jangan ganti label sumbu X dan Y, serta warna dibuat terserah sistem
                    ChartHandler.addChartSeries(ChartMatModel, DgvMatModel, fileName, seriesName, , , , "add")
                Else
                    'Tambahkan grafik sebagai grafik baru, set label sesuai isian pengguna, warna terserah sistem
                    ChartHandler.addChartSeries(ChartMatModel, DgvMatModel, fileName, seriesName, labelXaxis, labelYaxis)
                End If

                'Fokus ke tab chart utama
                TabControl1.SelectedTab = tabPage_chartMatModel
            End If
        End If
    End Sub

    '/**********************************************************************************************************/
    '/**************************************************************************** HANDLER on TAB MAT MODEL    */
    '/**********************************************************************************************************/
    Private Sub btnShowLoP_CheckedChanged(sender As Object, e As EventArgs) Handles btnShowLoP.CheckedChanged
        If (DgvMatModel.Visible) Then
            ChartMatModel.Width = 786
            DgvMatModel.Visible = False
        Else
            ChartMatModel.Width = 580
            DgvMatModel.Visible = True
        End If
    End Sub

    Private Sub btnChartMatModelUpdate_Click(sender As Object, e As EventArgs) Handles btnChartMatModelUpdate.Click
        ChartHandler.paintChart(ChartMatModel, DgvMatModel, optChartMatModel.SelectedValue)
    End Sub

    '/**********************************************************************************************************/
    '/****************************************************************************   HANDLER on TAB MAIN       */
    '/**********************************************************************************************************/

    Private Sub btnChartMainUpdate_Click(sender As Object, e As EventArgs) Handles btnChartMainUpdate.Click
        ChartHandler.paintChart(ChartMain, DgvMain, optChartMain.SelectedValue)
    End Sub

    Private Sub btnShowLoPMain_CheckedChanged(sender As Object, e As EventArgs) Handles btnShowLoPMain.CheckedChanged
        If (DgvMain.Visible) Then
            ChartMain.Width += 206
            DgvMain.Visible = False
        Else
            ChartMain.Width -= 206
            DgvMain.Visible = True
        End If
    End Sub

    Private Sub btnShowButtonMain_CheckedChanged(sender As Object, e As EventArgs) Handles btnShowButtonMain.CheckedChanged
        If (buttonVisible) Then
            For Each Button In buttons_inChartMain
                Button.Visible = False
            Next
            ChartMain.Width += 137
            ChartMain.Location = New Point(0, ChartMain.Location.Y)
            buttonVisible = False
        Else
            For Each Button In buttons_inChartMain
                Button.Visible = True
            Next
            ChartMain.Width -= 137
            ChartMain.Location = New Point(137, ChartMain.Location.Y)
            buttonVisible = True
        End If
    End Sub

    Private Sub btnTransReinf_Click(sender As Object, e As EventArgs) Handles btnTransReinf.Click
        'Dialog yg mau dibuka adalah dialog Trasverse Reinforcement
        Dim dlg As New DialogTransReinf()

        'Kirim data dari all_data ke dialog trans reinf
        dlg.inTransverse_fys.Text = all_data.inTransverse_fys
        dlg.inTransverse_ess.Text = all_data.inTransverse_ess
        dlg.inTransverse_dia.Text = all_data.inTransverse_dia
        dlg.inTransverse_spacing.Text = all_data.inTransverse_spacing
        dlg.inTransverse_type.SelectedIndex = all_data.inTransverse_type

        ' Show the dialog and determine the state of the DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then
            '=================== AMBIL DATA DARI DIALOG TransReinf
            'Ambil data trans reinf
            all_data.inTransverse_fys = dlg.inTransverse_fys.Text
            all_data.inTransverse_ess = dlg.inTransverse_ess.Text
            all_data.inTransverse_dia = dlg.inTransverse_dia.Text
            all_data.inTransverse_spacing = dlg.inTransverse_spacing.Text
            all_data.inTransverse_type = dlg.inTransverse_type.SelectedIndex

            'Check list bahwa trans reinf telah diisi
            Dim anyEmpty As Boolean = dlg.inTransverse_fys.Text = "" Or dlg.inTransverse_ess.Text = "" Or
            dlg.inTransverse_dia.Text = "" Or dlg.inTransverse_spacing.Text = "" Or dlg.inTransverse_type.SelectedIndex = -1

            If (anyEmpty) Then
                uipicTransReinf.Image = Nothing
            Else
                uipicTransReinf.Image = My.Resources.Check
            End If
        End If
    End Sub

    Private Sub btnSteelConfine_Click(sender As Object, e As EventArgs) Handles btnSteelConfine.Click
        'Dialog yg mau dibuka adalah dialog Steel Confinement
        Dim dlg As New DialogSteelConfine()

        'Kirim data dari all_data ke dialog trans reinf
        dlg.inSteelConf_wcx.Text = all_data.inSteelConf_wcx
        dlg.inSteelConf_hcy.Text = all_data.inSteelConf_hcy
        dlg.inSteelConf_bc.Text = all_data.inSteelConf_bc
        dlg.inSteelConf_tc.Text = all_data.inSteelConf_tc
        dlg.inSteelConf_slx.Text = all_data.inSteelConf_slx
        dlg.inSteelConf_sly.Text = all_data.inSteelConf_sly
        dlg.inSteelConf_nlx.Text = all_data.inSteelConf_nlx
        dlg.inSteelConf_nly.Text = all_data.inSteelConf_nly

        ' Show the dialog and determine the state of the DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then
            '=================== AMBIL DATA DARI DIALOG SteelConfine
            'Ambil data steel confinement
            all_data.inSteelConf_wcx = dlg.inSteelConf_wcx.Text
            all_data.inSteelConf_hcy = dlg.inSteelConf_hcy.Text
            all_data.inSteelConf_bc = dlg.inSteelConf_bc.Text
            all_data.inSteelConf_tc = dlg.inSteelConf_tc.Text
            all_data.inSteelConf_slx = dlg.inSteelConf_slx.Text
            all_data.inSteelConf_sly = dlg.inSteelConf_sly.Text
            all_data.inSteelConf_nlx = dlg.inSteelConf_nlx.Text
            all_data.inSteelConf_nly = dlg.inSteelConf_nly.Text

            'Check list bahwa trans reinf telah diisi
            Dim anyEmpty As Boolean = dlg.inSteelConf_wcx.Text = "" Or dlg.inSteelConf_hcy.Text = "" Or
            dlg.inSteelConf_bc.Text = "" Or dlg.inSteelConf_tc.Text = "" Or dlg.inSteelConf_slx.Text = "" Or
            dlg.inSteelConf_sly.Text = "" Or dlg.inSteelConf_nlx.Text = "" Or dlg.inSteelConf_nly.Text = ""

            If (anyEmpty) Then
                uipicSteelConfine.Image = Nothing
            Else
                uipicSteelConfine.Image = My.Resources.Check
            End If
        End If
    End Sub

    Private Sub btnStrFlex_Click(sender As Object, e As EventArgs) Handles btnStrFlex.Click
        'Dialog yg mau dibuka adalah dialog Strengthening Flexural
        Dim dlg As New DialogStrFlex()

        'Kirim data dari all_data ke Dialog StrFlex
        dlg.inStrFlex_bp.Text = all_data.inStrFlex_bp
        dlg.inStrFlex_di.Text = all_data.inStrFlex_di
        dlg.inStrFlex_ep.Text = all_data.inStrFlex_ep
        dlg.inStrFlex_fup.Text = all_data.inStrFlex_fup
        dlg.inStrFlex_ls.Text = all_data.inStrFlex_ls
        dlg.inStrFlex_tp.Text = all_data.inStrFlex_tp
        dlg.inStrFlex_us.Text = all_data.inStrFlex_us

        Try
            'isi h ke dialog untuk otomatis'
            If csOptionCircle.Checked() Then
                dlg.h = Convert.ToDouble(inCrossSect_dia.Text)
            Else
                dlg.h = Convert.ToDouble(inCrossSect_H.Text)
            End If
        Catch ex As Exception
            dlg.h = 0
        End Try

        ' Show the dialog and determine the state of the DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then
            'Ambil dan update data yg baru dimasukkan ke Dialog StrFlex
            all_data.inStrFlex_bp = dlg.inStrFlex_bp.Text
            all_data.inStrFlex_di = dlg.inStrFlex_di.Text
            all_data.inStrFlex_ep = dlg.inStrFlex_ep.Text
            all_data.inStrFlex_fup = dlg.inStrFlex_fup.Text
            all_data.inStrFlex_ls = dlg.inStrFlex_ls.Text
            all_data.inStrFlex_tp = dlg.inStrFlex_tp.Text
            all_data.inStrFlex_us = dlg.inStrFlex_us.Text

            'Check List bahwa semua field pada StrFlex terisi
            Dim anyEmpty As Boolean = all_data.inStrFlex_bp = "" Or all_data.inStrFlex_di = "" Or
                all_data.inStrFlex_ep = "" Or all_data.inStrFlex_fup = "" Or all_data.inStrFlex_ls = "" Or
                all_data.inStrFlex_tp = "" Or all_data.inStrFlex_us = ""

            'Update UI CheckBox          
            If (anyEmpty) Then
                uipicStrFlex.Image = Nothing
            Else
                uipicStrFlex.Image = My.Resources.Check
            End If
        End If
    End Sub

    Private Sub btnStrShear_Click(sender As Object, e As EventArgs) Handles btnStrShear.Click
        'Dialog yg mau dibuka adalah dialog Strengthening Shear
        Dim dlg As New DialogStrShear()
        'Kirim data dari all_data ke Dialog StrShear
        dlg.inStrShear_fuf.Text = all_data.inStrShear_fuf
        dlg.inStrShear_ef.Text = all_data.inStrShear_ef
        dlg.inStrShear_us.Text = all_data.inStrShear_us
        dlg.inStrShear_ls.Text = all_data.inStrShear_ls
        dlg.inStrShear_tf.Text = all_data.inStrShear_tf
        dlg.inStrShear_ang.Text = all_data.inStrShear_ang
        dlg.inStrShear_nf.Text = all_data.inStrShear_nf
        If (all_data.inStrShear_dorc = 0) Then
            dlg.inStrShear_continu.Checked = True
        Else
            dlg.inStrShear_discrit.Checked = True
            'hanya diisi jika pilihan discrete
            dlg.inStrShear_bf.Text = all_data.inStrShear_bf
            dlg.inStrShear_sf.Text = all_data.inStrShear_sf
        End If
        If (all_data.inStrShear_frpt = "1") Then
            dlg.inStrShear_cfrp.Checked = True
        ElseIf (all_data.inStrShear_frpt = "2") Then
            dlg.inStrShear_gfrp.Checked = True
        ElseIf (all_data.inStrShear_frpt = "3") Then
            dlg.inStrShear_afrp.Checked = True
        End If


        ' Show the dialog and determine the state of the DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then
            'Ambil dan update data yg baru dimasukkan ke Dialog StrShear
            all_data.inStrShear_fuf = dlg.inStrShear_fuf.Text
            all_data.inStrShear_ef = dlg.inStrShear_ef.Text
            all_data.inStrShear_us = dlg.inStrShear_us.Text
            all_data.inStrShear_ls = dlg.inStrShear_ls.Text
            all_data.inStrShear_tf = dlg.inStrShear_tf.Text
            all_data.inStrShear_ang = dlg.inStrShear_ang.Text
            all_data.inStrShear_nf = dlg.inStrShear_nf.Text
            If (dlg.inStrShear_continu.Checked) Then
                all_data.inStrShear_dorc = 0
                'dibuat nol jika pilihan continous
                all_data.inStrShear_bf = "0"
                all_data.inStrShear_sf = "0"
            ElseIf (dlg.inStrShear_discrit.Checked) Then
                all_data.inStrShear_dorc = 1
                'hanya diisi jika pilihan discrete
                all_data.inStrShear_bf = dlg.inStrShear_bf.Text
                all_data.inStrShear_sf = dlg.inStrShear_sf.Text
            Else
                all_data.inStrShear_dorc = -1
            End If
            If (dlg.inStrShear_cfrp.Checked) Then
                all_data.inStrShear_frpt = "1"
            ElseIf (dlg.inStrShear_gfrp.Checked) Then
                all_data.inStrShear_frpt = "2"
            ElseIf (dlg.inStrShear_afrp.Checked) Then
                all_data.inStrShear_frpt = "3"
            Else
                all_data.inStrShear_frpt = ""
            End If

            'Check List bahwa semua field pada StrShear terisi
            Dim anyEmpty As Boolean = all_data.inStrShear_fuf = "" Or all_data.inStrShear_ef = "" Or
                all_data.inStrShear_us = "" Or all_data.inStrShear_ls = "" Or all_data.inStrShear_tf = "" Or
                all_data.inStrShear_ang = "" Or all_data.inStrShear_nf = "" Or all_data.inStrShear_dorc = -1 Or
                all_data.inStrShear_frpt = ""
            If (all_data.inStrShear_dorc = 1) Then
                anyEmpty = anyEmpty Or all_data.inStrShear_bf = "" Or all_data.inStrShear_sf = ""
            End If

            'Update UI CheckBox                
            If (anyEmpty) Then
                uipicStrShear.Image = Nothing
            Else
                uipicStrShear.Image = My.Resources.Check
            End If
        End If
    End Sub

    Private Sub btnStrCnfmt_Click(sender As Object, e As EventArgs) Handles btnStrCnfmt.Click
        'Dialog yg mau dibuka adalah dialog Strengthening Confinement
        Dim dlg As New DialogStrCfnmt()
        'Kirim data dari all_data ke Dialog StrCfnmt
        dlg.inStrCfnmt_fuf.Text = all_data.inStrCfnmt_fuf
        dlg.inStrCfnmt_ef.Text = all_data.inStrCfnmt_ef
        dlg.inStrCfnmt_us.Text = all_data.inStrCfnmt_us
        dlg.inStrCfnmt_ls.Text = all_data.inStrCfnmt_ls
        dlg.inStrCfnmt_tf.Text = all_data.inStrCfnmt_tf
        dlg.inStrCfnmt_rad.Text = all_data.inStrCfnmt_rad
        dlg.inStrCfnmt_nf.Text = all_data.inStrCfnmt_nf
        If (all_data.inStrCfnmt_dorc = 0) Then
            dlg.inStrCfnmt_continu.Checked = True
        Else
            dlg.inStrCfnmt_discrit.Checked = True
            'hanya diisi jika pilihan discrete
            dlg.inStrCfnmt_bf.Text = all_data.inStrCfnmt_bf
            dlg.inStrCfnmt_sf.Text = all_data.inStrCfnmt_sf
        End If
        If (all_data.inStrCfnmt_frpt = "1") Then
            dlg.inStrCfnmt_cfrp.Checked = True
        ElseIf (all_data.inStrCfnmt_frpt = "2") Then
            dlg.inStrCfnmt_gfrp.Checked = True
        ElseIf (all_data.inStrCfnmt_frpt = "3") Then
            dlg.inStrCfnmt_afrp.Checked = True
        End If


        ' Show the dialog and determine the state of the DialogResult property for the form.
        If dlg.ShowDialog = DialogResult.OK Then
            'Ambil dan update data yg baru dimasukkan ke Dialog StrShear
            all_data.inStrCfnmt_fuf = dlg.inStrCfnmt_fuf.Text
            all_data.inStrCfnmt_ef = dlg.inStrCfnmt_ef.Text
            all_data.inStrCfnmt_us = dlg.inStrCfnmt_us.Text
            all_data.inStrCfnmt_ls = dlg.inStrCfnmt_ls.Text
            all_data.inStrCfnmt_tf = dlg.inStrCfnmt_tf.Text
            all_data.inStrCfnmt_rad = dlg.inStrCfnmt_rad.Text
            all_data.inStrCfnmt_nf = dlg.inStrCfnmt_nf.Text
            If (dlg.inStrCfnmt_continu.Checked) Then
                all_data.inStrCfnmt_dorc = 0
                'dibuat nol jika pilihan continous
                all_data.inStrCfnmt_bf = "0"
                all_data.inStrCfnmt_sf = "0"
            ElseIf (dlg.inStrCfnmt_discrit.Checked) Then
                all_data.inStrCfnmt_dorc = 1
                'hanya diisi jika pilihan discrete
                all_data.inStrCfnmt_bf = dlg.inStrCfnmt_bf.Text
                all_data.inStrCfnmt_sf = dlg.inStrCfnmt_sf.Text
            Else
                all_data.inStrCfnmt_dorc = -1
            End If
            If (dlg.inStrCfnmt_cfrp.Checked) Then
                all_data.inStrCfnmt_frpt = "1"
            ElseIf (dlg.inStrCfnmt_gfrp.Checked) Then
                all_data.inStrCfnmt_frpt = "2"
            ElseIf (dlg.inStrCfnmt_afrp.Checked) Then
                all_data.inStrCfnmt_frpt = "3"
            Else
                all_data.inStrCfnmt_frpt = ""
            End If

            'Check List bahwa semua field pada StrConfine terisi
            Dim anyEmpty As Boolean = all_data.inStrCfnmt_fuf = "" Or all_data.inStrCfnmt_ef = "" Or
                all_data.inStrCfnmt_us = "" Or all_data.inStrCfnmt_ls = "" Or all_data.inStrCfnmt_tf = "" Or
                all_data.inStrCfnmt_rad = "" Or all_data.inStrCfnmt_nf = "" Or all_data.inStrCfnmt_dorc = -1 Or
                all_data.inStrCfnmt_frpt = ""
            If (all_data.inStrCfnmt_dorc = 1) Then
                anyEmpty = anyEmpty Or all_data.inStrCfnmt_bf = "" Or all_data.inStrCfnmt_sf = ""
            End If

            'Update UI CheckBox                
            If (anyEmpty) Then
                uipicStrCfnmt.Image = Nothing
            Else
                uipicStrCfnmt.Image = My.Resources.Check
            End If
        End If
    End Sub

    Private Sub setWindowText(filename As String)
        Dim printPath As String = filename
        If (filename.Length > 66) Then
            Dim pathSplit As String()
            If (filename.IndexOf("\") <> -1) Then
                pathSplit = Split(filename, "\")
                printPath = filename.Substring(0, 22) & "~...\" & pathSplit.Last
            ElseIf (filename.IndexOf("/") <> -1) Then
                pathSplit = Split(filename, "/")
                printPath = filename.Substring(0, 22) & "~.../" & pathSplit.Last
            End If
        End If
        Me.Text = "RCCSA - " & printPath
    End Sub

    Private Sub inDataAnly_P_TextChanged(sender As Object, e As EventArgs) Handles inDataAnly_P.TextChanged
        inDataAnly_N.Text = ""
        'If (inDataAnly_P.Text <> "") Then
        '    MainTooltip.Show("Only one of P and Ni can be filled", inDataAnly_P)
        'Else
        '    MainTooltip.Hide(inDataAnly_P)
        'End If
    End Sub

    Private Sub inDataAnly_N_TextChanged(sender As Object, e As EventArgs) Handles inDataAnly_N.TextChanged
        inDataAnly_P.Text = ""
        'If (inDataAnly_N.Text <> "") Then
        '    MainTooltip.Show("Only one of P and Ni can be filled", inDataAnly_N)
        'Else
        '    MainTooltip.Hide(inDataAnly_N)
        'End If
    End Sub
End Class
