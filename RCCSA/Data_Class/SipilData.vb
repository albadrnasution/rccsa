﻿''' <summary>
''' Menyimpan data sipil rccsa untuk disimpan dalam xml.
''' </summary>
''' <author>Albadr Nasution</author>
Public Class SipilData

    '/**********************************************************************************************************/
    '/***************************************************************************   CONSTRUCTOR                */
    '/**********************************************************************************************************/
    Public Sub New()
        'Inisiasi awal layer data supaya kosong
        ReDim steel_layerData(-1)
        ReDim frp_layerData(-1)
        ReDim prestressed_layerData(-1)
        'Inisiasi awal rep load data supaya kosong
        ReDim repLoadData(-1)
    End Sub

    '/**********************************************************************************************************/
    '/***************************************************************************   REFERENSI                  */
    '/**********************************************************************************************************/

    'penyimpan yg sebenarnya optional, cuma buat deskripsi di xml simpanan
    Public cross_section_type_desc_master As String() = {"", "Rectangle", "Circle", "T-Section", "I-Section"}
    Public point_load_type_desc_master As String() = {"", "One-Point Load", "Two-Point Load", "Distributed Load"}

    '/**********************************************************************************************************/
    '/***************************************************************************   VARIABEL                   */
    '/**********************************************************************************************************/
    'Penyimpan variabel number of layer reinforcement, default = 0
    Public nr_total As Integer = 0
    Public is_hybrid As Boolean = False

    'Penyimpan variabel bagian cross section
    Public crossSectionType As Integer
    Public pointLoadType As Integer

    'Penyimpan variabel bagian transverse reinforcement
    Public inTransverse_fys As String
    Public inTransverse_ess As String
    Public inTransverse_dia As String
    Public inTransverse_spacing As String
    Public inTransverse_type As Integer = -1

    'Penyimpan variabel bagian steel confinement
    Public inSteelConf_wcx As String
    Public inSteelConf_hcy As String
    Public inSteelConf_bc As String
    Public inSteelConf_tc As String
    Public inSteelConf_slx As String
    Public inSteelConf_sly As String
    Public inSteelConf_nlx As String
    Public inSteelConf_nly As String

    'Penyimpan variabel bagian Strengthening Flexural
    Public inStrFlex_fup As String
    Public inStrFlex_ep As String
    Public inStrFlex_us As String
    Public inStrFlex_ls As String
    Public inStrFlex_bp As String
    Public inStrFlex_tp As String
    Public inStrFlex_di As String

    'Penyimpan variabel bagian Strengthening Shear
    Public inStrShear_fuf As String
    Public inStrShear_ef As String
    Public inStrShear_us As String
    Public inStrShear_ls As String
    Public inStrShear_tf As String
    Public inStrShear_ang As String
    Public inStrShear_nf As String
    Public inStrShear_dorc As Integer = -1 'discrete (1) or continous (0)
    Public inStrShear_bf As String
    Public inStrShear_sf As String
    Public inStrShear_frpt As String 'type of frp: CFRP(1)/GFRP(2)/AFRP(3)

    'Penyimpan variabel bagian Strengthening Confinement
    Public inStrCfnmt_fuf As String
    Public inStrCfnmt_ef As String
    Public inStrCfnmt_us As String
    Public inStrCfnmt_ls As String
    Public inStrCfnmt_tf As String
    Public inStrCfnmt_rad As String
    Public inStrCfnmt_nf As String
    Public inStrCfnmt_dorc As Integer = -1 'discrete (1) or continous (0)
    Public inStrCfnmt_bf As String
    Public inStrCfnmt_sf As String
    Public inStrCfnmt_frpt As String 'type of frp: CFRP(1)/GFRP(2)/AFRP(3)

    '/**********************************************************************************************************/
    '/***************************************************************************   BAGIAN LAYER REINFORCEMENT */
    '/**********************************************************************************************************/

    'Stress-strain Model, default = -1 means no model is selected
    Public steel_stressStrainModel As Double = -1
    Public frp_stressStrainModel As Double = -1
    Public prestressed_stressStrainModel As Double = -1

    'Layer data for each reinforcement type: steel, frp, & prestressed
    Private _steel_layerData() As LayerDataType_Steel
    Private _frp_layerData() As LayerDataType_FRP
    Private _prestressed_layerData() As LayerDataType_Prestressed

    Public Property steel_layerData() As LayerDataType_Steel()
        Get
            Return _steel_layerData
        End Get
        Set(ByVal value As LayerDataType_Steel())
            _steel_layerData = value
        End Set
    End Property

    Public Property frp_layerData() As LayerDataType_FRP()
        Get
            Return _frp_layerData
        End Get
        Set(ByVal value As LayerDataType_FRP())
            _frp_layerData = value
        End Set
    End Property

    Public Property prestressed_layerData() As LayerDataType_Prestressed()
        Get
            Return _prestressed_layerData
        End Get
        Set(ByVal value As LayerDataType_Prestressed())
            _prestressed_layerData = value
        End Set
    End Property

    'Repeated Load Data
    Private _repLoadData As RepeatedLoadData()

    Public Property repLoadData() As RepeatedLoadData()
        Get
            Return _repLoadData
        End Get
        Set(ByVal value As RepeatedLoadData())
            _repLoadData = value
        End Set
    End Property


    ''' <summary>
    ''' Menghitung jumlah layer pada seluruh layer reinforcement[steel, frp, prestressed].
    ''' Dihitung dengan cara melihat nilai "di" masing-masing layer. 
    ''' Jika ada yg sama, berarti reinforcement hibrid. NR adalah jumlah "di" yang unik  pada seluruh layer.
    ''' </summary>
    Public Sub countNR_Layer()

        Dim total_item As Integer = steel_layerData.Length + frp_layerData.Length + prestressed_layerData.Length
        Dim array_di(total_item - 1) As Integer

        'memasukkan di kepada array
        Dim index = 0
        For Each steel In steel_layerData
            array_di(index) = steel.di
            index += 1
        Next
        For Each frp In frp_layerData
            array_di(index) = frp.di
            index += 1
        Next
        For Each pre In prestressed_layerData
            array_di(index) = pre.di
            index += 1
        Next

        'mengambil array unik
        Dim array_di_unik = array_di.Distinct.ToArray

        'cek apakah jumlah array awal dan akhir sama, jika tidak sama berarti hibrid
        If (array_di_unik.Length <> array_di.Length) Then
            Me.is_hybrid = True
        Else
            Me.is_hybrid = False
        End If
        'set jumlah unik ke variable
        Me.nr_total = array_di_unik.Length

    End Sub

    ''' <summary>
    ''' Menghapus seluruh data Strengthening Flexural, Shear, dan Confinement yang sedang disimpan
    ''' </summary>
    Public Sub resetStrengtheningData()
        'Flexural
        inStrFlex_fup = ""
        inStrFlex_ep = ""
        inStrFlex_us = ""
        inStrFlex_ls = ""
        inStrFlex_bp = ""
        inStrFlex_tp = ""
        inStrFlex_di = ""

        ' Shear
        inStrShear_fuf = ""
        inStrShear_ef = ""
        inStrShear_us = ""
        inStrShear_ls = ""
        inStrShear_tf = ""
        inStrShear_ang = ""
        inStrShear_nf = ""
        inStrShear_dorc = -1
        inStrShear_bf = ""
        inStrShear_sf = ""
        inStrShear_frpt = ""

        'Confinement
        inStrCfnmt_fuf = ""
        inStrCfnmt_ef = ""
        inStrCfnmt_us = ""
        inStrCfnmt_ls = ""
        inStrCfnmt_tf = ""
        inStrCfnmt_rad = ""
        inStrCfnmt_nf = ""
        inStrCfnmt_dorc = -1
        inStrCfnmt_bf = ""
        inStrCfnmt_sf = ""
        inStrCfnmt_frpt = ""
    End Sub

    ''' <summary>
    ''' Menghapus seluruh data transverse reinforcement dan steel confinement yg sedang disimpan
    ''' </summary>
    Public Sub resetTransReinfData()
        ''transverse reinforcement
        inTransverse_fys = ""
        inTransverse_ess = ""
        inTransverse_dia = ""
        inTransverse_spacing = ""
        inTransverse_type = -1
        ''steel confinemenet
        inSteelConf_wcx = ""
        inSteelConf_hcy = ""
        inSteelConf_bc = ""
        inSteelConf_tc = ""
        inSteelConf_slx = ""
        inSteelConf_sly = ""
        inSteelConf_nlx = ""
        inSteelConf_nly = ""
    End Sub

    ''' <summary>
    ''' Menghapus seluruh layer data longitudal reinforcement
    ''' </summary>
    Public Sub resetLongitudalReinfData()
        nr_total = 0
        is_hybrid = False
        ReDim steel_layerData(-1)
        ReDim frp_layerData(-1)
        ReDim prestressed_layerData(-1)
    End Sub

    Public Sub resetLoadAnalysis()
        ReDim repLoadData(-1)
    End Sub

End Class
