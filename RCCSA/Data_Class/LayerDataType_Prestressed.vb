﻿''' <summary>
''' 
''' </summary>
''' <author>Albadr Nasution</author>
Public Class LayerDataType_Prestressed
    Private _dia As Double
    Private _N As Double
    Private _ap As Double
    Private _fyp As Double
    Private _esp As Double
    Private _rup As Double
    Private _di As Double
    Private _prestrain As Double
    Private _matModel As String

    'Constructor dengan dia dan n
    Public Sub New(ByVal Dia, ByVal N, ByVal ap, ByVal ffu, ByVal esp, ByVal rup, ByVal di, ByVal prestrain, ByVal matModel)
        _dia = Dia
        _N = N
        _ap = ap
        _fyp = ffu
        _esp = esp
        _rup = rup
        _di = di
        _prestrain = prestrain
        _matModel = If(matModel = "", "0", matModel)
    End Sub

    'Constructor tanpa dia dan n
    Public Sub New(ByVal ap, ByVal ffu, ByVal esp, ByVal rup, ByVal di, ByVal prestrain, ByVal matModel)
        _dia = 0
        _N = 0
        _ap = ap
        _fyp = ffu
        _esp = esp
        _rup = rup
        _di = di
        _prestrain = prestrain
        _matModel = If(matModel = "", "0", matModel)
    End Sub

    Public Property dia() As Double
        Get
            Return _dia
        End Get
        Set(ByVal Value As Double)
            _dia = Value
        End Set
    End Property
    Public Property N() As Double
        Get
            Return _N
        End Get
        Set(ByVal value As Double)
            _N = value
        End Set
    End Property

    Public Property fyp() As Double
        Get
            Return _fyp
        End Get
        Set(ByVal Value As Double)
            _fyp = Value
        End Set
    End Property

    Public Property esp() As Double
        Get
            Return _esp
        End Get
        Set(ByVal value As Double)
            _esp = value
        End Set
    End Property

    Public Property rup() As Double
        Get
            Return _rup
        End Get
        Set(ByVal value As Double)
            _rup = value
        End Set
    End Property

    Public Property ap() As Double
        Get
            Return _ap
        End Get
        Set(ByVal value As Double)
            _ap = value
        End Set
    End Property
    Public Property di() As Double
        Get
            Return _di
        End Get
        Set(ByVal value As Double)
            _di = value
        End Set
    End Property

    Public Property prestrain() As Double
        Get
            Return _prestrain
        End Get
        Set(ByVal Value As Double)
            _prestrain = Value
        End Set
    End Property

    ''' <summary>
    ''' Kenapa string? Karena cbOption dibuat hanya menerima string.
    ''' Ini akan digunakan sebagai valueMember untuk option pada datagridview.
    ''' </summary>
    ''' <value>
    ''' 1. Bi-Linear Model
    ''' 2. Prestressed Model
    ''' 3. Cylic Prestressed
    ''' </value>
    Public Property matModel() As String
        Get
            Return _matModel
        End Get
        Set(ByVal value As String)
            _matModel = value
        End Set
    End Property
End Class
