﻿''' <summary>
''' 
''' </summary>
''' <author>Albadr Nasution</author>
Public Class LayerDataType_Steel
    Private _dia As Double
    Private _N As Double
    Private _fy As Double
    Private _es As Double
    Private _rup As Double
    Private _as As Double
    Private _di As Double
    Private _matModel As String

    Public Sub New(ByVal Dia, ByVal N, ByVal asv, ByVal fy, ByVal es, ByVal rup, ByVal di, ByVal matModel)
        _dia = Dia
        _N = N
        _fy = fy
        _es = es
        _rup = rup
        _as = asv
        _di = di
        _matModel = If(matModel = "", "0", matModel)
    End Sub

    ' Constructor tanpa mempedulikan dia dan n
    Public Sub New(ByVal asv, ByVal fy, ByVal es, ByVal rup, ByVal di, ByVal matModel)
        _dia = 0
        _N = 0
        _as = asv
        _fy = fy
        _es = es
        _rup = rup
        _di = di
        _matModel = If(matModel = "", "0", matModel)
    End Sub

    Public Property dia() As Double
        Get
            Return _dia
        End Get
        Set(ByVal Value As Double)
            _dia = Value
        End Set
    End Property
    Public Property N() As Double
        Get
            Return _N
        End Get
        Set(ByVal value As Double)
            _N = value
        End Set
    End Property

    Public Property fy() As Double
        Get
            Return _fy
        End Get
        Set(ByVal Value As Double)
            _fy = Value
        End Set
    End Property

    Public Property es() As Double
        Get
            Return _es
        End Get
        Set(ByVal value As Double)
            _es = value
        End Set
    End Property

    Public Property rup() As Double
        Get
            Return _rup
        End Get
        Set(ByVal value As Double)
            _rup = value
        End Set
    End Property

    Public Property asv() As Double
        Get
            Return _as
        End Get
        Set(ByVal value As Double)
            _as = value
        End Set
    End Property

    Public Property di() As Double
        Get
            Return _di
        End Get
        Set(ByVal value As Double)
            _di = value
        End Set
    End Property

    ''' <summary>
    ''' Kenapa string? Karena cbOption dibuat hanya menerima string.
    ''' Ini akan digunakan sebagai valueMember untuk option pada datagridview.
    ''' </summary>
    ''' <value>
    ''' 1 = Bi-Linear Model
    ''' 2 = Tri-Linear Model
    ''' 3 = Strain Hardening Model
    ''' 4 = Cylic Steel1
    ''' 5 = Cylic Steel2
    ''' </value>
    Public Property matModel() As String
        Get
            Return _matModel
        End Get
        Set(ByVal value As String)
            _matModel = value
        End Set
    End Property
End Class
