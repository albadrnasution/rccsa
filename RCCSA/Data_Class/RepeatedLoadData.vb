﻿''' <summary>
''' Menyimpan satu baris data repeated load.
''' </summary>
''' <remarks></remarks>
Public Class RepeatedLoadData

    Public Sub New(ByVal no, ByVal numRepetition, ByVal turnPointPlus, ByVal turnPointMin)
        Me._no = no
        Me._numRepetition = numRepetition
        Me._turnPointPlus = turnPointPlus
        Me._turnPointMin = turnPointMin
    End Sub

    Private _no As Integer
    Public Property no() As Integer
        Get
            Return _no
        End Get
        Set(ByVal value As Integer)
            _no = value
        End Set
    End Property

    Private _numRepetition As Integer
    Public Property numRepetition() As Integer
        Get
            Return _numRepetition
        End Get
        Set(ByVal value As Integer)
            _numRepetition = value
        End Set
    End Property

    Private _turnPointPlus As Double
    Public Property turnPointPlus() As Double
        Get
            Return _turnPointPlus
        End Get
        Set(ByVal value As Double)
            _turnPointPlus = value
        End Set
    End Property

    Private _turnPointMin As Double
    Public Property turnPointMin() As Double
        Get
            Return _turnPointMin
        End Get
        Set(ByVal value As Double)
            _turnPointMin = value
        End Set
    End Property


End Class
