﻿''' <summary>
''' 
''' </summary>
''' <author>Albadr Nasution</author>
Public Class LayerDataType_FRP
    Private _dia As Double
    Private _N As Double
    Private _ffu As Double
    Private _ef As Double
    Private _rup As Double
    Private _af As Double
    Private _di As Double
    Private _matModel As String

    'Constructor dengan nilai dia dan n
    Public Sub New(ByVal dia, ByVal N, ByVal af, ByVal ffu, ByVal ef, ByVal rup, ByVal di, ByVal matModel)
        _dia = dia
        _N = N
        _af = af
        _ffu = ffu
        _ef = ef
        _rup = rup
        _di = di
        _matModel = If(matModel = "", "0", matModel)
    End Sub

    'Constructor dengan mengabaikan dia dan n
    Public Sub New(ByVal af, ByVal ffu, ByVal ef, ByVal rup, ByVal di, ByVal matModel)
        _dia = 0
        _N = 0
        _af = af
        _ffu = ffu
        _ef = ef
        _rup = rup
        _di = di
        _matModel = If(matModel = "", "0", matModel)
    End Sub

    Public Property dia() As Double
        Get
            Return _dia
        End Get
        Set(ByVal Value As Double)
            _dia = Value
        End Set
    End Property
    Public Property N() As Double
        Get
            Return _N
        End Get
        Set(ByVal value As Double)
            _N = value
        End Set
    End Property
    Public Property ffu() As Double
        Get
            Return _ffu
        End Get
        Set(ByVal Value As Double)
            _ffu = Value
        End Set
    End Property
    Public Property ef() As Double
        Get
            Return _ef
        End Get
        Set(ByVal value As Double)
            _ef = value
        End Set
    End Property
    Public Property rup() As Double
        Get
            Return _rup
        End Get
        Set(ByVal value As Double)
            _rup = value
        End Set
    End Property
    Public Property af() As Double
        Get
            Return _af
        End Get
        Set(ByVal value As Double)
            _af = value
        End Set
    End Property
    Public Property di() As Double
        Get
            Return _di
        End Get
        Set(ByVal value As Double)
            _di = value
        End Set
    End Property

    ''' <summary>
    ''' Kenapa string? Karena cbOption dibuat hanya menerima string.
    ''' Ini akan digunakan sebagai valueMember untuk option pada datagridview.
    ''' </summary>
    ''' <value>
    ''' 1 = Linear Model
    ''' 2 = Hybrid Model
    ''' 3 = Ductile Model
    ''' 4 = Cylic FRP
    ''' </value>
    Public Property matModel() As String
        Get
            Return _matModel
        End Get
        Set(ByVal value As String)
            _matModel = value
        End Set
    End Property
End Class
