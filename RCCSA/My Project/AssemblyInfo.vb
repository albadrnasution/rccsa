﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("RCCSA")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Andalas University, Padang, INDONESIA")> 
<Assembly: AssemblyProduct("Reinforced Concrete Cross Section Analysis")> 
<Assembly: AssemblyCopyright("Copyright © 2013-2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("41970ac1-a22d-4167-91b7-ac9183c870a2")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("4.2.0.1")> 
<Assembly: AssemblyFileVersion("4.2.0.1")> 
