﻿Imports System.ComponentModel
Imports System.Reflection

Public Class ChartClc

    Public Shared Property mom_curv As ChartItem = New ChartItem("VISMOM.txt", "mom_curv", "Moment - Curvature", "Moment - Curvature", "Curvature (rad/km)", "Moment (kNm)", Color.Blue)
    Public Shared Property interact_diagr As ChartItem = New ChartItem("VISDIN.txt", "interact_diagr", "Interaction Diagram", "Interaction Diagram", "Moment (kNm)", "Axial Load (kN)", Color.Tomato)

    Public Shared Property p_defl As ChartItem = New ChartItem("n/a", "p_defl", "Load - Deflection", "Load - Deflection", "Deflection (mm)", "Load (kN)", Color.Orange)
    Public Shared Property p_defl_def As ChartItem = New ChartItem("VISDEF.txt", "p_defl_def", p_defl.title, p_defl.shortTitle, p_defl.xLabel, "Load (kN)", p_defl.color)
    Public Shared Property p_defl_dew As ChartItem = New ChartItem("VISDEW.txt", "p_defl_dew", p_defl.title, p_defl.shortTitle, p_defl.xLabel, "Distributed Load (kN/m)", p_defl.color)

    Public Shared Property strain_dist As ChartItem = New ChartItem("VISTRA.txt", "strain_dist", "Strain Distribution", "Strain Distribution", "Strain (mm/mm)", "Section Depth (mm)", Color.Red)
    Public Shared Property reinf_stress_dist As ChartItem = New ChartItem("VISTRS.txt", "reinf_stress_dist", "Reinforcement Stress Distribution", "Reinf. Stress Dist.", "Stress (MPa)", "Section Depth (mm)", Color.DarkRed)
    Public Shared Property reinf_internal_forc As ChartItem = New ChartItem("VISFRS.TXT", "reinf_internal_forc", "Reinforcement Internal Forces", "Reinf. Internal Forces", "Internal Forces (kN)", "Section Depth (mm)", Color.Red)
    Public Shared Property con_stress_dist As ChartItem = New ChartItem("VISTRC.TXT", "con_stress_dist", "Concrete Stress Distribution", "Con. Stress Dist.", "Stress (MPa)", "Section Depth (mm)", Color.DarkGreen)
    Public Shared Property con_internal_forc As ChartItem = New ChartItem("VISFRC.TXT", "con_internal_forc", "Concrete Internal Forces", "Con. Internal Forces", "Internal Forces (kN)", "Section Depth (mm)", Color.Green)

    Public Shared Property net_axis_depth As ChartItem = New ChartItem("VISNAX.txt", "net_axis_depth", "Neutral Axis Depth", "Neutral Axis Depth", "Load (kN)", "Neutral Axis Depth (mm)", Color.Violet)
    Public Shared Property net_axis_depth_dsload As ChartItem = New ChartItem("VISNAX.txt", "net_axis_depth", "Neutral Axis Depth", "Neutral Axis Depth", "Distributed Load (kN/m)", "Neutral Axis Depth (mm)", Color.Violet)
    Public Shared Property net_axis_depth_moment As ChartItem = New ChartItem("VISNAX.txt", "net_axis_depth_moment", "Neutral Axis Depth", "Neutral Axis Depth", "Moment (kNm)", "Neutral Axis Depth (mm)", Color.Violet)
    Public Shared Property crack_depth As ChartItem = New ChartItem("VISCRD.txt", "crack_depth", "Crack Depth", "Crack Depth", "Load (kN)", "Crack Depth (mm)", Color.BlueViolet)
    Public Shared Property crack_depth_dsload As ChartItem = New ChartItem("VISCRD.txt", "crack_depth", "Crack Depth", "Crack Depth", "Distributed Load (kN/m)", "Crack Depth (mm)", Color.BlueViolet)
    Public Shared Property crack_depth_moment As ChartItem = New ChartItem("VISCRD.txt", "crack_depth_moment", "Crack Depth", "Crack Depth", "Moment (kNm)", "Crack Depth (mm)", Color.BlueViolet)
    Public Shared Property crack_width As ChartItem = New ChartItem("VISCRW.txt", "crack_width", "Crack Width", "Crack Width", "Crack Width (mm)", "Load (kN)", Color.DarkViolet)
    Public Shared Property crack_width_dsload As ChartItem = New ChartItem("VISCRW.txt", "crack_width", "Crack Width", "Crack Width", "Crack Width (mm)", "Distributed Load (kN/m)", Color.DarkViolet)
    Public Shared Property crack_width_moment As ChartItem = New ChartItem("VISCRW.txt", "crack_width_moment", "Crack Width", "Crack Width", "Crack Width (mm)", "Moment (kNm)", Color.DarkViolet)

    Public Shared Property viscon As ChartItem = New ChartItem("VISCON.txt", "viscon", "Concrete Material Model", "Concrete Material Model", "Strain (mm/mm)", "Stress (MPa)", Color.Blue)

    Public Shared Property vistl1 As ChartItem = New ChartItem("VISTL1.txt", "vistl1", "Bi-Linear Model", "Bi-Linear Model", "Strain (mm/mm)", "Stress (MPa)", Color.Blue)
    Public Shared Property vistl2 As ChartItem = New ChartItem("VISTL2.txt", "vistl2", "Tri-Linear Model", "Tri-Linear Model", "Strain (mm/mm)", "Stress (MPa)", Color.Aqua)
    Public Shared Property vistl3 As ChartItem = New ChartItem("VISTL3.txt", "vistl3", "Strain Hardening Model", "Strain Hardening Model", "Strain (mm/mm)", "Stress (MPa)", Color.DarkBlue)
    Public Shared Property vistl4 As ChartItem = New ChartItem("VISTL4.txt", "vistl4", "Cylic Steel 1", "Cylic Steel 1", "Strain (mm/mm)", "Stress (MPa)", Color.BlueViolet)
    Public Shared Property vistl5 As ChartItem = New ChartItem("VISTL5.txt", "vistl5", "Cylic Steel 2", "Cylic Steel 2", "Strain (mm/mm)", "Stress (MPa)", Color.DarkSlateBlue)

    Public Shared Property vifrp1 As ChartItem = New ChartItem("VIFRP1.txt", "vifrp1", "Linear Model", "Linear Model", "Strain (mm/mm)", "Stress (MPa)", Color.Red)
    Public Shared Property vifrp2 As ChartItem = New ChartItem("VIFRP2.txt", "vifrp2", "Hybrid Model", "Hybrid Model", "Strain (mm/mm)", "Stress (MPa)", Color.Orange)
    Public Shared Property vifrp3 As ChartItem = New ChartItem("VIFRP3.txt", "vifrp3", "Ductile Model", "Ductile Model", "Strain (mm/mm)", "Stress (MPa)", Color.DarkRed)
    Public Shared Property vifrp4 As ChartItem = New ChartItem("VIFRP4.txt", "vifrp4", "Cylic FRP", "Cylic FRP", "Strain (mm/mm)", "Stress (MPa)", Color.DarkMagenta)

    Public Shared Property vipre1 As ChartItem = New ChartItem("VIPRE1.txt", "vipre1", "Bi-Linear Model", "Bi-Linear Model", "Strain (mm/mm)", "Stress (MPa)", Color.Green)
    Public Shared Property vipre2 As ChartItem = New ChartItem("VIPRE2.txt", "vipre2", "Prestressed Model", "Prestressed Model", "Strain (mm/mm)", "Stress (MPa)", Color.GreenYellow)
    Public Shared Property vipre3 As ChartItem = New ChartItem("VIPRE3.txt", "vipre3", "Cylic Prestressed", "Cylic Prestressed", "Strain (mm/mm)", "Stress (MPa)", Color.YellowGreen)

    Public Shared Property ss_top_reinf As ChartItem = New ChartItem("HASSTL1.txt", "ss_top_reinf", "Stress-Strain Top Reinforcement", "Stress-Strain Top Reinf.", "Strain (mm/mm)", "Stress (MPa)", Color.Red)
    Public Shared Property ss_btm_reinf As ChartItem = New ChartItem("HASSTL2.txt", "ss_btm_reinf", "Stress-Strain Bottom Reinforcement", "Stress-Strain Bottom Reinf.", "Strain (mm/mm)", "Stress (MPa)", Color.DarkRed)
    Public Shared Property ms_top_reinf As ChartItem = New ChartItem("HASTRM1.txt", "ms_top_reinf", "Moment-Strain Top Reinforcement", "Moment-Strain Top Reinf.", "Strain (mm/mm)", "Moment (kNm)", Color.BlueViolet)
    Public Shared Property ms_btm_reinf As ChartItem = New ChartItem("HASTRM2.txt", "ms_btm_reinf", "Moment-Strain Bottom Reinforcement", "Moment-Strain Bottom Reinf.", "Strain (mm/mm)", "Moment (kNm)", Color.DarkBlue)
    Public Shared Property ss_con_top As ChartItem = New ChartItem("HASCYC1.txt", "ss_con_top", "Stress-Strain Con. (Top)", "Stress-Strain Con. (Top)", "Strain (mm/mm)", "Stress (MPa)", Color.Green)
    Public Shared Property ss_con_btm As ChartItem = New ChartItem("HASCYC2.txt", "ss_con_btm", "Stress-Strain Con. (Bottom)", "Stress-Strain Con. (Bottom)", "Strain (mm/mm)", "Stress (MPa)", Color.DarkGreen)
    Public Shared Property curv_ax_strain As ChartItem = New ChartItem("HASTCUR.txt", "curv_ax_strain", "Curvature-Axial Strain", "Curvature-Axial Strain", "Axial Strain (mm/mm)", "Curvature (rad/km)", Color.Orange)

    Public Shared Property mom_eccent As ChartItem = New ChartItem("VEXEN1.txt", "mom_eccent", "Moment-Eccentricity", "Moment-Eccentricity", "Eccentricity (mm)", "Moment (kNm)", Color.DarkSeaGreen)
    Public Shared Property axl_eccent As ChartItem = New ChartItem("VEXEN2.txt", "axl_eccent", "Axial Load-Eccentricity", "Axial Load-Eccentricity", "Eccentricity (mm)", "Axial Load (kN)", Color.SeaGreen)

    Public beamCbbOptions As New BindingList(Of CbOption)()
    Public sectionCbbOptions As New BindingList(Of CbOption)()
    Public columnCbbOptions2 As New BindingList(Of CbOption)()
    Public columnCbbOptions7 As New BindingList(Of CbOption)()
    Public reploadCbbOptions As New BindingList(Of CbOption)()

    Public emptyCharts As New BindingList(Of CbOption)()
    Public beamCharts As New BindingList(Of ChartItem)()
    Public sectionCharts As New BindingList(Of ChartItem)()
    Public columnCharts2 As New BindingList(Of ChartItem)()
    Public columnCharts7 As New BindingList(Of ChartItem)()
    Public reploadCharts As New BindingList(Of ChartItem)()

    Public Sub New()
        ''Option for tab chart beam
        beamCharts.Add(mom_curv)
        beamCharts.Add(p_defl)
        beamCharts.Add(strain_dist)
        beamCharts.Add(reinf_stress_dist)
        beamCharts.Add(reinf_internal_forc)
        beamCharts.Add(con_stress_dist)
        beamCharts.Add(con_internal_forc)
        beamCharts.Add(net_axis_depth)
        beamCharts.Add(crack_depth)
        beamCharts.Add(crack_width)
        For Each ci As ChartItem In beamCharts
            beamCbbOptions.Add(New CbOption(ci.code, ci.title))
        Next

        ''Option for tab chart section
        sectionCharts.Add(mom_curv)
        sectionCharts.Add(strain_dist)
        sectionCharts.Add(reinf_stress_dist)
        sectionCharts.Add(reinf_internal_forc)
        sectionCharts.Add(con_stress_dist)
        sectionCharts.Add(con_internal_forc)
        sectionCharts.Add(net_axis_depth_moment)
        sectionCharts.Add(crack_depth_moment)
        sectionCharts.Add(crack_width_moment)
        For Each ci As ChartItem In sectionCharts
            sectionCbbOptions.Add(New CbOption(ci.code, ci.title))
        Next

        ''Option for tab chart column (jika Ni diisi dan P kosong)
        columnCharts2.Add(mom_curv)
        columnCharts2.Add(interact_diagr)
        columnCharts2.Add(mom_eccent)
        columnCharts2.Add(axl_eccent)
        For Each ci As ChartItem In columnCharts2
            columnCbbOptions2.Add(New CbOption(ci.code, ci.title))
        Next

        ''Option for tab chart column (jika P diisi dan Ni kosong)
        columnCharts7.Add(mom_curv)
        columnCharts7.Add(interact_diagr)
        columnCharts7.Add(strain_dist)
        columnCharts7.Add(reinf_stress_dist)
        columnCharts7.Add(reinf_internal_forc)
        columnCharts7.Add(con_stress_dist)
        columnCharts7.Add(con_internal_forc)
        columnCharts7.Add(net_axis_depth_moment)
        columnCharts7.Add(crack_depth_moment)
        columnCharts7.Add(crack_width_moment)
        For Each ci As ChartItem In columnCharts7
            columnCbbOptions7.Add(New CbOption(ci.code, ci.title))
        Next

        ''Option for tab chart repload (jika P diisi dan Ni kosong)
        reploadCharts.Add(mom_curv)
        reploadCharts.Add(ss_top_reinf)
        reploadCharts.Add(ss_btm_reinf)
        reploadCharts.Add(ms_top_reinf)
        reploadCharts.Add(ms_btm_reinf)
        reploadCharts.Add(ss_con_top)
        reploadCharts.Add(ss_con_btm)
        reploadCharts.Add(curv_ax_strain)
        For Each ci As ChartItem In reploadCharts
            reploadCbbOptions.Add(New CbOption(ci.code, ci.title))
        Next


    End Sub

    ''' <summary>
    ''' Mengambil chartitem berdasarkan kodenya (yg sama dengan nama variabel propertinya)
    ''' </summary>
    ''' <param name="code">kode chartitem</param>
    Public Shared Function getChart(code As String) As ChartItem
        Try
            ' Get Type Object corresponding to MyClass. 
            Dim myType As Type = GetType(ChartClc)
            ' Get PropertyInfo object by passing property name. 
            Dim myPropInfo As PropertyInfo = myType.GetProperty(code)
            Dim theChartItem As ChartItem = myPropInfo.GetValue(New ChartClc)
            Return theChartItem
        Catch e As NullReferenceException
            Console.WriteLine("The property does not exist in MyClass.", e.Message.ToString())
            Return Nothing
        End Try
    End Function

End Class
