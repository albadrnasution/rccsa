﻿Public Class ChartItem
    Public fileData As String
    Public title As String
    Public shortTitle As String
    Public code As String
    Public xLabel As String
    Public yLabel As String
    Public color As Color

    Public Sub New(fileData As String, code As String, title As String, shortTitle As String, xLabel As String, yLabel As String)
        Me.fileData = fileData
        Me.title = title
        Me.shortTitle = shortTitle
        Me.code = code
        Me.xLabel = xLabel
        Me.yLabel = yLabel
    End Sub

    Public Sub New(fileData As String, code As String, title As String, shortTitle As String, xLabel As String, yLabel As String, color As Color)
        Me.fileData = fileData
        Me.title = title
        Me.shortTitle = shortTitle
        Me.code = code
        Me.xLabel = xLabel
        Me.yLabel = yLabel
        Me.color = color
    End Sub

    ' This is neccessary because the ListBox and ComboBox rely 
    ' on this method when determining the text to display. 
    Public Overrides Function ToString() As String
        Return Me.title
    End Function
End Class
