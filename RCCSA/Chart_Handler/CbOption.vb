﻿''' <summary>
''' Kelas ini digunakan untuk objek pilihan pada combobox. Setiap pilihan memiliki text yang ditampilkan dan
''' teks yang menjadi nilai intinstrik dari pilihan tersebut. Contoh: pilihan ditampilkan adalah Jawa Barat,
''' tapi agar singkat untuk pengecekan dll, nilai instrinstik yg diambil adalah JB.
''' </summary>
''' <remarks></remarks>
Public Class CbOption
    Public Sub New(value As String, display As String)
        Me._display = display
        Me._value = value
    End Sub
    Public Property display() As String
        Get
            Return Me._display
        End Get
        Set(value As String)
            Me._display = value
        End Set
    End Property
    Public Property value() As String
        Get
            Return Me._value
        End Get
        Set(value As String)
            Me._value = value
        End Set
    End Property

    ' This is neccessary because the ListBox and ComboBox rely 
    ' on this method when determining the text to display. 
    Public Overrides Function ToString() As String
        Return Me._display
    End Function

    Private _display As String
    Private _value As String
End Class
