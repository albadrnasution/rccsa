﻿Imports System.Windows.Forms.DataVisualization.Charting
Imports System.IO

''' <summary>
''' Melakukan handel seluruh hal berkaitan menambah grafik/chart atau seri ke dalam objek chart.
''' </summary>
''' <author>Albadr Nasution</author>
Public Class ChartHandler

    ''' <summary>
    ''' Membuat seri baru pada grafik utama pada program. Seri akan dibuat baru atau ditambahkan ke grafik yang ada
    ''' sesuai parameter mode. 
    ''' </summary>
    ''' <param name="VisFileName">Nama file yang berisi kumpulan titik x y</param>
    ''' <param name="VisChartSeriesName">Nama seri pada grafik, jika mode baru, akan dipakai pula untuk judul grafik</param>
    ''' <param name="AxisX_Title">optional diisi, jika diisi label pada sumbu x akan diganti</param>
    ''' <param name="AxisY_Title">optional diisi, jika diisi label pada sumbu y akan diganti</param>
    ''' <param name="mode">"new" jika buat chart baru (fungsi reset akan dipanggil), "add" untuk menambahkan ke chart yang ada</param>
    ''' <param name="color" >optional diisi, jika diisi warna akan disesuaikan, jika tidak warna seri akan otomatis</param>
    ''' <remarks></remarks>
    Public Shared Function addChartSeries(ByRef ChartObject As Chart, ByRef GridView As DataGridView, ByVal VisFileName As String, ByVal VisChartSeriesName As String, Optional ByVal AxisX_Title As String = Nothing, Optional ByVal AxisY_Title As String = Nothing, Optional ByVal color As Color = Nothing, Optional ByVal mode As String = "new")
        'SERI CHART yang baru
        Dim series As New Series
        series.Name = VisChartSeriesName
        'Change to a line graph.
        series.ChartType = SeriesChartType.Line
        'Ketebalan seri
        series.BorderWidth = 3

        '=============================Chart option
        'Jika parameter diisi, ganti label X dan Y
        If (AxisX_Title IsNot Nothing) Then
            ChartObject.ChartAreas(0).AxisX.Title = AxisX_Title
        End If
        If (AxisX_Title IsNot Nothing) Then
            ChartObject.ChartAreas(0).AxisY.Title = AxisY_Title
        End If
        'Lihat parameter mode, default berisi new
        If (mode = "new") Then
            ChartHandler.resetChart(ChartObject, GridView)
            'Ganti judul jika mode adalah buat chart baru
            ChartObject.Titles(0).Text = VisChartSeriesName
            ChartObject.Legends(0).Enabled = False
        Else
            'Munculkan series jika mode penambahan
            ChartObject.Legends(0).Enabled = True
        End If
        'Jika parameter warna diisi, set warna seri
        If (color <> Nothing) Then
            series.Color = color
        End If

        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(VisFileName)
            Dim line As String

            ' first line
            line = sr.ReadLine()
            ' jika line ada (minimal file punya 1 line) lanjutkan dan loop hingga ke line akhir
            While line IsNot Nothing
                'Hilangkan spasi di depan dan belakang line
                Dim lt = line.Trim
                'Hilangkan spasi ganda pada satu line, sehingga hasil akhir adalah spasi tunggal setiap kemunculannya
                While lt.Contains("  ")             '2 spaces.
                    lt = lt.Replace("  ", " ")      'Replace with 1 space.
                End While

                'Potong line berdasarkan spasi, asumsi: nilai x y dipisahkan oleh spasi
                Dim sp As String() = lt.Split(" ")
                'Ambil nilai keduanya dengan cara memparsing string menjadi double
                Dim value1, value2 As Double
                If (sp.Length = 2) Then
                    If (Double.TryParse(sp.ElementAt(0), value1) And Double.TryParse(sp.ElementAt(1), value2)) Then
                        ' text is convertible to Double, and value contains the Double value now
                        'Debug.WriteLine(value1 & ", " & value2)
                        series.Points.AddXY(value1, value2)

                        If (GridView IsNot Nothing) Then
                            GridView.Rows.Add(value1, value2)
                        End If
                    End If
                End If

                'Line berikutnya untuk diproses
                line = sr.ReadLine()
            End While

            ' Close StreamReader
            sr.Close()

            'Add the series to the Chart1 control.
            ChartObject.Series.Add(series)

            ' ==== Set nilai minimum sumbu X 
            ''berdasarkan minimum nilai yang baru diisikan, bulatkan supaya integer
            Dim min = series.Points.FindMinByValue("X").XValue
            Dim max = series.Points.FindMaxByValue("X").XValue
            Dim span = max - min
            Dim setMin As Double = min
            'Hitung berdasarkan rentang nilai grafik
            If (span >= 1000) Then
                Dim m As Integer = Math.Floor(min / 100)
                setMin = m * 100
            ElseIf (span >= 750 And span < 1000) Then
                Dim m As Integer = Math.Floor(min / 75)
                setMin = m * 75
            ElseIf (span >= 250 And span < 750) Then
                Dim m As Integer = Math.Floor(min / 50)
                setMin = m * 50
            ElseIf (span >= 100 And span < 250) Then
                Dim m As Integer = Math.Floor(min / 25)
                setMin = m * 25
            ElseIf (span >= 40 And span < 100) Then
                Dim m As Integer = Math.Floor(min / 10)
                setMin = m * 10
            ElseIf (span >= 15 And span < 40) Then
                Dim m As Integer = Math.Floor(min / 5)
                setMin = m * 5
            ElseIf (span >= 1 And span < 15) Then
                setMin = Math.Floor(min)
            ElseIf (span >= 0.5 And span < 1) Then
                Dim m As Integer = Math.Floor(min / 0.1)
                setMin = m * 0.1
            ElseIf (span >= 0.01 And span < 0.1) Then
                setMin = Math.Round(min - 0.0049, 2)
            ElseIf (span >= 0.001 And span < 0.01) Then
                setMin = Math.Round(min - 0.00049, 3)
            ElseIf (span < 0.001) Then
                setMin = min
            End If
            Debug.WriteLine("span  =" & span)
            Debug.WriteLine("MinVal=" & min)
            Debug.WriteLine("MinSet=" & setMin)
            'Set minimum dari perhitungan di atas
            ChartObject.ChartAreas(0).AxisX.Minimum = setMin 'If(max - min > 1, Math.Floor(min), min)
            ChartObject.ChartAreas(0).AxisX.Maximum = Double.NaN 'max
            'Pewarnaan grid dalam
            ChartObject.ChartAreas(0).AxisX.MajorGrid.LineColor = Drawing.Color.LightGray
            ChartObject.ChartAreas(0).AxisY.MajorGrid.LineColor = Drawing.Color.LightGray
            'Setting perpotongn sumbu pada titik NOL
            ChartObject.ChartAreas(0).AxisY.Crossing = 0
            ChartObject.ChartAreas(0).AxisX.Crossing = 0

            ' ==== Fix error if all x values is zero
            ''Determine whether every x value is zero
            Dim onlyZeroes As Boolean = True
            For Each dp As DataPoint In series.Points
                If dp.XValue <> 0 Then
                    onlyZeroes = False
                    Exit For
                End If
            Next
            ''Fix error
            If onlyZeroes Then
                'Set x axis min & max to something suitable
                '(If not set, the dummy data point may affect the x axis limits)
                ChartObject.ChartAreas(0).AxisX.Minimum = -1
                ChartObject.ChartAreas(0).AxisX.Maximum = 1

                'Add a transparent point at an x position other than zero
                Dim dummyPoint As New DataPoint(1, series.Points(0).YValues(0))
                dummyPoint.Color = color.Transparent
                series.Points.Add(dummyPoint)
            End If

        Catch ex As System.IO.FileNotFoundException
            ' Let the user know what went wrong.
            MsgBox("The file could not be read: " & VisFileName)
        Catch ex As ArgumentException
            ' Let the user know what went wrong.
            MsgBox(ex.Message)
        Catch ex As Exception
            ' Let the user know what went wrong.
            Console.WriteLine(ex.Message)
        End Try

        'Kembalikan ke fungsi yang memanggil supaya series bisa diubah-ubah lagi, misal ganti warna
        Return series
    End Function

    ''' <summary>
    ''' Menghapus seluruh perubahan pada grafik.
    ''' </summary>
    Public Shared Sub resetChart(ByRef ChartObject As Chart, ByRef GridView As DataGridView)
        'kembalikan grafik
        ChartObject.Series.Clear()
        ChartObject.ResetAutoValues()
        ChartObject.ChartAreas(0).AxisX.IsReversed = False
        ChartObject.ChartAreas(0).AxisY.IsReversed = False
        ChartObject.Titles(0).Text = ""
        'hapus daftar point pada grid view jika ada
        If (GridView IsNot Nothing) Then
            GridView.Rows.Clear()
        End If
    End Sub

    ''' <summary>
    ''' Menggambar chart yang sudah ada (preset). Dafyar chart bisa dilihat pada chartItems pada collection (ChartClc).
    ''' Asosiasi file data, label, dan warna diset disana.
    ''' Sub ini akan menggambar chart baru sehingga chart sebelumnya akan dihapus dahulu.
    ''' 
    ''' Khusus untuk load deflection (p_defl) akan dicek input pada mainform.
    ''' </summary>
    ''' <param name="ChartObject"></param>
    ''' <param name="GridView"></param>
    ''' <param name="chartItem"></param>
    ''' <todo>Yang mat model belum di objekkan dan referensi ke paintchart masih string code</todo>
    Public Shared Sub paintChartItem(ByRef ChartObject As Chart, ByRef GridView As DataGridView, chartItem As ChartItem)
        If (chartItem.code = "p_defl") Then
            If (MainForm.pointLoadOptionDistributed.Checked()) Then
                chartItem = ChartClc.p_defl_dew
            Else
                chartItem = ChartClc.p_defl_def
            End If
        ElseIf (chartItem.code = "net_axis_depth") Then
            If (MainForm.pointLoadOptionDistributed.Checked()) Then
                chartItem = ChartClc.net_axis_depth_dsload
            Else
                chartItem = ChartClc.net_axis_depth
            End If
        ElseIf (chartItem.code = "crack_depth") Then
            If (MainForm.pointLoadOptionDistributed.Checked()) Then
                chartItem = ChartClc.crack_depth_dsload
            Else
                chartItem = ChartClc.crack_depth
            End If
        ElseIf (chartItem.code = "crack_width") Then
            If (MainForm.pointLoadOptionDistributed.Checked()) Then
                chartItem = ChartClc.crack_width_dsload
            Else
                chartItem = ChartClc.crack_width
            End If
        End If

        ChartHandler.addChartSeries(ChartObject, GridView, chartItem.fileData, chartItem.title, chartItem.xLabel, chartItem.yLabel, chartItem.color)
        '' IF item is , flip the chart automatically
        If (chartItem.code = "net_axis_depth" Or chartItem.code = "net_axis_depth_moment") Then
            ChartObject.ChartAreas(0).AxisY.IsReversed = True
        End If
    End Sub

    ''' <summary>
    ''' Menggambar chart yang sudah ada (preset). Dafar chart bisa dilihat pada kode dengan asosiasi chartCode
    ''' dengan chart tertentu. Sub ini akan menggambar chart baru sehingga chart sebelumnya akan dihapus dahulu.
    ''' </summary>
    ''' <param name="chartCode"></param>
    ''' <remarks></remarks>
    Public Shared Sub paintChart(ByRef ChartObject As Chart, ByRef GridView As DataGridView, chartCode As String)
        ChartHandler.paintChartItem(ChartObject, GridView, ChartClc.getChart(chartCode))
    End Sub


End Class
