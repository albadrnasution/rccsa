﻿Imports System.Windows.Forms
Imports System.ComponentModel

Public Class DialogPrestressedReinf

    Private _prestressed_layerData As LayerDataType_Prestressed()
    Private _prestressed_stressStrainModel As Integer
    Private _h As Double

    '================================================================================================================
    'PROPERTY
    '================================================================================================================

    'Properti yang merupakan penyimpan nilai variabel setiap layer dari reinforcement prestressed
    Public Property prestressed_layerData() As LayerDataType_Prestressed()
        Get
            Return _prestressed_layerData
        End Get
        Set(value As LayerDataType_Prestressed())
            _prestressed_layerData = value
        End Set
    End Property

    'Properti yang merupakan penyimpan model stress-strain dari reinforcement prestressed
    Public Property prestressed_stressStrainModel() As Integer
        Get
            Return _prestressed_stressStrainModel
        End Get
        Set(value As Integer)
            _prestressed_stressStrainModel = value
        End Set
    End Property

    'Tinggi/diameter objek yg bisa diisi layer
    Public Property h() As Double
        Get
            Return _h
        End Get
        Set(value As Double)
            _h = value
        End Set
    End Property

    '/**********************************************************************************************************/
    '/***************************************************************************************   METHOD         */
    '/**********************************************************************************************************/

    Private Sub DialogPrestressedReinf_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '================================= PENGATURAN KOLOM ===============================
        ' Mengisi kolom Material Model dengan pilihan
        Dim clmMatModel As DataGridViewComboBoxColumn = CType(dataGrid.Columns("clmMatModel"), DataGridViewComboBoxColumn)
        'Memasukkan daftar chart ke dalam options
        Dim matModelOptions As New BindingList(Of CbOption)()
        matModelOptions.Add(New CbOption("1", "Bi-Linear"))
        matModelOptions.Add(New CbOption("2", "Prestressed"))
        matModelOptions.Add(New CbOption("3", "Cylic Prestressed"))

        clmMatModel.DataSource = matModelOptions
        clmMatModel.DisplayMember = "display"
        clmMatModel.ValueMember = "value"

        'Set coloumns on the table grid to accept only Double
        dataGrid.Columns("clmDia").ValueType = GetType(Double)
        dataGrid.Columns("clmN").ValueType = GetType(Double)
        dataGrid.Columns("clmPrestressedAp").ValueType = GetType(Double)
        dataGrid.Columns("clmPrestressedFyp").ValueType = GetType(Double)
        dataGrid.Columns("clmPrestressedEsp").ValueType = GetType(Double)
        dataGrid.Columns("clmPrestressedRupStrain").ValueType = GetType(Double)
        dataGrid.Columns("clmPrestressedDi").ValueType = GetType(Double)
        dataGrid.Columns("clmPrestressedPrestrain").ValueType = GetType(Double)

        '================================= MENGESET NILAI JIKA ADA

        Try

            '=================================== MENGAMBIL LAYER DATA ==================================
            'Ambil data layer yang sudah diisi oleh user (ketika dialog ini dibuka sebelum sekarang atau dari file simpanan)
            Dim ss As LayerDataType_Prestressed() = Me.prestressed_layerData
            For index As Integer = 0 To ss.Length - 1
                dataGrid.Rows.Add()
                dataGrid.Rows(index).Cells("clmDia").Value() = ss(index).dia
                dataGrid.Rows(index).Cells("clmN").Value() = ss(index).N
                dataGrid.Rows(index).Cells("clmPrestressedAp").Value() = ss(index).ap
                dataGrid.Rows(index).Cells("clmPrestressedFyp").Value() = ss(index).fyp
                dataGrid.Rows(index).Cells("clmPrestressedEsp").Value() = ss(index).esp
                dataGrid.Rows(index).Cells("clmPrestressedRupStrain").Value() = ss(index).rup
                dataGrid.Rows(index).Cells("clmPrestressedDi").Value() = ss(index).di
                dataGrid.Rows(index).Cells("clmPrestressedPrestrain").Value() = ss(index).prestrain
                dataGrid.Rows(index).Cells("clmMatModel").Value() = ss(index).matModel
            Next
        Catch ex As ArgumentException
            'Wah, ada error apa ya?
            MessageBox.Show("Unexpected Error. " & ex.Message)
        Catch ex As NullReferenceException
            'Kalau null, abaikan berarti belum pernah diisi
        End Try
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        '================================= MENYIMPAN LAYER DATA ===============================
        'Ambil jumlah baris, tinggalkan row terakhir yg isinya 0 semua
        Dim rowCount As Integer = dataGrid.RowCount - 1
        'Ambil jumlah kolom (ada 5 harusnya)
        Dim columnCount As Integer = dataGrid.ColumnCount

        'Inisiasi ulang variabel data nilai objek ini, pasang sebanyak rowCount buah
        ReDim _prestressed_layerData(rowCount - 1)
        'Ambil setiap data pada baris data layer dan masukkan ke variabel penyimpanan untuk dikembalikan ke MainForm
        For rowIndex As Integer = 0 To rowCount - 1
            _prestressed_layerData(rowIndex) = New LayerDataType_Prestressed(
                dataGrid.Rows(rowIndex).Cells("clmDia").Value,
                dataGrid.Rows(rowIndex).Cells("clmN").Value,
                dataGrid.Rows(rowIndex).Cells("clmPrestressedAp").Value,
                dataGrid.Rows(rowIndex).Cells("clmPrestressedFyp").Value,
                dataGrid.Rows(rowIndex).Cells("clmPrestressedEsp").Value,
                dataGrid.Rows(rowIndex).Cells("clmPrestressedRupStrain").Value,
                dataGrid.Rows(rowIndex).Cells("clmPrestressedDi").Value,
                dataGrid.Rows(rowIndex).Cells("clmPrestressedPrestrain").Value,
                dataGrid.Rows(rowIndex).Cells("clmMatModel").Value
            )
        Next

        'Set kembalian dialog dan tutup dialog
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub


    '/**********************************************************************************************************/
    '/***************************************************************************************   EVENT HANDLER  */
    '/**********************************************************************************************************/

    Private Sub dataGrid_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dataGrid.DataError
        If (e.Exception.GetType = GetType(FormatException)) Then
            ' If users type a non Double input
            e.ThrowException = False
            MessageBox.Show("Input must be a real number.")
        End If
    End Sub

    Private Sub dataGrid_CellValidated(sender As Object, e As DataGridViewCellEventArgs) Handles dataGrid.CellValidated
        Dim rowCount As Integer = dataGrid.RowCount - 1
        ' Cek situasi yg memanggil fungsi ini
        If (dataGrid.Rows(e.RowIndex).Cells("clmPrestressedRupStrain").Value() Is Nothing And e.RowIndex < rowCount) Then
            ' Ketika bukan baris terakhir dan kolom clmSteelRupStrain belum diisi
            '' Isi nilai default clmSteelRupStrain
            dataGrid.Rows(e.RowIndex).Cells("clmPrestressedRupStrain").Value() = 0.05
        End If
    End Sub


    Private Sub dataGrid_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dataGrid.CellEndEdit
        Dim rowCount As Integer = dataGrid.RowCount - 1
        ' Cek situasi yg memanggil fungsi ini
        If (e.ColumnIndex = 0 Or e.ColumnIndex = 1) Then
            ' Ketika baru keluar dari kolom pertama (dia) atau kedua (N)'
            '' Kalkulasi nilai Ap dengan 0.25 * pi * dia^2 * N
            Dim dia = dataGrid.Rows(e.RowIndex).Cells("clmDia").Value()
            Dim N = dataGrid.Rows(e.RowIndex).Cells("clmN").Value()
            dataGrid.Rows(e.RowIndex).Cells("clmPrestressedAp").Value() = N * (Math.PI * 0.25 * dia ^ 2)
        ElseIf (e.ColumnIndex = 2) Then
            ' Ketika baru keluar dari kolom ketiga (Ap)
            '' hapus nilai dia dan N jika perkaliannya beda
            Dim dia = dataGrid.Rows(e.RowIndex).Cells("clmDia").Value()
            Dim N = dataGrid.Rows(e.RowIndex).Cells("clmN").Value()
            Dim ApPerkalian = N * (Math.PI * 0.25 * dia ^ 2)

            If (IsDBNull(dataGrid.Rows(e.RowIndex).Cells("clmPrestressedAp").Value())) Then
                'Kalau diisi kosong
                dataGrid.Rows(e.RowIndex).Cells("clmDia").Value() = 0.0
                dataGrid.Rows(e.RowIndex).Cells("clmN").Value() = 0.0
            ElseIf (dataGrid.Rows(e.RowIndex).Cells("clmPrestressedAp").Value() <> ApPerkalian) Then
                dataGrid.Rows(e.RowIndex).Cells("clmDia").Value() = 0.0
                dataGrid.Rows(e.RowIndex).Cells("clmN").Value() = 0.0
            End If
        End If
    End Sub

    Private Sub showDian_CheckedChanged(sender As Object, e As EventArgs) Handles showDian.CheckedChanged
        'Memunculkan/menyembunyikan kolom dia dan N jika tombol diklik
        If (showDian.Checked) Then
            dataGrid.Columns("clmDia").Visible = True
            dataGrid.Columns("clmN").Visible = True
        Else
            dataGrid.Columns("clmDia").Visible = False
            dataGrid.Columns("clmN").Visible = False
        End If
    End Sub
End Class
