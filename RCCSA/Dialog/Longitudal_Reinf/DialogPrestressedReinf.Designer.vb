﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DialogPrestressedReinf
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.lblH = New System.Windows.Forms.Label()
        Me.dataGrid = New System.Windows.Forms.DataGridView()
        Me.showDian = New System.Windows.Forms.CheckBox()
        Me.clmDia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmPrestressedAp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmPrestressedFyp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmPrestressedEsp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmPrestressedRupStrain = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmPrestressedDi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmPrestressedPrestrain = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmMatModel = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(491, 274)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(156, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(5, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(83, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'lblH
        '
        Me.lblH.AutoSize = True
        Me.lblH.Location = New System.Drawing.Point(12, 282)
        Me.lblH.Name = "lblH"
        Me.lblH.Size = New System.Drawing.Size(42, 13)
        Me.lblH.TabIndex = 11
        Me.lblH.Text = "H/Dia="
        '
        'dataGrid
        '
        Me.dataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clmDia, Me.clmN, Me.clmPrestressedAp, Me.clmPrestressedFyp, Me.clmPrestressedEsp, Me.clmPrestressedRupStrain, Me.clmPrestressedDi, Me.clmPrestressedPrestrain, Me.clmMatModel})
        Me.dataGrid.Location = New System.Drawing.Point(0, 59)
        Me.dataGrid.Name = "dataGrid"
        Me.dataGrid.Size = New System.Drawing.Size(660, 209)
        Me.dataGrid.TabIndex = 7
        '
        'showDian
        '
        Me.showDian.Appearance = System.Windows.Forms.Appearance.Button
        Me.showDian.AutoSize = True
        Me.showDian.Location = New System.Drawing.Point(12, 21)
        Me.showDian.Name = "showDian"
        Me.showDian.Size = New System.Drawing.Size(156, 23)
        Me.showDian.TabIndex = 12
        Me.showDian.TabStop = False
        Me.showDian.Text = "Diameter and Number of Bars"
        Me.showDian.UseVisualStyleBackColor = True
        '
        'clmDia
        '
        Me.clmDia.HeaderText = "dia [mm]"
        Me.clmDia.Name = "clmDia"
        Me.clmDia.Visible = False
        Me.clmDia.Width = 75
        '
        'clmN
        '
        Me.clmN.HeaderText = "N"
        Me.clmN.Name = "clmN"
        Me.clmN.Visible = False
        Me.clmN.Width = 75
        '
        'clmPrestressedAp
        '
        Me.clmPrestressedAp.HeaderText = "Ap [mm²]"
        Me.clmPrestressedAp.Name = "clmPrestressedAp"
        Me.clmPrestressedAp.ToolTipText = "Can be auto computed by DIA and N value, with formula N * PI * 0.25 * dia ^ 2"
        Me.clmPrestressedAp.Width = 80
        '
        'clmPrestressedFyp
        '
        Me.clmPrestressedFyp.HeaderText = "fyp [MPa]"
        Me.clmPrestressedFyp.Name = "clmPrestressedFyp"
        Me.clmPrestressedFyp.Width = 80
        '
        'clmPrestressedEsp
        '
        Me.clmPrestressedEsp.HeaderText = "Esp [MPa]"
        Me.clmPrestressedEsp.Name = "clmPrestressedEsp"
        Me.clmPrestressedEsp.Width = 80
        '
        'clmPrestressedRupStrain
        '
        Me.clmPrestressedRupStrain.HeaderText = "Rup. Strain [mm/mm]"
        Me.clmPrestressedRupStrain.Name = "clmPrestressedRupStrain"
        Me.clmPrestressedRupStrain.Width = 90
        '
        'clmPrestressedDi
        '
        Me.clmPrestressedDi.HeaderText = "di [mm]"
        Me.clmPrestressedDi.Name = "clmPrestressedDi"
        Me.clmPrestressedDi.Width = 80
        '
        'clmPrestressedPrestrain
        '
        Me.clmPrestressedPrestrain.HeaderText = "Prestrain [mm/mm]"
        Me.clmPrestressedPrestrain.Name = "clmPrestressedPrestrain"
        Me.clmPrestressedPrestrain.Width = 80
        '
        'clmMatModel
        '
        Me.clmMatModel.HeaderText = "Stress-Strain Curve"
        Me.clmMatModel.Name = "clmMatModel"
        Me.clmMatModel.Width = 115
        '
        'DialogPrestressedReinf
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(659, 315)
        Me.Controls.Add(Me.showDian)
        Me.Controls.Add(Me.lblH)
        Me.Controls.Add(Me.dataGrid)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DialogPrestressedReinf"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Prestressed Reinforcement Data"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents lblH As System.Windows.Forms.Label
    Friend WithEvents dataGrid As System.Windows.Forms.DataGridView
    Friend WithEvents showDian As System.Windows.Forms.CheckBox
    Friend WithEvents clmDia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmPrestressedAp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmPrestressedFyp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmPrestressedEsp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmPrestressedRupStrain As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmPrestressedDi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmPrestressedPrestrain As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmMatModel As System.Windows.Forms.DataGridViewComboBoxColumn

End Class
