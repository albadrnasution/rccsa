﻿Imports System.Windows.Forms
Imports System.ComponentModel

Public Class DialogFRPReinf

    Private _frp_layerData As LayerDataType_FRP()
    Private _frp_stressStrainModel As Integer
    Private _h As Double

    '================================================================================================================
    'PROPERTY
    '================================================================================================================

    'Properti yang merupakan penyimpan nilai variabel setiap layer dari reinforcement frp
    Public Property frp_layerData() As LayerDataType_FRP()
        Get
            Return _frp_layerData
        End Get
        Set(value As LayerDataType_FRP())
            _frp_layerData = value
        End Set
    End Property

    'Properti yang merupakan penyimpan model stress-strain dari reinforcement frp
    Public Property frp_stressStrainModel() As Integer
        Get
            Return _frp_stressStrainModel
        End Get
        Set(value As Integer)
            _frp_stressStrainModel = value
        End Set
    End Property

    'Properti height diperoleh dari jendela utama
    Public Property h() As Double
        Get
            Return _h
        End Get
        Set(value As Double)
            _h = value
        End Set
    End Property


    '/**********************************************************************************************************/
    '/***************************************************************************************   METHOD         */
    '/**********************************************************************************************************/

    Private Sub DialogFRPReinf_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '================================= PENGATURAN KOLOM ===============================
        ' Mengisi kolom Material Model dengan pilihan
        Dim clmMatModel As DataGridViewComboBoxColumn = CType(dataGrid.Columns("clmMatModel"), DataGridViewComboBoxColumn)
        'Memasukkan daftar chart ke dalam options
        Dim matModelOptions As New BindingList(Of CbOption)()
        matModelOptions.Add(New CbOption("1", "Linear"))
        matModelOptions.Add(New CbOption("2", "Hybrid"))
        matModelOptions.Add(New CbOption("3", "Ductile"))
        matModelOptions.Add(New CbOption("4", "Cylic FRP"))
        clmMatModel.DataSource = matModelOptions
        clmMatModel.DisplayMember = "display"
        clmMatModel.ValueMember = "value"

        'Set coloumns on the table grid to accept only Double
        dataGrid.Columns("clmDia").ValueType = GetType(Double)
        dataGrid.Columns("clmN").ValueType = GetType(Double)
        dataGrid.Columns("clmFrpAf").ValueType = GetType(Double)
        dataGrid.Columns("clmFrpFfu").ValueType = GetType(Double)
        dataGrid.Columns("clmFrpEf").ValueType = GetType(Double)
        dataGrid.Columns("clmFrpRupStrain").ValueType = GetType(Double)
        dataGrid.Columns("clmFrpDi").ValueType = GetType(Double)

        '================================= MENGESET NILAI JIKA ADA

        Try

            '=================================== MENGAMBIL LAYER DATA ==================================
            'Ambil data layer yang sudah diisi oleh user (ketika dialog ini dibuka sebelum sekarang atau dari file simpanan)
            Dim ss As LayerDataType_FRP() = Me.frp_layerData
            For index As Integer = 0 To ss.Length - 1
                dataGrid.Rows.Add()
                dataGrid.Rows(index).Cells("clmDia").Value() = ss(index).dia
                dataGrid.Rows(index).Cells("clmN").Value() = ss(index).N
                dataGrid.Rows(index).Cells("clmFrpAf").Value() = ss(index).af
                dataGrid.Rows(index).Cells("clmFrpFfu").Value() = ss(index).ffu
                dataGrid.Rows(index).Cells("clmFrpEf").Value() = ss(index).ef
                dataGrid.Rows(index).Cells("clmFrpRupStrain").Value() = ss(index).rup
                dataGrid.Rows(index).Cells("clmFrpDi").Value() = ss(index).di
                dataGrid.Rows(index).Cells("clmMatModel").Value() = ss(index).matModel
            Next
        Catch ex As ArgumentException
            'Wah, ada error apa ya?
            MessageBox.Show("Unexpected Error. " & ex.Message)
        Catch ex As NullReferenceException
            'Kalau null, abaikan berarti belum pernah diisi
        End Try

    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        '================================= MENYIMPAN LAYER DATA ===============================
        'Ambil jumlah baris, tinggalkan row terakhir yg isinya 0 semua
        Dim rowCount As Integer = dataGrid.RowCount - 1
        'Ambil jumlah kolom (ada 5 harusnya)
        Dim columnCount As Integer = dataGrid.ColumnCount

        'Inisiasi ulang variabel data nilai objek ini, pasang sebanyak rowCount buah
        ReDim _frp_layerData(rowCount - 1)
        'Ambil setiap data pada baris data layer dan masukkan ke variabel penyimpanan untuk dikembalikan ke MainForm
        For rowIndex As Integer = 0 To rowCount - 1
            _frp_layerData(rowIndex) = New LayerDataType_FRP(
                dataGrid.Rows(rowIndex).Cells("clmDia").Value,
                dataGrid.Rows(rowIndex).Cells("clmN").Value,
                dataGrid.Rows(rowIndex).Cells("clmFrpAf").Value,
                dataGrid.Rows(rowIndex).Cells("clmFrpFfu").Value,
                dataGrid.Rows(rowIndex).Cells("clmFrpEf").Value,
                dataGrid.Rows(rowIndex).Cells("clmFrpRupStrain").Value,
                dataGrid.Rows(rowIndex).Cells("clmFrpDi").Value,
                dataGrid.Rows(rowIndex).Cells("clmMatModel").Value
            )
        Next

        'Set kembalian dialog dan tutup dialog
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub


    '/**********************************************************************************************************/
    '/***************************************************************************************   EVENT HANDLER  */
    '/**********************************************************************************************************/
    Private Sub dataGrid_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dataGrid.DataError
        If (e.Exception.GetType = GetType(FormatException)) Then
            ' If users type a non Double input
            e.ThrowException = False
            MessageBox.Show("Input must be a real number.")
        End If
    End Sub

    Private Sub dataGrid_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dataGrid.CellEndEdit
        Dim rowCount As Integer = dataGrid.RowCount - 1
        ' Cek situasi yg memanggil fungsi ini
        If (e.ColumnIndex = 0 Or e.ColumnIndex = 1) Then
            ' Ketika baru keluar dari kolom pertama (dia) atau kedua (N)'
            '' Kalkulasi nilai Af dengan N * 0.25 * pi * dia^2
            Dim dia = dataGrid.Rows(e.RowIndex).Cells("clmDia").Value()
            Dim N = dataGrid.Rows(e.RowIndex).Cells("clmN").Value()
            dataGrid.Rows(e.RowIndex).Cells("clmFrpAf").Value() = N * (Math.PI * 0.25 * dia ^ 2)

        ElseIf (e.ColumnIndex = 2) Then
            ' Ketika baru keluar dari kolom ketiga (Af)
            '' hapus nilai dia dan N jika perkaliannya beda
            Dim dia = dataGrid.Rows(e.RowIndex).Cells("clmDia").Value()
            Dim N = dataGrid.Rows(e.RowIndex).Cells("clmN").Value()
            Dim AfPerkalian = N * (Math.PI * 0.25 * dia ^ 2)

            If (IsDBNull(dataGrid.Rows(e.RowIndex).Cells("clmFrpAf").Value())) Then
                'Kalau diisi kosong
                dataGrid.Rows(e.RowIndex).Cells("clmDia").Value() = 0.0
                dataGrid.Rows(e.RowIndex).Cells("clmN").Value() = 0.0
            ElseIf (dataGrid.Rows(e.RowIndex).Cells("clmFrpAf").Value() <> AfPerkalian) Then
                dataGrid.Rows(e.RowIndex).Cells("clmDia").Value() = 0.0
                dataGrid.Rows(e.RowIndex).Cells("clmN").Value() = 0.0
            End If

        ElseIf (e.ColumnIndex = 3 Or e.ColumnIndex = 4) Then
            ' Ketika keluar dari kolom keempat (Ef) atau kelima (Ffu)
            '' Hitung nilai rupstrain jika nilai ef dan ffu tidak kosong
            If (dataGrid.Rows(e.RowIndex).Cells("clmFrpEf").Value() IsNot Nothing And
                dataGrid.Rows(e.RowIndex).Cells("clmFrpFfu").Value() IsNot Nothing) Then

                'Set nilai rupstrain sebagai ffu / ef
                Dim ffu As Double = dataGrid.Rows(e.RowIndex).Cells("clmFrpFfu").Value()
                Dim Ef As Double = dataGrid.Rows(e.RowIndex).Cells("clmFrpEf").Value()
                dataGrid.Rows(e.RowIndex).Cells("clmFrpRupStrain").Value() = (ffu / Ef)
            End If
        End If

    End Sub

    Private Sub showDian_CheckedChanged(sender As Object, e As EventArgs) Handles showDian.CheckedChanged
        'Memunculkan/menyembunyikan kolom dia dan N jika tombol diklik
        If (showDian.Checked) Then
            dataGrid.Columns("clmDia").Visible = True
            dataGrid.Columns("clmN").Visible = True
        Else
            dataGrid.Columns("clmDia").Visible = False
            dataGrid.Columns("clmN").Visible = False
        End If
    End Sub
End Class
