﻿Imports System.Windows.Forms
Imports System.ComponentModel

Public Class dialogSteelReinf

    Private _steel_layerData As LayerDataType_Steel()
    Private _steel_stressStrainModel As Integer
    Private _h As Double

    '/**********************************************************************************************************/
    '/***************************************************************************************   PROPERTY       */
    '/**********************************************************************************************************/

    'Properti yang merupakan penyimpan nilai variabel setiap layer dari reinforcement steel
    Public Property steel_layerData() As LayerDataType_Steel()
        Get
            Return _steel_layerData
        End Get
        Set(value As LayerDataType_Steel())
            _steel_layerData = value
        End Set
    End Property

    'Properti yang merupakan penyimpan model stress-strain dari reinforcement steel
    Public Property steel_stressStrainModel() As Integer
        Get
            Return _steel_stressStrainModel
        End Get
        Set(value As Integer)
            _steel_stressStrainModel = value
        End Set
    End Property

    'Properti yang merupakan penyimpan model stress-strain dari reinforcement steel
    Public Property h() As Double
        Get
            Return _h
        End Get
        Set(value As Double)
            _h = value
        End Set
    End Property


    '/**********************************************************************************************************/
    '/***************************************************************************************   METHOD         */
    '/**********************************************************************************************************/

    ' Rutin yang dijalankan saat jendela dialog terbuka
    Private Sub dialogSteelReinf_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '================================= PENGATURAN KOLOM ===============================
        ' Mengisi kolom Material Model dengan pilihan
        Dim clmMatModel As DataGridViewComboBoxColumn = CType(dataGrid.Columns("clmMatModel"), DataGridViewComboBoxColumn)
        'Memasukkan daftar chart ke dalam options
        Dim matModelOptions As New BindingList(Of CbOption)()
        matModelOptions.Add(New CbOption("1", "Bi-Linear"))
        matModelOptions.Add(New CbOption("2", "Tri-Linear"))
        matModelOptions.Add(New CbOption("3", "Strain Hardening"))
        matModelOptions.Add(New CbOption("4", "Cylic Steel 1"))
        matModelOptions.Add(New CbOption("5", "Cylic Steel 2"))
        clmMatModel.DataSource = matModelOptions
        clmMatModel.DisplayMember = "display"
        clmMatModel.ValueMember = "value"

        'Set coloumns on the table grid to accept only Double
        dataGrid.Columns("clmDia").ValueType = GetType(Double)
        dataGrid.Columns("clmN").ValueType = GetType(Double)
        dataGrid.Columns("clmSteelAs").ValueType = GetType(Double)
        dataGrid.Columns("clmSteelFy").ValueType = GetType(Double)
        dataGrid.Columns("clmSteelEs").ValueType = GetType(Double)
        dataGrid.Columns("clmSteelRupStrain").ValueType = GetType(Double)
        dataGrid.Columns("clmSteelDi").ValueType = GetType(Double)

        '================================= MENGESET NILAI JIKA ADA

        Try

            '=================================== MENGAMBIL LAYER DATA ==================================
            'Ambil data layer yang sudah diisi oleh user (ketika dialog ini dibuka sebelum sekarang atau dari file simpanan)
            Dim ss As LayerDataType_Steel() = Me.steel_layerData
            For index As Integer = 0 To ss.Length - 1
                dataGrid.Rows.Add()
                dataGrid.Rows(index).Cells("clmDia").Value() = ss(index).dia
                dataGrid.Rows(index).Cells("clmN").Value() = ss(index).N
                dataGrid.Rows(index).Cells("clmSteelAs").Value() = ss(index).asv
                dataGrid.Rows(index).Cells("clmSteelFy").Value() = ss(index).fy
                dataGrid.Rows(index).Cells("clmSteelEs").Value() = ss(index).es
                dataGrid.Rows(index).Cells("clmSteelRupStrain").Value() = ss(index).rup
                dataGrid.Rows(index).Cells("clmSteelDi").Value() = ss(index).di
                dataGrid.Rows(index).Cells("clmMatModel").Value() = ss(index).matModel
            Next
        Catch ex As ArgumentException
            'Wah, ada error apa ya?
            MessageBox.Show("Unexpected Error. " & ex.Message)
        Catch ex As NullReferenceException
            'Kalau null, abaikan berarti belum pernah diisi
        End Try

    End Sub

    ' Rutin yang dijalankan saat tombol OK ditekan
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        '================================= MENYIMPAN LAYER DATA ===============================
        'Ambil jumlah baris, tinggalkan row terakhir yg isinya 0 semua
        Dim rowCount As Integer = dataGrid.RowCount - 1
        'Ambil jumlah kolom (ada 5 harusnya)
        Dim columnCount As Integer = dataGrid.ColumnCount

        'Inisiasi ulang variabel data nilai objek ini, pasang sebanyak rowCount buah
        ReDim _steel_layerData(rowCount - 1)
        'Ambil setiap data pada baris data layer dan masukkan ke variabel penyimpanan untuk dikembalikan ke MainForm
        For rowIndex As Integer = 0 To rowCount - 1
            _steel_layerData(rowIndex) = New LayerDataType_Steel(
                dataGrid.Rows(rowIndex).Cells("clmDia").Value,
                dataGrid.Rows(rowIndex).Cells("clmN").Value,
                dataGrid.Rows(rowIndex).Cells("clmSteelAs").Value,
                dataGrid.Rows(rowIndex).Cells("clmSteelFy").Value,
                dataGrid.Rows(rowIndex).Cells("clmSteelEs").Value,
                dataGrid.Rows(rowIndex).Cells("clmSteelRupStrain").Value,
                dataGrid.Rows(rowIndex).Cells("clmSteelDi").Value,
                dataGrid.Rows(rowIndex).Cells("clmMatModel").Value
            )
        Next

        'Set kembalian dialog dan tutup dialog
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub


    '/**********************************************************************************************************/
    '/***************************************************************************************   EVENT HANDLER  */
    '/**********************************************************************************************************/

    ' Handle error on dataGrid
    Private Sub dataGrid_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dataGrid.DataError
        If (e.Exception.GetType = GetType(FormatException)) Then
            ' If users type a non Double input
            e.ThrowException = False
            MessageBox.Show("Input must be a real number.")
        End If
    End Sub

    ' Handle automattic default value on cell(s)
    Private Sub dataGrid_CellValidated(sender As Object, e As DataGridViewCellEventArgs) Handles dataGrid.CellValidated
        Dim rowCount As Integer = dataGrid.RowCount - 1
        ' Cek situasi yg memanggil fungsi ini
        If (dataGrid.Rows(e.RowIndex).Cells("clmSteelRupStrain").Value() Is Nothing And e.RowIndex < rowCount) Then
            ' Ketika bukan baris terakhir dan kolom clmSteelRupStrain belum diisi
            '' Isi nilai default clmSteelRupStrain
            dataGrid.Rows(e.RowIndex).Cells("clmSteelRupStrain").Value() = 0.05
        End If
    End Sub

    Private Sub dataGrid_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dataGrid.CellEndEdit
        Dim rowCount As Integer = dataGrid.RowCount - 1
        ' Cek situasi yg memanggil fungsi ini
        If (e.ColumnIndex = 0 Or e.ColumnIndex = 1) Then
            ' Ketika baru keluar dari kolom pertama (dia) atau kedua (N)'
            '' Kalkulasi nilai As dengan N * (Math.PI * 0.25 * dia ^ 2)
            Dim dia = dataGrid.Rows(e.RowIndex).Cells("clmDia").Value()
            Dim N = dataGrid.Rows(e.RowIndex).Cells("clmN").Value()
            dataGrid.Rows(e.RowIndex).Cells("clmSteelAs").Value() = N * (Math.PI * 0.25 * dia ^ 2)
        ElseIf (e.ColumnIndex = 2) Then
            ' Ketika baru keluar dari kolom kedua (As)
            '' hapus nilai dia dan N jika perkaliannya beda
            Dim dia = dataGrid.Rows(e.RowIndex).Cells("clmDia").Value()
            Dim N = dataGrid.Rows(e.RowIndex).Cells("clmN").Value()
            Dim AsPerkalian = N * (Math.PI * 0.25 * dia ^ 2)

            If (IsDBNull(dataGrid.Rows(e.RowIndex).Cells("clmSteelAs").Value())) Then
                'Kalau diisi kosong
                dataGrid.Rows(e.RowIndex).Cells("clmDia").Value() = 0.0
                dataGrid.Rows(e.RowIndex).Cells("clmN").Value() = 0.0
            ElseIf (dataGrid.Rows(e.RowIndex).Cells("clmSteelAs").Value() <> AsPerkalian) Then
                'atau kalau isinya tidak sama
                dataGrid.Rows(e.RowIndex).Cells("clmDia").Value() = 0.0
                dataGrid.Rows(e.RowIndex).Cells("clmN").Value() = 0.0
            End If
        End If

    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles showDian.CheckedChanged
        'Memunculkan/menyembunyikan kolom dia dan N jika tombol diklik
        If (showDian.Checked) Then
            dataGrid.Columns("clmDia").Visible = True
            dataGrid.Columns("clmN").Visible = True
        Else
            dataGrid.Columns("clmDia").Visible = False
            dataGrid.Columns("clmN").Visible = False
        End If
    End Sub

End Class
