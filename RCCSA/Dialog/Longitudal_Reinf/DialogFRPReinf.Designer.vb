﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DialogFRPReinf
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.dataGrid = New System.Windows.Forms.DataGridView()
        Me.lblH = New System.Windows.Forms.Label()
        Me.showDian = New System.Windows.Forms.CheckBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.clmDia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmFrpAf = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmFrpFfu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmFrpEf = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmFrpRupStrain = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmFrpDi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clmMatModel = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(411, 274)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(156, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(5, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(83, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'dataGrid
        '
        Me.dataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clmDia, Me.clmN, Me.clmFrpAf, Me.clmFrpFfu, Me.clmFrpEf, Me.clmFrpRupStrain, Me.clmFrpDi, Me.clmMatModel})
        Me.dataGrid.Location = New System.Drawing.Point(0, 59)
        Me.dataGrid.Name = "dataGrid"
        Me.dataGrid.Size = New System.Drawing.Size(580, 209)
        Me.dataGrid.TabIndex = 7
        '
        'lblH
        '
        Me.lblH.AutoSize = True
        Me.lblH.Location = New System.Drawing.Point(12, 282)
        Me.lblH.Name = "lblH"
        Me.lblH.Size = New System.Drawing.Size(42, 13)
        Me.lblH.TabIndex = 11
        Me.lblH.Text = "H/Dia="
        '
        'showDian
        '
        Me.showDian.Appearance = System.Windows.Forms.Appearance.Button
        Me.showDian.AutoSize = True
        Me.showDian.Location = New System.Drawing.Point(12, 21)
        Me.showDian.Name = "showDian"
        Me.showDian.Size = New System.Drawing.Size(156, 23)
        Me.showDian.TabIndex = 13
        Me.showDian.TabStop = False
        Me.showDian.Text = "Diameter and Number of Bars"
        Me.ToolTip1.SetToolTip(Me.showDian, "Show the Diameter and N column")
        Me.showDian.UseVisualStyleBackColor = True
        '
        'clmDia
        '
        Me.clmDia.HeaderText = "dia [mm]"
        Me.clmDia.Name = "clmDia"
        Me.clmDia.Visible = False
        Me.clmDia.Width = 75
        '
        'clmN
        '
        Me.clmN.HeaderText = "N"
        Me.clmN.Name = "clmN"
        Me.clmN.Visible = False
        Me.clmN.Width = 75
        '
        'clmFrpAf
        '
        Me.clmFrpAf.HeaderText = "Af [mm²]"
        Me.clmFrpAf.Name = "clmFrpAf"
        Me.clmFrpAf.ToolTipText = "Can be auto computed by DIA and N value, with formula N * PI * 0.25 * dia ^ 2"
        Me.clmFrpAf.Width = 80
        '
        'clmFrpFfu
        '
        Me.clmFrpFfu.HeaderText = "ffu [MPa]"
        Me.clmFrpFfu.Name = "clmFrpFfu"
        Me.clmFrpFfu.Width = 80
        '
        'clmFrpEf
        '
        Me.clmFrpEf.HeaderText = "Ef [MPa]"
        Me.clmFrpEf.Name = "clmFrpEf"
        Me.clmFrpEf.Width = 80
        '
        'clmFrpRupStrain
        '
        Me.clmFrpRupStrain.HeaderText = "Rup. Strain [mm/mm]"
        Me.clmFrpRupStrain.Name = "clmFrpRupStrain"
        Me.clmFrpRupStrain.ToolTipText = "Auto computed, rupstrain = ffu / Ef"
        Me.clmFrpRupStrain.Width = 90
        '
        'clmFrpDi
        '
        Me.clmFrpDi.HeaderText = "di [mm]"
        Me.clmFrpDi.Name = "clmFrpDi"
        Me.clmFrpDi.Width = 80
        '
        'clmMatModel
        '
        Me.clmMatModel.HeaderText = "Stress-Strain Curve"
        Me.clmMatModel.Name = "clmMatModel"
        Me.clmMatModel.Width = 115
        '
        'DialogFRPReinf
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(579, 315)
        Me.Controls.Add(Me.showDian)
        Me.Controls.Add(Me.dataGrid)
        Me.Controls.Add(Me.lblH)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DialogFRPReinf"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FRP Reinforcement Data"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.dataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents dataGrid As System.Windows.Forms.DataGridView
    Friend WithEvents lblH As System.Windows.Forms.Label
    Friend WithEvents showDian As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents clmDia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmFrpAf As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmFrpFfu As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmFrpEf As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmFrpRupStrain As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmFrpDi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clmMatModel As System.Windows.Forms.DataGridViewComboBoxColumn

End Class
