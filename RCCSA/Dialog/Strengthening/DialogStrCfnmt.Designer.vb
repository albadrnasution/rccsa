﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DialogStrCfnmt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.inStrCfnmt_afrp = New System.Windows.Forms.RadioButton()
        Me.inStrCfnmt_gfrp = New System.Windows.Forms.RadioButton()
        Me.inStrCfnmt_cfrp = New System.Windows.Forms.RadioButton()
        Me.inStrCfnmt_sf = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.inStrCfnmt_bf = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.inStrCfnmt_continu = New System.Windows.Forms.RadioButton()
        Me.inStrCfnmt_discrit = New System.Windows.Forms.RadioButton()
        Me.inStrCfnmt_nf = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.inStrCfnmt_rad = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.inStrCfnmt_ef = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.inStrCfnmt_fuf = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.inStrCfnmt_tf = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.inStrCfnmt_ls = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.inStrCfnmt_us = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(106, 363)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.inStrCfnmt_afrp)
        Me.GroupBox1.Controls.Add(Me.inStrCfnmt_gfrp)
        Me.GroupBox1.Controls.Add(Me.inStrCfnmt_cfrp)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 312)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(240, 39)
        Me.GroupBox1.TabIndex = 134
        Me.GroupBox1.TabStop = False
        '
        'inStrCfnmt_afrp
        '
        Me.inStrCfnmt_afrp.AutoSize = True
        Me.inStrCfnmt_afrp.Location = New System.Drawing.Point(156, 14)
        Me.inStrCfnmt_afrp.Name = "inStrCfnmt_afrp"
        Me.inStrCfnmt_afrp.Size = New System.Drawing.Size(53, 17)
        Me.inStrCfnmt_afrp.TabIndex = 107
        Me.inStrCfnmt_afrp.TabStop = True
        Me.inStrCfnmt_afrp.Text = "AFRP"
        Me.inStrCfnmt_afrp.UseVisualStyleBackColor = True
        '
        'inStrCfnmt_gfrp
        '
        Me.inStrCfnmt_gfrp.AutoSize = True
        Me.inStrCfnmt_gfrp.Location = New System.Drawing.Point(96, 14)
        Me.inStrCfnmt_gfrp.Name = "inStrCfnmt_gfrp"
        Me.inStrCfnmt_gfrp.Size = New System.Drawing.Size(54, 17)
        Me.inStrCfnmt_gfrp.TabIndex = 106
        Me.inStrCfnmt_gfrp.TabStop = True
        Me.inStrCfnmt_gfrp.Text = "GFRP"
        Me.inStrCfnmt_gfrp.UseVisualStyleBackColor = True
        '
        'inStrCfnmt_cfrp
        '
        Me.inStrCfnmt_cfrp.AutoSize = True
        Me.inStrCfnmt_cfrp.Location = New System.Drawing.Point(32, 14)
        Me.inStrCfnmt_cfrp.Name = "inStrCfnmt_cfrp"
        Me.inStrCfnmt_cfrp.Size = New System.Drawing.Size(53, 17)
        Me.inStrCfnmt_cfrp.TabIndex = 105
        Me.inStrCfnmt_cfrp.TabStop = True
        Me.inStrCfnmt_cfrp.Text = "CFRP"
        Me.inStrCfnmt_cfrp.UseVisualStyleBackColor = True
        '
        'inStrCfnmt_sf
        '
        Me.inStrCfnmt_sf.Location = New System.Drawing.Point(88, 285)
        Me.inStrCfnmt_sf.Name = "inStrCfnmt_sf"
        Me.inStrCfnmt_sf.Size = New System.Drawing.Size(120, 20)
        Me.inStrCfnmt_sf.TabIndex = 133
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(58, 288)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 13)
        Me.Label1.TabIndex = 132
        Me.Label1.Text = "sf ="
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrCfnmt_bf
        '
        Me.inStrCfnmt_bf.Location = New System.Drawing.Point(88, 258)
        Me.inStrCfnmt_bf.Name = "inStrCfnmt_bf"
        Me.inStrCfnmt_bf.Size = New System.Drawing.Size(120, 20)
        Me.inStrCfnmt_bf.TabIndex = 131
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(56, 261)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 13)
        Me.Label2.TabIndex = 130
        Me.Label2.Text = "bf ="
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrCfnmt_continu
        '
        Me.inStrCfnmt_continu.AutoSize = True
        Me.inStrCfnmt_continu.Location = New System.Drawing.Point(133, 228)
        Me.inStrCfnmt_continu.Name = "inStrCfnmt_continu"
        Me.inStrCfnmt_continu.Size = New System.Drawing.Size(78, 17)
        Me.inStrCfnmt_continu.TabIndex = 129
        Me.inStrCfnmt_continu.TabStop = True
        Me.inStrCfnmt_continu.Text = "Continuous"
        Me.inStrCfnmt_continu.UseVisualStyleBackColor = True
        '
        'inStrCfnmt_discrit
        '
        Me.inStrCfnmt_discrit.AutoSize = True
        Me.inStrCfnmt_discrit.Location = New System.Drawing.Point(48, 228)
        Me.inStrCfnmt_discrit.Name = "inStrCfnmt_discrit"
        Me.inStrCfnmt_discrit.Size = New System.Drawing.Size(64, 17)
        Me.inStrCfnmt_discrit.TabIndex = 128
        Me.inStrCfnmt_discrit.TabStop = True
        Me.inStrCfnmt_discrit.Text = "Discrete"
        Me.inStrCfnmt_discrit.UseVisualStyleBackColor = True
        '
        'inStrCfnmt_nf
        '
        Me.inStrCfnmt_nf.Location = New System.Drawing.Point(88, 193)
        Me.inStrCfnmt_nf.Name = "inStrCfnmt_nf"
        Me.inStrCfnmt_nf.Size = New System.Drawing.Size(120, 20)
        Me.inStrCfnmt_nf.TabIndex = 127
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(57, 196)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(25, 13)
        Me.Label21.TabIndex = 126
        Me.Label21.Text = "nf ="
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrCfnmt_rad
        '
        Me.inStrCfnmt_rad.Location = New System.Drawing.Point(88, 166)
        Me.inStrCfnmt_rad.Name = "inStrCfnmt_rad"
        Me.inStrCfnmt_rad.Size = New System.Drawing.Size(120, 20)
        Me.inStrCfnmt_rad.TabIndex = 125
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(38, 169)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(44, 13)
        Me.Label19.TabIndex = 124
        Me.Label19.Text = "radius ="
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(216, 142)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(29, 13)
        Me.Label16.TabIndex = 123
        Me.Label16.Text = "[mm]"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(60, 142)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(22, 13)
        Me.Label17.TabIndex = 120
        Me.Label17.Text = "tf ="
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrCfnmt_ef
        '
        Me.inStrCfnmt_ef.Location = New System.Drawing.Point(88, 60)
        Me.inStrCfnmt_ef.Name = "inStrCfnmt_ef"
        Me.inStrCfnmt_ef.Size = New System.Drawing.Size(120, 20)
        Me.inStrCfnmt_ef.TabIndex = 117
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(56, 63)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(26, 13)
        Me.Label15.TabIndex = 116
        Me.Label15.Text = "Ef ="
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrCfnmt_fuf
        '
        Me.inStrCfnmt_fuf.Location = New System.Drawing.Point(88, 33)
        Me.inStrCfnmt_fuf.Name = "inStrCfnmt_fuf"
        Me.inStrCfnmt_fuf.Size = New System.Drawing.Size(120, 20)
        Me.inStrCfnmt_fuf.TabIndex = 114
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(216, 36)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 13)
        Me.Label12.TabIndex = 115
        Me.Label12.Text = "[MPa]"
        '
        'inStrCfnmt_tf
        '
        Me.inStrCfnmt_tf.Location = New System.Drawing.Point(88, 139)
        Me.inStrCfnmt_tf.Name = "inStrCfnmt_tf"
        Me.inStrCfnmt_tf.Size = New System.Drawing.Size(120, 20)
        Me.inStrCfnmt_tf.TabIndex = 122
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(216, 63)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 13)
        Me.Label14.TabIndex = 119
        Me.Label14.Text = "[MPa]"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(54, 36)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(28, 13)
        Me.Label13.TabIndex = 113
        Me.Label13.Text = "fuf ="
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrCfnmt_ls
        '
        Me.inStrCfnmt_ls.Location = New System.Drawing.Point(88, 112)
        Me.inStrCfnmt_ls.Name = "inStrCfnmt_ls"
        Me.inStrCfnmt_ls.Size = New System.Drawing.Size(120, 20)
        Me.inStrCfnmt_ls.TabIndex = 121
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(20, 115)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(62, 13)
        Me.Label29.TabIndex = 112
        Me.Label29.Text = "Lim.Strain ="
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrCfnmt_us
        '
        Me.inStrCfnmt_us.Location = New System.Drawing.Point(88, 85)
        Me.inStrCfnmt_us.Name = "inStrCfnmt_us"
        Me.inStrCfnmt_us.Size = New System.Drawing.Size(120, 20)
        Me.inStrCfnmt_us.TabIndex = 118
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(23, 88)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(59, 13)
        Me.Label31.TabIndex = 111
        Me.Label31.Text = "Ult.Strain ="
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(215, 262)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 135
        Me.Label3.Text = "[mm]"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(215, 289)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 13)
        Me.Label4.TabIndex = 136
        Me.Label4.Text = "[mm]"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(215, 170)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 13)
        Me.Label5.TabIndex = 137
        Me.Label5.Text = "[mm]"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(213, 89)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 138
        Me.Label8.Text = "[mm/mm]"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(213, 116)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 13)
        Me.Label6.TabIndex = 139
        Me.Label6.Text = "[mm/mm]"
        '
        'DialogStrCfnmt
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(264, 404)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.inStrCfnmt_sf)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.inStrCfnmt_bf)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.inStrCfnmt_continu)
        Me.Controls.Add(Me.inStrCfnmt_discrit)
        Me.Controls.Add(Me.inStrCfnmt_nf)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.inStrCfnmt_rad)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.inStrCfnmt_ef)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.inStrCfnmt_fuf)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.inStrCfnmt_tf)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.inStrCfnmt_ls)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.inStrCfnmt_us)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DialogStrCfnmt"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FRP Confinement Data"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents inStrCfnmt_afrp As System.Windows.Forms.RadioButton
    Friend WithEvents inStrCfnmt_gfrp As System.Windows.Forms.RadioButton
    Friend WithEvents inStrCfnmt_cfrp As System.Windows.Forms.RadioButton
    Friend WithEvents inStrCfnmt_sf As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents inStrCfnmt_bf As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents inStrCfnmt_continu As System.Windows.Forms.RadioButton
    Friend WithEvents inStrCfnmt_discrit As System.Windows.Forms.RadioButton
    Friend WithEvents inStrCfnmt_nf As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents inStrCfnmt_rad As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents inStrCfnmt_ef As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents inStrCfnmt_fuf As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents inStrCfnmt_tf As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents inStrCfnmt_ls As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents inStrCfnmt_us As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label

End Class
