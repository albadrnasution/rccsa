﻿Imports System.Windows.Forms

Public Class DialogStrCfnmt

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub inStrCfnmt_discrit_CheckedChanged(sender As Object, e As EventArgs) Handles inStrCfnmt_discrit.CheckedChanged
        inStrCfnmt_bf.Enabled = True
        inStrCfnmt_sf.Enabled = True
    End Sub

    Private Sub inStrCfnmt_continu_CheckedChanged(sender As Object, e As EventArgs) Handles inStrCfnmt_continu.CheckedChanged
        inStrCfnmt_bf.Enabled = False
        inStrCfnmt_sf.Enabled = False
    End Sub
End Class
