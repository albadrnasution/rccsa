﻿Imports System.Windows.Forms

Public Class DialogStrFlex

    Private _h As Double

    'Properti height diperoleh dari jendela utama
    Public Property h() As Double
        Get
            Return _h
        End Get
        Set(value As Double)
            _h = value
        End Set
    End Property

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub inStrFlex_tp_Leave(sender As Object, e As EventArgs) Handles inStrFlex_tp.Leave
        Try
            'ambil nilai tp dan ubah dari string ke double
            Dim tp As Double = Convert.ToDouble(inStrFlex_tp.Text)
            'isi otomatis nilai di sesuai dengan tp dan H
            inStrFlex_di.Text = h + 0.5 * tp
        Catch ex As Exception
            'Error (terjadi saat tp dikosongkan)
            inStrFlex_di.Text = ""
        End Try
    End Sub
End Class
