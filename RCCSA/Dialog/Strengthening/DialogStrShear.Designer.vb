﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DialogStrShear
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.inStrShear_nf = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.inStrShear_ang = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.inStrShear_tf = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.inStrShear_ef = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.inStrShear_fuf = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.inStrShear_ls = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.inStrShear_us = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.inStrShear_discrit = New System.Windows.Forms.RadioButton()
        Me.inStrShear_continu = New System.Windows.Forms.RadioButton()
        Me.inStrShear_sf = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.inStrShear_bf = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.inStrShear_cfrp = New System.Windows.Forms.RadioButton()
        Me.inStrShear_gfrp = New System.Windows.Forms.RadioButton()
        Me.inStrShear_afrp = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(106, 363)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'inStrShear_nf
        '
        Me.inStrShear_nf.Location = New System.Drawing.Point(85, 189)
        Me.inStrShear_nf.Name = "inStrShear_nf"
        Me.inStrShear_nf.Size = New System.Drawing.Size(120, 20)
        Me.inStrShear_nf.TabIndex = 98
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(54, 192)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(25, 13)
        Me.Label21.TabIndex = 97
        Me.Label21.Text = "nf ="
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrShear_ang
        '
        Me.inStrShear_ang.Location = New System.Drawing.Point(85, 162)
        Me.inStrShear_ang.Name = "inStrShear_ang"
        Me.inStrShear_ang.Size = New System.Drawing.Size(120, 20)
        Me.inStrShear_ang.TabIndex = 95
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(37, 165)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 13)
        Me.Label19.TabIndex = 94
        Me.Label19.Text = "angle ="
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrShear_tf
        '
        Me.inStrShear_tf.Location = New System.Drawing.Point(85, 135)
        Me.inStrShear_tf.Name = "inStrShear_tf"
        Me.inStrShear_tf.Size = New System.Drawing.Size(120, 20)
        Me.inStrShear_tf.TabIndex = 92
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(213, 138)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(29, 13)
        Me.Label16.TabIndex = 93
        Me.Label16.Text = "[mm]"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(57, 138)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(22, 13)
        Me.Label17.TabIndex = 90
        Me.Label17.Text = "tf ="
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrShear_ef
        '
        Me.inStrShear_ef.Location = New System.Drawing.Point(85, 56)
        Me.inStrShear_ef.Name = "inStrShear_ef"
        Me.inStrShear_ef.Size = New System.Drawing.Size(120, 20)
        Me.inStrShear_ef.TabIndex = 87
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(213, 59)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 13)
        Me.Label14.TabIndex = 89
        Me.Label14.Text = "[MPa]"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(53, 59)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(26, 13)
        Me.Label15.TabIndex = 86
        Me.Label15.Text = "Ef ="
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrShear_fuf
        '
        Me.inStrShear_fuf.Location = New System.Drawing.Point(85, 29)
        Me.inStrShear_fuf.Name = "inStrShear_fuf"
        Me.inStrShear_fuf.Size = New System.Drawing.Size(120, 20)
        Me.inStrShear_fuf.TabIndex = 84
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(213, 32)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 13)
        Me.Label12.TabIndex = 85
        Me.Label12.Text = "[MPa]"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(51, 32)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(28, 13)
        Me.Label13.TabIndex = 83
        Me.Label13.Text = "fuf ="
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrShear_ls
        '
        Me.inStrShear_ls.Location = New System.Drawing.Point(85, 108)
        Me.inStrShear_ls.Name = "inStrShear_ls"
        Me.inStrShear_ls.Size = New System.Drawing.Size(120, 20)
        Me.inStrShear_ls.TabIndex = 91
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 111)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(62, 13)
        Me.Label29.TabIndex = 82
        Me.Label29.Text = "Lim.Strain ="
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrShear_us
        '
        Me.inStrShear_us.Location = New System.Drawing.Point(85, 81)
        Me.inStrShear_us.Name = "inStrShear_us"
        Me.inStrShear_us.Size = New System.Drawing.Size(120, 20)
        Me.inStrShear_us.TabIndex = 88
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(20, 84)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(59, 13)
        Me.Label31.TabIndex = 81
        Me.Label31.Text = "Ult.Strain ="
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrShear_discrit
        '
        Me.inStrShear_discrit.AutoSize = True
        Me.inStrShear_discrit.Location = New System.Drawing.Point(45, 224)
        Me.inStrShear_discrit.Name = "inStrShear_discrit"
        Me.inStrShear_discrit.Size = New System.Drawing.Size(64, 17)
        Me.inStrShear_discrit.TabIndex = 99
        Me.inStrShear_discrit.TabStop = True
        Me.inStrShear_discrit.Text = "Discrete"
        Me.inStrShear_discrit.UseVisualStyleBackColor = True
        '
        'inStrShear_continu
        '
        Me.inStrShear_continu.AutoSize = True
        Me.inStrShear_continu.Location = New System.Drawing.Point(130, 224)
        Me.inStrShear_continu.Name = "inStrShear_continu"
        Me.inStrShear_continu.Size = New System.Drawing.Size(78, 17)
        Me.inStrShear_continu.TabIndex = 100
        Me.inStrShear_continu.TabStop = True
        Me.inStrShear_continu.Text = "Continuous"
        Me.inStrShear_continu.UseVisualStyleBackColor = True
        '
        'inStrShear_sf
        '
        Me.inStrShear_sf.Location = New System.Drawing.Point(85, 281)
        Me.inStrShear_sf.Name = "inStrShear_sf"
        Me.inStrShear_sf.Size = New System.Drawing.Size(120, 20)
        Me.inStrShear_sf.TabIndex = 104
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(55, 284)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 13)
        Me.Label1.TabIndex = 103
        Me.Label1.Text = "sf ="
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrShear_bf
        '
        Me.inStrShear_bf.Location = New System.Drawing.Point(85, 254)
        Me.inStrShear_bf.Name = "inStrShear_bf"
        Me.inStrShear_bf.Size = New System.Drawing.Size(120, 20)
        Me.inStrShear_bf.TabIndex = 102
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(53, 257)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 13)
        Me.Label2.TabIndex = 101
        Me.Label2.Text = "bf ="
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'inStrShear_cfrp
        '
        Me.inStrShear_cfrp.AutoSize = True
        Me.inStrShear_cfrp.Location = New System.Drawing.Point(32, 14)
        Me.inStrShear_cfrp.Name = "inStrShear_cfrp"
        Me.inStrShear_cfrp.Size = New System.Drawing.Size(53, 17)
        Me.inStrShear_cfrp.TabIndex = 105
        Me.inStrShear_cfrp.TabStop = True
        Me.inStrShear_cfrp.Text = "CFRP"
        Me.inStrShear_cfrp.UseVisualStyleBackColor = True
        '
        'inStrShear_gfrp
        '
        Me.inStrShear_gfrp.AutoSize = True
        Me.inStrShear_gfrp.Location = New System.Drawing.Point(96, 14)
        Me.inStrShear_gfrp.Name = "inStrShear_gfrp"
        Me.inStrShear_gfrp.Size = New System.Drawing.Size(54, 17)
        Me.inStrShear_gfrp.TabIndex = 106
        Me.inStrShear_gfrp.TabStop = True
        Me.inStrShear_gfrp.Text = "GFRP"
        Me.inStrShear_gfrp.UseVisualStyleBackColor = True
        '
        'inStrShear_afrp
        '
        Me.inStrShear_afrp.AutoSize = True
        Me.inStrShear_afrp.Location = New System.Drawing.Point(156, 14)
        Me.inStrShear_afrp.Name = "inStrShear_afrp"
        Me.inStrShear_afrp.Size = New System.Drawing.Size(53, 17)
        Me.inStrShear_afrp.TabIndex = 107
        Me.inStrShear_afrp.TabStop = True
        Me.inStrShear_afrp.Text = "AFRP"
        Me.inStrShear_afrp.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.inStrShear_afrp)
        Me.GroupBox1.Controls.Add(Me.inStrShear_gfrp)
        Me.GroupBox1.Controls.Add(Me.inStrShear_cfrp)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 308)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(240, 39)
        Me.GroupBox1.TabIndex = 108
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(213, 258)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 109
        Me.Label3.Text = "[mm]"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(213, 284)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 13)
        Me.Label4.TabIndex = 110
        Me.Label4.Text = "[mm]"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(211, 85)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 111
        Me.Label8.Text = "[mm/mm]"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(211, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 112
        Me.Label5.Text = "[mm/mm]"
        '
        'DialogStrShear
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(264, 404)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.inStrShear_sf)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.inStrShear_bf)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.inStrShear_continu)
        Me.Controls.Add(Me.inStrShear_discrit)
        Me.Controls.Add(Me.inStrShear_nf)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.inStrShear_ang)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.inStrShear_tf)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.inStrShear_ef)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.inStrShear_fuf)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.inStrShear_ls)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.inStrShear_us)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DialogStrShear"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Shear Strengthening"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents inStrShear_nf As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents inStrShear_ang As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents inStrShear_tf As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents inStrShear_ef As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents inStrShear_fuf As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents inStrShear_ls As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents inStrShear_us As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents inStrShear_discrit As System.Windows.Forms.RadioButton
    Friend WithEvents inStrShear_continu As System.Windows.Forms.RadioButton
    Friend WithEvents inStrShear_sf As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents inStrShear_bf As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents inStrShear_cfrp As System.Windows.Forms.RadioButton
    Friend WithEvents inStrShear_gfrp As System.Windows.Forms.RadioButton
    Friend WithEvents inStrShear_afrp As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label

End Class
