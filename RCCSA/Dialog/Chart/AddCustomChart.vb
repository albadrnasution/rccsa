﻿Imports System.Windows.Forms
Imports System.ComponentModel


''' <summary>
''' Dialog ini menampilkan dialog untuk menambah chart dari txt yang belum di premeditasi.
''' Chart bisa ditampilkan baru ke area grafik yang ada atau ditambahkan dengan chart yang baru.
''' </summary>
''' <remarks></remarks>
Public Class AddCustomChart

    Private Sub AddCustomChart_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim chartList As New BindingList(Of CbOption)()
        chartList.Add(New CbOption("main", "Main Chart"))
        chartList.Add(New CbOption("mat_model", "Material Model Chart"))
        intoChart.DataSource = chartList
        intoChart.DisplayMember = "display"
        intoChart.ValueMember = "value"
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnOpenFile_Click(sender As Object, e As EventArgs) Handles btnOpenFile.Click
        OpenFileDialog1.Title = "Please Select a File"
        OpenFileDialog1.InitialDirectory = "C:temp"
        OpenFileDialog1.ShowDialog()
    End Sub

    Private Sub OpenFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        inFilepath.Text = OpenFileDialog1.FileName.ToString()
    End Sub

    Private Sub cbAddToCurrentChart_CheckedChanged(sender As Object, e As EventArgs) Handles cbAddToCurrentChart.CheckedChanged
        If (cbAddToCurrentChart.Checked) Then
            inLabelX.Enabled = False
            inLabelY.Enabled = False
        Else
            inLabelX.Enabled = True
            inLabelY.Enabled = True
        End If
    End Sub

    Private Sub inFilepath_MouseDown(sender As Object, e As MouseEventArgs) Handles inFilepath.MouseDown
        btnOpenFile_Click(sender, e)
    End Sub

End Class
