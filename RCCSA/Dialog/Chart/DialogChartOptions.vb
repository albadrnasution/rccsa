﻿Imports System.Windows.Forms
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.ComponentModel

Public Class DialogChartOptions
    'Chart yang jadi fokus sekarang
    Public currentChart As Chart

    'Mode All, nanti ketiga chart akan diambil
    Public _main As Chart
    Public _matModel As Chart
    Public _tab As TabControl
    Public modeAll As Boolean = False

    'Default value
    Private defaultTitle As String
    Private defaultXLabel As String
    Private defaultYLabel As String
    Private defaultSeriesName() As String
    Private defaultSeriesColor() As Color
    Private default_xReverse As Boolean
    Private default_yReverse As Boolean

    Private Sub DialogChartOptions_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        populateChartList()

        If (Not modeAll) Then
            hideChartList()
            fetchCurrentChart()
        Else
            showChartList()
        End If
    End Sub

    ''' <summary>
    ''' Mengambil data awal chart sekarang untuk dijadikan data default saat tombol cancel di tekan
    ''' dan memberikan informasi awal untuk user.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub fetchCurrentChart()
        currentChart.ApplyPaletteColors()

        Dim index As Integer = 0
        Dim count As Integer = currentChart.Series.Count
        ReDim defaultSeriesName(count - 1)
        ReDim defaultSeriesColor(count - 1)
        'Load series
        For Each series As Series In currentChart.Series
            DataGridView1.Rows.Add()
            DataGridView1.Rows(index).Cells("series_name").Value = series.Name
            DataGridView1.Rows(index).Cells("series_color").Style.BackColor = series.Color
            defaultSeriesName(index) = series.Name
            defaultSeriesColor(index) = series.Color
            index = index + 1
        Next

        'Load chart property
        inChartTitle.Text = currentChart.Titles(0).Text
        defaultTitle = currentChart.Titles(0).Text
        inXLabel.Text = currentChart.ChartAreas(0).AxisX.Title
        defaultXLabel = currentChart.ChartAreas(0).AxisX.Title
        inYLabel.Text = currentChart.ChartAreas(0).AxisY.Title
        defaultYLabel = currentChart.ChartAreas(0).AxisY.Title
        'Axis reversal
        tgbtnReverseXaxis.Checked = currentChart.ChartAreas(0).AxisX.IsReversed
        tgbtnReverseYaxis.Checked = currentChart.ChartAreas(0).AxisY.IsReversed
        default_xReverse = currentChart.ChartAreas(0).AxisX.IsReversed
        default_yReverse = currentChart.ChartAreas(0).AxisY.IsReversed
    End Sub

    Private Sub hideChartList()
        lblChartList.Visible = False
        intoChart.Visible = False
        chartChange.Visible = False
        Panel1.Location = New System.Drawing.Point(Panel1.Location.X, 20)
    End Sub

    Private Sub showChartList()
        lblChartList.Visible = True
        intoChart.Visible = True
        chartChange.Visible = True
    End Sub

    Private Sub populateChartList()
        Dim chartList As New BindingList(Of CbOption)()
        chartList.Add(New CbOption("main", "Main Chart"))
        chartList.Add(New CbOption("mat_model", "Material Model Chart"))
        intoChart.DataSource = chartList
        intoChart.DisplayMember = "display"
        intoChart.ValueMember = "value"
    End Sub

    Private Sub chartChange_Click(sender As Object, e As EventArgs) Handles chartChange.Click
        DataGridView1.Rows.Clear()
        Debug.WriteLine(intoChart.SelectedValue)
        If (intoChart.SelectedValue = "main") Then
            currentChart = _main
            _tab.SelectedIndex = 3
        ElseIf (intoChart.SelectedValue = "mat_model") Then
            currentChart = _matModel
            _tab.SelectedIndex = 2
        End If
        fetchCurrentChart()
    End Sub

    ''' <summary>
    ''' Revert all options changed into current chart previous value
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub revertCurrentChart()
        currentChart.Titles(0).Text = defaultTitle
        currentChart.ChartAreas(0).AxisX.Title = defaultXLabel
        currentChart.ChartAreas(0).AxisY.Title = defaultYLabel
        currentChart.ChartAreas(0).AxisX.IsReversed = default_xReverse
        currentChart.ChartAreas(0).AxisY.IsReversed = default_yReverse

        Dim count As Integer = currentChart.Series.Count
        For index As Integer = 0 To count - 1
            currentChart.Series(index).Name = defaultSeriesName(index)
            currentChart.Series(index).Color = defaultSeriesColor(index)
        Next
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        'Jika ada chart yang dipilih
        If (currentChart IsNot Nothing) Then
            revertCurrentChart()
        End If

        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DataGridView1_CellStateChanged(sender As Object, e As DataGridViewCellStateChangedEventArgs) Handles DataGridView1.CellStateChanged
        If e.Cell.ColumnIndex = 1 Then
            e.Cell.Selected = False
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If (e.ColumnIndex = 2) Then
            ColorDialog1.Color = DataGridView1.Rows(e.RowIndex).Cells("series_color").Style.BackColor
            If (ColorDialog1.ShowDialog) Then
                DataGridView1.Rows(e.RowIndex).Cells("series_color").Style.BackColor = ColorDialog1.Color
                currentChart.Series(e.RowIndex).Color = ColorDialog1.Color
            End If
        ElseIf (e.ColumnIndex = 3) Then
            Dim dlg As New DialogChartPoints()

            For Each p As DataPoint In currentChart.Series(e.RowIndex).Points
                dlg.dgv.Rows.Add(p.XValue, p.YValues(0))
            Next

            dlg.ShowDialog()
        End If
    End Sub

    Private Sub DataGridView1_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellEndEdit
        If (e.ColumnIndex = 0) Then
            currentChart.Series(e.RowIndex).Name = DataGridView1.Rows(e.RowIndex).Cells("series_name").Value
            Debug.WriteLine(currentChart.Series(e.RowIndex).Name)
        End If
    End Sub

    Private Sub inChartTitle_TextChanged(sender As Object, e As EventArgs) Handles inChartTitle.TextChanged
        currentChart.Titles(0).Text = inChartTitle.Text
    End Sub

    Private Sub inXLabel_TextChanged(sender As Object, e As EventArgs) Handles inXLabel.TextChanged
        currentChart.ChartAreas(0).AxisX.Title = inXLabel.Text
    End Sub

    Private Sub inYLabel_TextChanged(sender As Object, e As EventArgs) Handles inYLabel.TextChanged
        currentChart.ChartAreas(0).AxisY.Title = inYLabel.Text
    End Sub

    Private Sub tgbtnReverseXaxis_CheckedChanged(sender As Object, e As EventArgs) Handles tgbtnReverseXaxis.CheckedChanged
        currentChart.ChartAreas(0).AxisX.IsReversed = tgbtnReverseXaxis.Checked
    End Sub

    Private Sub tgbtnReverseYaxis_CheckedChanged(sender As Object, e As EventArgs) Handles tgbtnReverseYaxis.CheckedChanged
        currentChart.ChartAreas(0).AxisY.IsReversed = tgbtnReverseYaxis.Checked
    End Sub

End Class
