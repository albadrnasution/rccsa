﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DialogChartOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.series_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.series_color = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.change_color = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.see_points = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.tgbtnReverseXaxis = New System.Windows.Forms.CheckBox()
        Me.tgbtnReverseYaxis = New System.Windows.Forms.CheckBox()
        Me.inChartTitle = New System.Windows.Forms.TextBox()
        Me.inXLabel = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.inYLabel = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.intoChart = New System.Windows.Forms.ComboBox()
        Me.lblChartList = New System.Windows.Forms.Label()
        Me.chartChange = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(336, 257)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Chart Title:"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.series_name, Me.series_color, Me.change_color, Me.see_points})
        Me.DataGridView1.Location = New System.Drawing.Point(12, 134)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(470, 111)
        Me.DataGridView1.TabIndex = 3
        '
        'series_name
        '
        Me.series_name.HeaderText = "Series Name"
        Me.series_name.Name = "series_name"
        Me.series_name.Width = 200
        '
        'series_color
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(20)
        Me.series_color.DefaultCellStyle = DataGridViewCellStyle1
        Me.series_color.HeaderText = "Color"
        Me.series_color.Name = "series_color"
        Me.series_color.ReadOnly = True
        Me.series_color.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.series_color.Width = 50
        '
        'change_color
        '
        Me.change_color.HeaderText = ""
        Me.change_color.Name = "change_color"
        Me.change_color.Text = "Change Color"
        Me.change_color.UseColumnTextForButtonValue = True
        Me.change_color.Width = 80
        '
        'see_points
        '
        Me.see_points.HeaderText = ""
        Me.see_points.Name = "see_points"
        Me.see_points.Text = "See Points"
        Me.see_points.UseColumnTextForButtonValue = True
        Me.see_points.Width = 80
        '
        'tgbtnReverseXaxis
        '
        Me.tgbtnReverseXaxis.Appearance = System.Windows.Forms.Appearance.Button
        Me.tgbtnReverseXaxis.AutoSize = True
        Me.tgbtnReverseXaxis.Location = New System.Drawing.Point(330, 28)
        Me.tgbtnReverseXaxis.Name = "tgbtnReverseXaxis"
        Me.tgbtnReverseXaxis.Size = New System.Drawing.Size(89, 23)
        Me.tgbtnReverseXaxis.TabIndex = 6
        Me.tgbtnReverseXaxis.Text = "Reverse X Axis"
        Me.tgbtnReverseXaxis.UseVisualStyleBackColor = True
        '
        'tgbtnReverseYaxis
        '
        Me.tgbtnReverseYaxis.Appearance = System.Windows.Forms.Appearance.Button
        Me.tgbtnReverseYaxis.AutoSize = True
        Me.tgbtnReverseYaxis.Location = New System.Drawing.Point(330, 57)
        Me.tgbtnReverseYaxis.Name = "tgbtnReverseYaxis"
        Me.tgbtnReverseYaxis.Size = New System.Drawing.Size(89, 23)
        Me.tgbtnReverseYaxis.TabIndex = 7
        Me.tgbtnReverseYaxis.Text = "Reverse Y Axis"
        Me.tgbtnReverseYaxis.UseVisualStyleBackColor = True
        '
        'inChartTitle
        '
        Me.inChartTitle.Location = New System.Drawing.Point(75, 5)
        Me.inChartTitle.Name = "inChartTitle"
        Me.inChartTitle.Size = New System.Drawing.Size(222, 20)
        Me.inChartTitle.TabIndex = 8
        '
        'inXLabel
        '
        Me.inXLabel.Location = New System.Drawing.Point(75, 31)
        Me.inXLabel.Name = "inXLabel"
        Me.inXLabel.Size = New System.Drawing.Size(222, 20)
        Me.inXLabel.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "X Label:"
        '
        'inYLabel
        '
        Me.inYLabel.Location = New System.Drawing.Point(75, 57)
        Me.inYLabel.Name = "inYLabel"
        Me.inYLabel.Size = New System.Drawing.Size(222, 20)
        Me.inYLabel.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Y Label:"
        '
        'intoChart
        '
        Me.intoChart.FormattingEnabled = True
        Me.intoChart.Location = New System.Drawing.Point(76, 13)
        Me.intoChart.Name = "intoChart"
        Me.intoChart.Size = New System.Drawing.Size(222, 21)
        Me.intoChart.TabIndex = 14
        '
        'lblChartList
        '
        Me.lblChartList.AutoSize = True
        Me.lblChartList.Location = New System.Drawing.Point(12, 16)
        Me.lblChartList.Name = "lblChartList"
        Me.lblChartList.Size = New System.Drawing.Size(35, 13)
        Me.lblChartList.TabIndex = 13
        Me.lblChartList.Text = "Chart:"
        '
        'chartChange
        '
        Me.chartChange.Location = New System.Drawing.Point(328, 13)
        Me.chartChange.Name = "chartChange"
        Me.chartChange.Size = New System.Drawing.Size(92, 23)
        Me.chartChange.TabIndex = 15
        Me.chartChange.Text = "Change"
        Me.chartChange.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.inYLabel)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.inXLabel)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.inChartTitle)
        Me.Panel1.Controls.Add(Me.tgbtnReverseYaxis)
        Me.Panel1.Controls.Add(Me.tgbtnReverseXaxis)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(1, 45)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(478, 89)
        Me.Panel1.TabIndex = 16
        '
        'DialogChartOptions
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(489, 298)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.chartChange)
        Me.Controls.Add(Me.intoChart)
        Me.Controls.Add(Me.lblChartList)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "DialogChartOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Chart Options"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents ColorDialog1 As System.Windows.Forms.ColorDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents series_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents series_color As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents change_color As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents see_points As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents tgbtnReverseXaxis As System.Windows.Forms.CheckBox
    Friend WithEvents tgbtnReverseYaxis As System.Windows.Forms.CheckBox
    Friend WithEvents inChartTitle As System.Windows.Forms.TextBox
    Friend WithEvents inXLabel As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents inYLabel As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents intoChart As System.Windows.Forms.ComboBox
    Friend WithEvents lblChartList As System.Windows.Forms.Label
    Friend WithEvents chartChange As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel

End Class
