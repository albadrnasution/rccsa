﻿Imports System.Windows.Forms

Public Class DialogRepLoadData

    Private _repLoadData As RepeatedLoadData()
    Public Property repLoadData() As RepeatedLoadData()
        Get
            Return _repLoadData
        End Get
        Set(ByVal value As RepeatedLoadData())
            _repLoadData = value
        End Set
    End Property


    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        '================================= MENYIMPAN REPEATED LOAD DATA ===============================
        'Ambil jumlah baris, Rowcount tidak di minus satu karena jumlah baris berisi pada dataGrid tetap
        Dim rowCount As Integer = DataGrid.RowCount
        'Ambil jumlah kolom (ada 4 harusnya)
        Dim columnCount As Integer = DataGrid.ColumnCount

        'Inisiasi ulang variabel data nilai objek ini, pasang sebanyak rowCount buah
        ReDim _repLoadData(rowCount - 1)
        'Ambil setiap data pada baris dan masukkan ke variabel penyimpanan untuk dikembalikan ke MainForm
        '
        For rowIndex As Integer = 0 To rowCount - 1
            _repLoadData(rowIndex) = New RepeatedLoadData(
                DataGrid.Rows(rowIndex).Cells("clmCyclicNo").Value,
                DataGrid.Rows(rowIndex).Cells("clmNrep").Value,
                DataGrid.Rows(rowIndex).Cells("clmTurnPointPlus").Value,
                DataGrid.Rows(rowIndex).Cells("clmTurnPointMin").Value
            )
        Next

        'Set kembalian dialog dan tutup dialog
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub inNcyclic_TextChanged(sender As Object, e As EventArgs) Handles inNcyclic.TextChanged
        Try
            DataGrid.RowCount = inNcyclic.Text
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
        End Try
    End Sub

    Private Sub DialogRepLoadData_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        '================================= MENGESET BATASAN INPUT
        'Set coloumns on the table grid to accept only Double
        DataGrid.Columns("clmCyclicNo").ValueType = GetType(Integer)
        DataGrid.Columns("clmNrep").ValueType = GetType(Integer)
        DataGrid.Columns("clmTurnPointPlus").ValueType = GetType(Double)
        DataGrid.Columns("clmTurnPointMin").ValueType = GetType(Double)


        '================================= MENGESET DAFTAR REP LOAD JIKA SEBELUMNYA SUDAH DIISI
        Try
            '=================================== MENGAMBIL LAYER DATA ==================================
            'Ambil repload yg sudah disimpan (ketika dialog ini dibuka sebelumnya atau dari file simpanan)
            Dim repLoad As RepeatedLoadData() = Me.repLoadData
            For index As Integer = 0 To repLoad.Length - 1
                DataGrid.Rows.Add()
                DataGrid.Rows(index).Cells("clmCyclicNo").Value() = repLoad(index).no
                DataGrid.Rows(index).Cells("clmNrep").Value() = repLoad(index).numRepetition
                DataGrid.Rows(index).Cells("clmTurnPointPlus").Value() = repLoad(index).turnPointPlus
                DataGrid.Rows(index).Cells("clmTurnPointMin").Value() = repLoad(index).turnPointMin
            Next

            inNcyclic.Text = repLoadData.Count
        Catch ex As ArgumentException
            'Wah, ada error apa ya?
            MessageBox.Show("Unexpected Error. " & ex.Message)
        Catch ex As NullReferenceException
            'Kalau null, abaikan berarti belum pernah diisi
        End Try
    End Sub

    ''' <summary>
    '''  Handle error on dataGrid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dataGrid_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles DataGrid.DataError
        If (e.Exception.GetType = GetType(FormatException)) Then
            ' If users type a non Double input
            e.ThrowException = False
            MessageBox.Show("Input must be a real number.")
        End If
    End Sub

    Private Sub DataGrid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGrid.CellContentClick

    End Sub
End Class
